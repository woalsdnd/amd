# **ISBI 2020 AMD challenge - VUNO EYE TEAM**  

## **Public Dataset**   
- **RIGA: **  https://deepblue.lib.umich.edu/data/concern/data_sets/3b591905z?locale=en  
- **IDRiD: **  https://idrid.grand-challenge.org/  
- **REFUGE: **  https://refuge.grand-challenge.org/  
- **PALM: **  https://palm.grand-challenge.org/  


## **TRAIN**
1. prepare images using task-specific pre-processing codes    
   - ** AMD Classification : ** classification/codes/from_findings/crop_resize.py  
   - ** Disc Segmentation : ** disc_segmentation/codes/crop_resize.py  
   - ** Fovea Localization: ** fovea_localization/codes/prepare_coords.py  
   - ** Lesion Segmentation: ** lesion_segmentation/codes/generate_lesion_mask.py
2. run training commands (followings are examples)    
   - **AMD Classification: (should have the weights of a pretrained finding network)**  
    ```python train_from_findings.py --batch_size=64 --gpu_index=3 --diagnosis=AMD --load_model_path=../models/unified_models/unified_models_BRVO_EarlyDR_DryAMD_bug_fixed/finding_unified.h5 --accum_iters=0  --dataset=AMD```  
   - **Disc Segmentation:**   
   ```python train.py  --config_file=../config/segmentation_overfit.cfg```  
   - **Fovea Localization:**  
   ```python train.py  --config_file=../config/detection_use_all_data.cfg```  
   - **Lesion Segmentation: (should have the weights of a pretrained finding network)**  
   ```python train_finetune_individual.py --config_file=../config/segmentation_overfit_64x64_drusen.cfg```   
   ```python train_finetune_individual.py --config_file=../config/segmentation_overfit_64x64_exudate.cfg```   
   ```python train_finetune_individual.py --config_file=../config/segmentation_overfit_64x64_hemorrhage.cfg```   
   ```python train_finetune_individual.py --config_file=../config/segmentation_overfit_64x64_others.cfg```   
   ```python train_finetune_individual.py --config_file=../config/segmentation_overfit_64x64_scar.cfg```   


## **INFERENCE**   
1. prepare images in a original size  
2. run inference commands (followings are examples)    
   - **AMD Classification:**  
    ```python inference_AMD.py  --gpu_index=4 --validation=True --Test=True```  
   - **Disc Segmentation:**   
   ```python inference.py --gpu_index=1 --img_dir=../../../data/AMD/Test-400-images/ --submission_version=FinalRound```  
   - **Fovea Localization:**  
   ```python inference.py --gpu_index=0 --img_dir=../../../data/AMD/Test-400-images/ --submission_version=FinalRound```  
   - **Lesion Segmentation:**  
   ```python inference_hemorrhage.py --gpu_index=3 --img_dir=../../../data/AMD/Test-400-images/ --submission_version=FinalRound```   
   ```python inference_drusen.py --gpu_index=0 --img_dir=../../../data/AMD/Test-400-images/ --submission_version=FinalRound```   
   ```python inference_exudate.py --gpu_index=1 --img_dir=../../../data/AMD/Test-400-images/ --submission_version=FinalRound```   
   ```python inference_others.py --gpu_index=3 --img_dir=../../../data/AMD/Test-400-images/ --submission_version=FinalRound```   
   ```python inference_scar.py --gpu_index=0 --img_dir=../../../data/AMD/Test-400-images/ --submission_version=FinalRound```   
   
