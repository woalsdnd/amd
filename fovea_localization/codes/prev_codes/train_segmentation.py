# TODO decrease learning rate when val loss hits plateueo
import os
import logging
import argparse
import configparser

import numpy as np
from PIL import Image

import utils
import iterator
import models

# arrange arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    '--config_file',
    type=str,
    required=True
    )
FLAGS, _ = parser.parse_known_args()

# set config
config = configparser.ConfigParser(interpolation=configparser.ExtendedInterpolation())
config.read_file(open(FLAGS.config_file))
dir_input_check = config["Path"]["dir_input_check"]
dir_save_model = config["Path"]["dir_save_model"]
path_load_model = config["Path"]["path_load_model"] if "path_load_model" in config["Path"] else None
dir_experimental_result = config["Path"]["dir_experimental_result"]
dir_training_input_data = config["Path"]["dir_training_input_data"]
dir_training_label_data = config["Path"]["dir_training_label_data"]
dir_test_input_data = config["Path"]["dir_test_input_data"]
dir_test_label_data = config["Path"]["dir_test_label_data"]
dir_logger = config["Path"]["dir_logger"]
path_logger = os.path.join(dir_logger, os.path.basename(FLAGS.config_file)) 
batch_size = int(config["Train"]["batch_size"])
n_epochs = int(config["Train"]["n_epochs"])
gpu_index = config["Train"]["gpu_index"]
img_length = int(config["Image"]["length"])
utils.makedirs(dir_save_model)
utils.makedirs(dir_experimental_result)
utils.makedirs(dir_logger)

# set logger
logger = logging.getLogger()
logger.setLevel(logging.INFO)
formatter = logging.Formatter(fmt='%(asctime)s %(levelname)-8s %(message)s', datefmt='[%Y-%m-%d %H:%M:%S]')
handler = logging.FileHandler(path_logger)
handler.setFormatter(formatter)
logger.addHandler(handler)

# set gpu index
# gpu_index = utils.available_gpu(10)[0]
# os.environ['CUDA_VISIBLE_DEVICES'] = str(gpu_index)
os.environ['CUDA_VISIBLE_DEVICES'] = gpu_index

# split data
train_input_data = utils.all_files_under(dir_training_input_data)
train_label_data = utils.all_files_under(dir_training_label_data)
test_input_data = utils.all_files_under(dir_test_input_data)
test_label_data = utils.all_files_under(dir_test_label_data)

# set iterator
train_batch_fetcher = iterator.BatchFetcher((train_input_data, train_label_data),
                                           utils.uniform_sample_weight(len(train_input_data)),
                                           utils.vessel_segmentation_processing_func_train, batch_size, shuffle=True, replace=True)
val_batch_fetcher = iterator.BatchFetcher((test_input_data, test_label_data), None,
                                           utils.vessel_segmentation_processing_func_val, batch_size, shuffle=False, replace=False)

# define network
network = models.unet((img_length, img_length, 3))
network.summary()
with open(os.path.join(dir_save_model, "network.json"), 'w') as f:
    f.write(network.to_json())

for epoch in range(n_epochs):
    # train loop
    list_training_loss = []
    for batch in train_batch_fetcher:
        utils.vessel_segmentation_check_data(batch, os.path.join(dir_input_check, "train"))
        loss = network.train_on_batch(batch[0], np.expand_dims(batch[1], axis=-1))
        list_training_loss.append([loss] * len(batch[0]))
    logger.info("training loss at {}th epoch: {}".format(epoch, np.mean(list_training_loss)))
        
    # val loop
    list_imgs, list_segmap, list_mask = [], [], []
    for batch in val_batch_fetcher:
        utils.vessel_segmentation_check_data(batch, os.path.join(dir_input_check, "val"))
        segmap = network.predict(batch[0])
        list_imgs.append(batch[0])
        list_segmap.append(segmap)
        list_mask.append(np.expand_dims(batch[1], axis=-1))
    arr_imgs = np.concatenate(list_imgs, axis=0)
    arr_masks = np.concatenate(list_mask, axis=0)
    arr_segmaps = np.concatenate(list_segmap, axis=0)
    val_performance = utils.dice(arr_masks, np.round(arr_segmaps))
    logger.info("val performance at {}th epoch: {}".format(epoch, val_performance))
    for index in range(arr_segmaps.shape[0]):  # save segmentation results
        Image.fromarray((arr_segmaps[index, ..., 0] * 255).astype(np.uint8)).save(os.path.join(dir_experimental_result, "{}epoch_{}th_case_predicted.png".format(epoch, index)))
        if epoch == 0:
            Image.fromarray((arr_masks[index, ..., 0] * 255).astype(np.uint8)).save(os.path.join(dir_experimental_result, "{}th_gt.png".format(index)))
            Image.fromarray((arr_imgs[index, ...] * 30 + 100).astype(np.uint8)).save(os.path.join(dir_experimental_result, "{}th_img.png".format(index)))
    
    # save network
    network.save_weights(os.path.join(dir_save_model, "weight_{}epoch.h5".format(epoch)))
