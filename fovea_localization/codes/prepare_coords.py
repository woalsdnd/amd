import pandas as pd
from PIL import Image
import numpy as np
import os

# REFUGE train
df_REFUGE_train = pd.read_excel("/home/vuno/development/data/REFUGE/Fovea_location_train.xlsx")
list_id, list_X, list_Y = [], [], []
for index, row in df_REFUGE_train.iterrows():
    fpath = os.path.join("/home/vuno/development/data/REFUGE/train_images/", row["ImgName"])
    list_id.append(row["ImgName"].replace(".jpg", ""))
    img = np.array(Image.open(fpath))
    h, w, _ = img.shape
    list_X.append(640.*row["Fovea_X"]/w)
    list_Y.append(640.*row["Fovea_Y"]/h)
# REFUGE val
df_REFUGE_val = pd.read_excel("/home/vuno/development/data/REFUGE/Fovea_locations_val.xlsx")
for index, row in df_REFUGE_val.iterrows():
    fpath = os.path.join("/home/vuno/development/data/REFUGE/val_cropped_images/", row["ImgName"].replace(".jpg", ".png"))
    list_id.append(row["ImgName"].replace(".jpg", ""))
    img = np.array(Image.open(fpath))
    h, w, _ = img.shape
    list_X.append(640.*row["Fovea_X"]/w)
    list_Y.append(640.*row["Fovea_Y"]/h)
df_REFUGE = pd.DataFrame({"id":list_id, "X": list_X, "Y":list_Y})
df_REFUGE.to_csv("/home/vuno/development/data/REFUGE/Fovea_location_640x640.csv", index=False)

exit(1)

# AMD
df_AMD = pd.read_excel("/home/vuno/development/data/AMD/Training400_disc_fovea/Fovea_location.xlsx")
list_id, list_X, list_Y = [], [], []
for index, row in df_AMD.iterrows():
    if "A" in row["imgName"]:
        fpath = os.path.join("/home/vuno/development/data/AMD/Training400/AMD", row["imgName"])
    else:
        fpath = os.path.join("/home/vuno/development/data/AMD/Training400/Non-AMD", row["imgName"])
    list_id.append(row["imgName"].replace(".jpg", ""))
    img = np.array(Image.open(fpath))
    h, w, _ = img.shape
    list_X.append(640.*row["Fovea_X"]/w)
    list_Y.append(640.*row["Fovea_Y"]/h)
df_AMD = pd.DataFrame({"id":list_id, "X": list_X, "Y":list_Y})
df_AMD.to_csv("/home/vuno/development/data/AMD/Training400_disc_fovea/Fovea_location_640x640.csv", index=False)

# IDRiD train
df_IDRiD_train = pd.read_csv("/home/vuno/development/data/IDRiD_localization/labels/IDRiD_Fovea_Center_Training.csv")
df_IDRiD_train.dropna(subset=["Image No"],inplace=True)
x_offset, ori_h, ori_w=250, 2848, 4288
w_cropped=3720-250
y_offset = (w_cropped-ori_h)//2
list_id, list_X, list_Y = [], [], []
for index, row in df_IDRiD_train.iterrows():
    fpath = os.path.join("/home/vuno/development/data/IDRiD_localization/images/training/", row["Image No"]+".jpg")
    list_id.append(row["Image No"])
    list_X.append(640.*(row["X- Coordinate"]-x_offset)/w_cropped)
    list_Y.append(640.*(y_offset+row["Y - Coordinate"])/w_cropped)
df_IDRiD_train = pd.DataFrame({"id":list_id, "X": list_X, "Y":list_Y})
df_IDRiD_train.to_csv("/home/vuno/development/data/IDRiD_localization/labels/IDRiD_fovea_640x640_Training.csv", index=False)
# IDRiD test
df_IDRiD_test = pd.read_csv("/home/vuno/development/data/IDRiD_localization/labels/IDRiD_Fovea_Center_Testing.csv")
df_IDRiD_test.dropna(subset=["Image No"],inplace=True)
list_id, list_X, list_Y = [], [], []
for index, row in df_IDRiD_test.iterrows():
    fpath = os.path.join("/home/vuno/development/data/IDRiD_localization/images/testing/", row["Image No"]+".jpg")
    list_id.append(row["Image No"])
    list_X.append(640.*(row["X- Coordinate"]-x_offset)/w_cropped)
    list_Y.append(640.*(y_offset+row["Y - Coordinate"])/w_cropped)
df_IDRiD_test = pd.DataFrame({"id":list_id, "X": list_X, "Y":list_Y})
df_IDRiD_test.to_csv("/home/vuno/development/data/IDRiD_localization/labels/IDRiD_fovea_640x640_Testing.csv", index=False)

# PALM
df_PALM = pd.read_excel("/home/vuno/development/data/PALM/Fovea_Location.xlsx")
list_id, list_X, list_Y = [], [], []
for index, row in df_PALM.iterrows():
    fpath = os.path.join("/home/vuno/development/data/PALM/PALM-Training400/", row["imgName"])
    list_id.append(row["imgName"].replace(".jpg", ""))
    img = np.array(Image.open(fpath))
    h, w, _ = img.shape
    list_X.append(640.*row["Fovea_X"]/w)
    list_Y.append(640.*row["Fovea_Y"]/h)
df_PALM = pd.DataFrame({"id":list_id, "X": list_X, "Y":list_Y})
df_PALM.to_csv("/home/vuno/development/data/PALM/Fovea_location_640x640.csv", index=False)

