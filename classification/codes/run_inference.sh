gpu_index=1
model_path=../model/B4/dr_pretrained_3rd/weight_489epoch.h5

python inference.py --gpu_index=${gpu_index} --img_dir=/home/vuno/development/data/AV_groundTruth/resized_512x512/ --model_path=${model_path} --save_heatmap=False --submission_version=unlabeled_inference
python inference.py --gpu_index=${gpu_index} --img_dir=/home/vuno/development/data/kaggle2015/resized_512x512/ --model_path=${model_path} --save_heatmap=False --submission_version=unlabeled_inference
python inference.py --gpu_index=${gpu_index} --img_dir=/home/vuno/development/data/APTOS/resized_512x512/ --model_path=${model_path} --save_heatmap=False --submission_version=unlabeled_inference
python inference.py --gpu_index=${gpu_index} --img_dir=/home/vuno/development/data/IDRiD/resized_512x512/ --model_path=${model_path} --save_heatmap=False --submission_version=unlabeled_inference
python inference.py --gpu_index=${gpu_index} --img_dir=/home/vuno/development/data/DeepDRiD/regular-fundus-training/resized_512x512/ --model_path=${model_path} --save_heatmap=False --submission_version=unlabeled_inference
python inference.py --gpu_index=${gpu_index} --img_dir=/home/vuno/development/data/e_ophtha/resized_512x512/ --model_path=${model_path} --save_heatmap=False --submission_version=unlabeled_inference
python inference.py --gpu_index=${gpu_index} --img_dir=/home/vuno/development/data/messidor/resized_512x512/ --model_path=${model_path} --save_heatmap=False --submission_version=unlabeled_inference
python inference.py --gpu_index=${gpu_index} --img_dir=/home/vuno/development/data/messidor2/resized_512x512/ --model_path=${model_path} --save_heatmap=False --submission_version=unlabeled_inference
python inference.py --gpu_index=${gpu_index} --img_dir=/home/vuno/development/ODIR/data/ODIR-5K_Training_Dataset_resized_512_512/ --model_path=${model_path} --save_heatmap=False --submission_version=unlabeled_inference
python inference.py --gpu_index=${gpu_index} --img_dir=/home/vuno/development/ODIR/data/ODIR-5K_Testing_Images_resized_512x512/ --model_path=${model_path} --save_heatmap=False --submission_version=unlabeled_inference
python inference.py --gpu_index=${gpu_index} --img_dir=/home/vuno/development/data/REFUGE/resized_512x512/ --model_path=${model_path} --save_heatmap=False --submission_version=unlabeled_inference
python inference.py --gpu_index=${gpu_index} --img_dir=/home/vuno/development/data/STARE/resized_512x512/ --model_path=${model_path} --save_heatmap=False --submission_version=unlabeled_inference
python inference.py --gpu_index=${gpu_index} --img_dir=/home/vuno/development/data/diaretdb/resized_512x512/ --model_path=${model_path} --save_heatmap=False --submission_version=unlabeled_inference
python inference.py --gpu_index=${gpu_index} --img_dir=/home/vuno/development/data/PALM/resized_512x512/ --model_path=${model_path} --save_heatmap=False --submission_version=unlabeled_inference
python inference.py --gpu_index=${gpu_index} --img_dir=/home/vuno/development/data/riga/resized_512x512/ --model_path=${model_path} --save_heatmap=False --submission_version=unlabeled_inference
