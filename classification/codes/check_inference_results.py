import pandas as pd
import os

list_csv_name = ["data_APTOS_resized_512x512.csv", "data_messidor_resized_512x512.csv", "data_AV_groundTruth_resized_512x512.csv",
                 "data_PALM_resized_512x512.csv", "data_diaretdb_resized_512x512.csv", "data_REFUGE_resized_512x512.csv",
                 "data_e_ophtha_resized_512x512.csv", "data_riga_resized_512x512.csv", "data_IDRiD_resized_512x512.csv",
                 "DeepDRiD_regular-fundus-training_resized_512x512.csv", "data_kaggle2015_resized_512x512.csv",
                 "ODIR_data_ODIR-5K_Testing_Images_resized_512x512.csv", "data_messidor2_resized_512x512.csv",
                 "data_STARE_resized_512x512.csv", "ODIR_data_ODIR-5K_Training_Dataset_resized_512_512.csv"]

list_dir_path = ["/home/vuno/development/data/APTOS/resized_512x512", "/home/vuno/development/data/messidor/resized_512x512/",
                 "/home/vuno/development/data/AV_groundTruth/resized_512x512/", "/home/vuno/development/data/PALM/resized_512x512/",
                 "/home/vuno/development/data/diaretdb/resized_512x512/", "/home/vuno/development/data/REFUGE/resized_512x512/",
                 "/home/vuno/development/data/e_ophtha/resized_512x512/", "/home/vuno/development/data/riga/resized_512x512/",
                 "/home/vuno/development/data/IDRiD/resized_512x512/", "/home/vuno/development/data/DeepDRiD/regular-fundus-training/resized_512x512/",
                 "/home/vuno/development/data/kaggle2015/resized_512x512", "/home/vuno/development/ODIR/data/ODIR-5K_Testing_Images_resized_512x512/",
                 "/home/vuno/development/data/messidor2/resized_512x512/", "/home/vuno/development/data/STARE/resized_512x512/",
                 "/home/vuno/development/ODIR/data/ODIR-5K_Training_Dataset_resized_512_512/"]

neg_threshold=0.4
pos_threshold=0.7
data_home = "/home/vuno/development/AMD/classification/data"
dir_neg=os.path.join(data_home, "neg")
dir_pos=os.path.join(data_home, "pos")
os.makedirs(dir_neg, exist_ok=True)
os.makedirs(dir_pos, exist_ok=True)
list_df = []
for index, csv_name in enumerate(list_csv_name):
    print(csv_name)
    df = pd.read_csv(os.path.join("../submission/unlabeled_inference/", csv_name))
    # for fname in df[df["AMD Risk"]<neg_threshold]["FileName"]:
        # os.system('cp {} {}'.format(os.path.join(list_dir_path[index], fname), dir_neg))
    # for fname in df[df["AMD Risk"]>pos_threshold]["FileName"]:
        # os.system('cp {} {}'.format(os.path.join(list_dir_path[index], fname), dir_pos))
    df["FileName"]=df["FileName"].map(lambda fname:os.path.join(list_dir_path[index], fname))
    list_df.append(df)
df = pd.concat(list_df)
df.to_csv(os.path.join("/home/vuno/development/AMD/classification/pseudo_label", "AMD_pseudo_label_v1.csv"), index=False)

for interval in [0.1, 0.15, 0.2, 0.3, 0.4, 0.5]:
    print("#less than {}: {}".format(interval, len(df[df["AMD Risk"]<interval])))
for interval in [0.5, 0.6, 0.7, 0.75, 0.8, 0.9]:
    print("#greater than {}: {}".format(interval, len(df[df["AMD Risk"]>interval])))
