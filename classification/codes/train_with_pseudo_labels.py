import os
import logging
import argparse
import configparser
import time

import numpy as np
import pandas as pd
from PIL import Image

import utils
import iterator_shared_array
from model.efficient_net import EfficientNetB4, set_single_output, compile_single_loss

from keras.utils.training_utils import multi_gpu_model


def set_filenames_labels(dataset_name, fpaths_data, df_label):
    if "AMD" in dataset_name:
        fid_col, label_col = "filename", "label"
    else:
        raise ValueError("dataset unidentified")
    df_filepath_idcode = pd.DataFrame({"filepath": fpaths_data, fid_col: [os.path.basename(fpath) for fpath in fpaths_data]})
    df_merged = pd.merge(df_label, df_filepath_idcode, on=fid_col, how="inner")
    return list(df_merged["filepath"]), list(df_merged[label_col])


def set_single_output_multigpu_loss(network, lr_start_value, n_multigpu):
    new_network = set_single_output(network)
    if n_multigpu > 1:
        new_network = multi_gpu_model(new_network, gpus=n_multigpu)
    new_network = compile_single_loss(new_network, lr_start_value)
    return new_network


def set_trainable(netweork, val):
    network.trainable = val
    for l in network.layers:
        l.trainable = val


# arrange arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    '--config_file',
    type=str,
    required=True
)
FLAGS, _ = parser.parse_known_args()

# set config
config = configparser.ConfigParser(interpolation=configparser.ExtendedInterpolation())
config.read_file(open(FLAGS.config_file))
path_pos_data = config["Path"]["path_pos_data"]
dir_training_input_label1 = config["Path"]["path_unlabeled_inference"]
dir_val_input_data = config["Path"]["path_val_data"]
dir_val_input_label = config["Path"]["path_val_label"]
dir_save_model = config["Path"]["dir_save_model"]
path_load_model = config["Path"]["path_load_model"] if "path_load_model" in config["Path"] else None
dir_experimental_result = config["Path"]["dir_experimental_result"]
dir_logger = config["Path"]["dir_logger"]
path_logger = os.path.join(dir_logger, os.path.basename(FLAGS.config_file).replace(".cfg", ".log"))
model_type = config["Train"]["model_type"]
batch_size = int(config["Train"]["batch_size"])
n_epochs = int(config["Train"]["n_epochs"])
lr_decay_tolerance = int(config["Train"]["lr_decay_tolerance"])
lr_decay_factor = float(config["Train"]["lr_decay_factor"])
lr_start_value = float(config["Train"]["lr_start"])
lr_min_value = float(config["Train"]["lr_min_value"])
optimizer = config["Train"]["optimizer"]
gpu_index = config["Train"]["gpu_index"]
neg_threshold = float(config["Train"]["neg_threshold"])
pos_threshold = float(config["Train"]["pos_threshold"])
n_multigpu = len(gpu_index.split(","))
input_size = (int(config["Input"]["height"]), int(config["Input"]["width"]), int(config["Input"]["depth"]))
utils.makedirs(dir_save_model)
utils.makedirs(dir_experimental_result)
utils.makedirs(dir_logger)

# set gpu index
os.environ['CUDA_VISIBLE_DEVICES'] = gpu_index

# set logger and tensorboard
logger = logging.getLogger()
logger.setLevel(logging.INFO)
formatter = logging.Formatter(fmt='%(asctime)s %(levelname)-8s %(message)s', datefmt='[%Y-%m-%d %H:%M:%S]')
handler = logging.FileHandler(path_logger)
handler.setFormatter(formatter)
logger.addHandler(handler)
tensorboard = utils.CustomTensorBoard(
    log_dir=os.path.join(dir_logger, "tensorboard", os.path.basename(FLAGS.config_file).replace(".cfg", ".log")),
    write_graph=False,
    batch_size=batch_size
)

# prepare train and val
list_fpath_pos = utils.all_files_under(path_pos_data)
df_label_data1 = pd.read_csv(dir_training_input_label1)
df_pos = df_label_data1[df_label_data1["AMD Risk"]>pos_threshold]
df_neg = df_label_data1[df_label_data1["AMD Risk"]<neg_threshold]
fpaths_data1 = list(list_fpath_pos)+list(df_pos["FileName"])+list(df_neg["FileName"])
label_data1 = [1]*len(list_fpath_pos)+[1]*len(df_pos["FileName"])+[0]*len(df_neg["FileName"])
fpaths_val_data = utils.all_files_under(dir_val_input_data)
df_val_label = pd.read_csv(dir_val_input_label)
fpaths_val_data, label_val_data = set_filenames_labels(dir_val_input_data, fpaths_val_data, df_val_label)
assert len(fpaths_data1) == len(label_data1)
assert len(fpaths_val_data) == len(label_val_data)

# set iterators
class_weight, sample_weight_data1 = utils.balanced_class_weights(label_data1)
logger.info("#data:{}, #pos:{}, #neg:{}, class weight: {}".format(len(label_data1), len(df_pos)+len(list_fpath_pos), len(df_neg), class_weight))
train_batch_fetcher = iterator_shared_array.BatchFetcher((fpaths_data1, label_data1), [sample_weight_data1],
                                                         utils.fundus_classification_processing_func_strong_aug_DeepDRiD, batch_size, sample=True, replace=True,
                                                         shared_array_shape=[input_size, ()])
val_batch_fetcher = iterator_shared_array.BatchFetcher((fpaths_val_data, label_val_data), None,
                                                       utils.fundus_classification_processing_func_val_DeepDRiD, batch_size, sample=False, replace=False, shared_array_shape=[input_size, ()])

# define network
network = EfficientNetB4(include_top=False, weights=None, use_resnet_init_conv=True, input_shape=input_size)
network.load_weights(path_load_model)
logger.info("load weights from {}".format(path_load_model))

# compile network (while freezing backbone)
network = set_single_output_multigpu_loss(network, lr_start_value, n_multigpu)
network.summary()
# network.load_weights(path_load_model)
# logger.info("load weights from {}".format(path_load_model))
tensorboard.set_model(network)  # set tensorboard callback associated with network

lr_scheduler = utils.LrScheduler(lr_object=network.optimizer.lr, lr_start_value=lr_start_value,
                                 lr_min_value=lr_min_value, lr_decay_tolerance=lr_decay_tolerance,
                                 lr_decay_factor=lr_decay_factor, logger=logger, score_func=lambda x:x["AUROC"])

best_val_auroc = 0
for epoch in range(n_epochs):
        
    # train loop with labeled data
    list_loss, list_acc = [], []
    for list_arr in train_batch_fetcher:
        # list_arr = [img1, label1]
        tensorboard.draw_imgs("Training Image", epoch, (list_arr[0] * 255).astype(np.uint8), plot_once=True)
        loss, acc = network.train_on_batch(list_arr[0], list_arr[1])
        list_loss += [loss]*len(list_arr[0])
        list_acc += [acc]*len(list_arr[0])
    train_metrics = {"loss": np.mean(list_loss), "acc":np.mean(list_acc)}
    utils.log_summary(logger, phase="training", epoch=epoch, **train_metrics)
    tensorboard.on_epoch_end(epoch, train_metrics)
    
    # val loop
    list_gt, list_pred = [], []
    for list_arr in val_batch_fetcher:
        # list_arr = [img, label]
        tensorboard.draw_imgs("Validation Image", epoch, (list_arr[0] * 255).astype(np.uint8), plot_once=True)
        preds = network.predict(list_arr[0])
        utils.stack_list(head1=list_gt, tail1=list(list_arr[1]), head2=list_pred, tail2=list(preds[:,0]))
    val_metrics = utils.binary_stats(list_gt, list_pred)
    utils.log_summary(logger, phase="validation", epoch=epoch, **val_metrics)
    tensorboard.on_epoch_end(epoch, {key: val for key, val in val_metrics.items() if key != "confusion_matrix"})

    # adjust learning rate
    lr_scheduler.adjust_lr(epoch, val_metrics)

    # save network
    if n_multigpu>1:
        network.layers[-2].save_weights(os.path.join(dir_save_model, "weight_{}epoch.h5".format(epoch)))
    else:
        network.save_weights(os.path.join(dir_save_model, "weight_{}epoch.h5".format(epoch)))
    if best_val_auroc < val_metrics["AUROC"]:
        best_val_auroc = val_metrics["AUROC"]
        if n_multigpu>1:
            network.layers[-2].save_weights(os.path.join(dir_save_model, "weight_best_AUROC.h5".format(epoch)))
        else:
            network.save_weights(os.path.join(dir_save_model, "weight_best_AUROC.h5".format(epoch)))