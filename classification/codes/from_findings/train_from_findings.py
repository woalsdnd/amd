# python2.7
#  python train_from_findings.py --batch_size=64 --gpu_index=3 --diagnosis=AMD --load_model_path=../models/unified_models/unified_models_BRVO_EarlyDR_DryAMD_bug_fixed/finding_unified.h5 --accum_iters=0  --dataset=AMD
import utils
import os
import argparse
import time
from keras import backend as K
import sys
import iterator_diagnosis
import models
import numpy as np
import random
import pandas as pd
import efficient_net_modify

list_findings = ["Hemorrhage", "HardExudate", "CWP", "Drusen", "RetinalPigmentaryChange", "VascularAbnormality", "Membrane", "FluidAccumulation",
                 "ChroioretinalAtrophy", "ChoroidalLesion", "MyelinatedNerveFiber", "RNFLDefect", "GlaucomatousDiscChange", "NonGlaucomatousDiscChange", "MacularHole"]

list_diagnoses = ['EarlyDR', 'AdvancedDR', 'BRVOHemiCRVO', 'CRVO', 'DryAMD', 'WetAMD', 'GlaucomaSuspect', 'EpiretinalMembrane']  # ignore macular hole

label_options = {"Hemorrhage": "Hemorrhage", "HardExudate":'Hard Exudate', "CWP":'Cotton Wool Patch',
         "Drusen":'Drusen & Drusenoid Deposits', "RetinalPigmentaryChange":'Retinal Pigmentary Change',
          "MacularHole":'Macular Hole', "VascularAbnormality":'Vascular Abnormality', "Membrane":'Membrane',
          "FluidAccumulation":'Fluid Accumulation', "ChroioretinalAtrophy":'Chroioretinal Atrophy/Scar',
          "ChoroidalLesion":'Choroidal Lesion', "MyelinatedNerveFiber":'Myelinated Nerve Fiber',
          "RNFLDefect":'RNFL Defect', "GlaucomatousDiscChange":'Glaucomatous Disc Change',
          "NonGlaucomatousDiscChange":'Non-glaucomatous Disc Change',
          "DryAMD": "Dry AMD", "WetAMD": "Wet AMD", "EarlyDR":"Early DR", "AdvancedDR":"Advanced DR", "DR":"DR", "CRVO":"CRVO",
          "BRVOHemiCRVO":"BRVO / Hemi-CRVO", "EpiretinalMembrane":"Epiretinal membrane", "GlaucomaSuspect":"Glaucoma Suspect"}

# arrange arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    '--batch_size',
    type=int,
    help="size of a batch for each GPU",
    required=True
    )
parser.add_argument(
    '--gpu_index',
    type=int,
    help="GPU index",
    required=True
    )
parser.add_argument(
    '--load_model_path',
    type=str,
    required=True,
    )
parser.add_argument(
    '--diagnosis',
    type=str,
    required=True,
    )
parser.add_argument(
    '--accum_iters',
    type=int,
    required=False,
    default=0
    )
parser.add_argument(
    '--dataset',
    type=str,
    required=True,
    )
FLAGS, _ = parser.parse_known_args()

# set lambda, gpu, path
lr_start_value = 1e-3 if FLAGS.accum_iters==0 else 1e-3 * FLAGS.accum_iters
lr_min_value = 6e-5 if FLAGS.accum_iters==0 else 6e-5 * FLAGS.accum_iters
lr_decay_tolerance = 3
lr_decay_factor = 1. / 10
os.environ['CUDA_VISIBLE_DEVICES'] = str(FLAGS.gpu_index)
n_epochs = 100
model_dir = "miccai_{}_{}".format(FLAGS.dataset, FLAGS.diagnosis)
if not os.path.isdir(model_dir):
    os.makedirs(model_dir)

# training settings
if FLAGS.dataset=="AMD":
    df_label = pd.read_csv("/media/ext/external_data/AMD/AMD.csv")
    df_label.loc[:, "filename"] = df_label["filename"].map(lambda x:os.path.join("/media/ext/external_data/AMD/resized_1024x1024", x))
    df_val=df_label.copy()
else:
    raise ValueError("dataset should be AMD")

# set -1 to nan (-1 will be ignored in loss computation)
cw, p = utils.balanced_class_weights(df_label["label"])
df_label.loc[:, "sample_weight"] = p
df_label_val.loc[:, "sample_weight"]=None
batch_fetcher = {"train": iterator_diagnosis.BatchFetcher(df_label, FLAGS.batch_size, "normalize", augment=True),
                "val": iterator_diagnosis.BatchFetcher(df_label_val, FLAGS.batch_size*2, "normalize", augment=False)}

# create & save network
img_shape = (1024, 1024, 3)
network = efficient_net_modify.EfficientNet_multiheads(input_shape=img_shape)
for l in network.layers:
    l.trainable = False

weight_file = FLAGS.load_model_path
network.load_weights(weight_file)
print("weights loaded from {}".format(weight_file))
current_network = models.network_diagnosis(network, FLAGS.diagnosis, lr_start_value, FLAGS.accum_iters)
current_network.summary()

# train the network 
lr_scheduler = utils.LrScheduler(lr_object=current_network.optimizer.lr, lr_start_value=lr_start_value,
                                 lr_min_value=lr_min_value, lr_decay_tolerance=lr_decay_tolerance,
                                 lr_decay_factor=lr_decay_factor, score_func=lambda x:x[0])
best_training_loss = 100
for epoch in range(n_epochs):
    
    start_time = time.time()
    training_class_loss, training_class_acc = [], []
    for filenames, batch_x, diagnosis_labels in batch_fetcher["train"]():
        loss, acc = current_network.train_on_batch(batch_x, diagnosis_labels)
        training_class_loss += [loss] * len(filenames)
        training_class_acc += [acc] * len(filenames)
   
    # evaluate on training set
    print("=============== {}th epoch ===============".format(epoch))
    print("{} - classification loss: {}".format(FLAGS.diagnosis, np.mean(training_class_loss)))
    print("{} - classification acc: {}".format(FLAGS.diagnosis, np.mean(training_class_acc)))

    # evaluate on val set
    list_diagnosis_target, list_diagnosis_pred = [], []
    for fns, batch_x, diagnosis_labels in batch_fetcher["val"]():
        preds = current_network.predict(batch_x)
        list_diagnosis_target += list(diagnosis_labels)
        list_diagnosis_pred += list(preds[:, 0])
    true_labels = np.array(list_diagnosis_target)
    pred_labels = np.array(list_diagnosis_pred)
    print("--- validation ---")
    utils.print_stats(true_labels[true_labels != -1], pred_labels[true_labels != -1])
    
    if FLAGS.dataset=="REFUGE":
        # evaluate on test set (REFUGE)
        list_diagnosis_target, list_diagnosis_pred = [], []
        for fns, batch_x, diagnosis_labels in batch_fetcher["test"]():
            preds = current_network.predict(batch_x)
            list_diagnosis_target += list(diagnosis_labels)
            list_diagnosis_pred += list(preds[:, 0])
        true_labels = np.array(list_diagnosis_target)
        pred_labels = np.array(list_diagnosis_pred)
        print("--- test ---")
        utils.print_stats(true_labels[true_labels != -1], pred_labels[true_labels != -1])
        
    # adjust learning rate
    lr_scheduler.adjust_lr(epoch, [np.mean(training_class_loss)])

    # save weights
    current_network.save_weights(os.path.join(model_dir, "weight_{}th_epoch.h5".format(epoch)))
    if best_training_loss > np.mean(training_class_loss):
        current_network.save_weights(os.path.join(model_dir, "weight_best_training_loss.h5"))
    sys.stdout.flush()
