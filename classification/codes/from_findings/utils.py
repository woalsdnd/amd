# python2.7
import os
import re

from PIL import Image, ImageEnhance

import numpy as np
import itertools
import pandas as pd
from sklearn.metrics import confusion_matrix
from sklearn.metrics.ranking import roc_auc_score
import random
from skimage.transform import warp, AffineTransform
from subprocess import Popen, PIPE
import math
import mask_region
from scipy.misc import imresize
from skimage import color
from keras import backend as K
from keras.models import model_from_json
import matplotlib.cm as cm
from PIL import ImageFilter
from skimage import measure
from albumentations.augmentations import transforms
from sklearn.utils import class_weight

from sklearn.manifold import TSNE

PROB_AUG = 0.1

list_findings = ["Hemorrhage", "HardExudate", "CWP", "Drusen", "RetinalPigmentaryChange", "VascularAbnormality", "Membrane", "FluidAccumulation",
             "ChroioretinalAtrophy", "ChoroidalLesion", "MyelinatedNerveFiber", "RNFLDefect", "GlaucomatousDiscChange", "NonGlaucomatousDiscChange", "MacularHole"]

list_diseases = ['EarlyDR', 'AdvancedDR', 'BRVOHemiCRVO', 'CRVO', 'DryAMD', 'WetAMD', 'GlaucomaSuspect', 'EpiretinalMembrane']  # ignore macular hole


class LrScheduler:
    """
    Learning Rate Scheduler.
    Reduce on validation plateau and reset to init lr when the lr becomes less than minimum
    """
    
    def __init__(self, lr_object, lr_start_value, lr_min_value, lr_decay_tolerance, lr_decay_factor, score_func):
        """
        Args:
            lr_object: keras optimizer lr object
            lr_start_value: float
            lr_min_value: float
            lr_decay_tolerance: int
            lr_decay_factor: float
            score_func: function that maps validation metric to scalar
        """
        self.lr_object = lr_object
        self.lr_start_value = lr_start_value
        self.lr_min_value = lr_min_value
        self.lr_decay_tolerance = lr_decay_tolerance
        self.lr_decay_factor = lr_decay_factor
        self.best_val_score = None
        self.score_func = score_func
        self.n_no_improvements = 0

    def adjust_lr(self, epoch, val_metrics):
        curr_val_score = self.score_func(val_metrics)
        if self.best_val_score is None:  # first epoch
            self.best_val_score = curr_val_score
        elif self.best_val_score < curr_val_score:  # improved
            self.best_val_score = curr_val_score
            self.n_no_improvements = 0
        else:  # not improved
            self.n_no_improvements += 1
            if self.n_no_improvements == self.lr_decay_tolerance:
                new_lr = K.get_value(self.lr_object) * self.lr_decay_factor
                K.set_value(self.lr_object, new_lr)    
                print("set learning rate to {} at epoch {}".format(new_lr, epoch))
                self.n_no_improvements = 0
                
        # restart when lr is less than lr_min
        if K.get_value(self.lr_object) < self.lr_min_value:   
            K.set_value(self.lr_object, self.lr_start_value)
            print("reset learning rate to {} at epoch".format(self.lr_start_value, epoch))


def run_tsne(features):
    """
    T-SNE to 2d plane
    
    Args:
        features (N x D numpy array) -- N: #data, D: #dimension of a data
    
    Returns:
        embedded matrix (N x D numpy array) -- N: #data, D: #dimension of a data in 2d plane
    """
    embedded = TSNE(n_components=2, verbose=1, n_iter=3000).fit_transform(features)
    return embedded


def copy_weights_multiheads(multiheads_network, finding_network, finding, copy_shared_weights=False):
    list_findings = ["Hemorrhage", "HardExudate", "CWP", "Drusen", "RetinalPigmentaryChange", "VascularAbnormality", "Membrane", "FluidAccumulation",
                     "ChroioretinalAtrophy", "ChoroidalLesion", "MyelinatedNerveFiber", "RNFLDefect", "GlaucomatousDiscChange", "NonGlaucomatousDiscChange", "MacularHole"]

    n_layers_finding_network = len(finding_network.layers)
    starting_index_block4 = [index for index in range(n_layers_finding_network) if "block4" in finding_network.layers[index].name][0]

    # set weights from the beginning to block4
    if copy_shared_weights:
        for index in range(starting_index_block4):
            multiheads_network_weights = multiheads_network.layers[index].get_weights()
            finding_network_weights = finding_network.layers[index].get_weights()
            if len(multiheads_network_weights) == len(finding_network_weights) and np.array([multiheads_network_weights[i].shape == finding_network_weights[i].shape for i in range(len(finding_network_weights))]).all():
                multiheads_network.layers[index].set_weights(finding_network_weights)
                print(finding_network.layers[index].name, multiheads_network.layers[index].name)
    
    # set weights from block4 to output layers
    for index in range(starting_index_block4, n_layers_finding_network):
        finding_network_weights = finding_network.layers[index].get_weights()
        layer_name = finding_network.layers[index].name
        if "block" in layer_name:
            list_tokens = layer_name.split("_")
            if "expand" in layer_name or "se" in layer_name or "project" in layer_name:
                list_new_tokens = list_tokens.insert(-2, finding)
            else:
                list_new_tokens = list_tokens.insert(-1, finding)
            multiheads_network_layer_name = "_".join(list_tokens)
        elif "conv2d" in layer_name:
            break
        else:
            multiheads_network_layer_name = "{}_{}".format(layer_name, finding)
         
        multiheads_network.get_layer(multiheads_network_layer_name).set_weights(finding_network_weights)
        print(multiheads_network_layer_name, finding_network.layers[index].name)
    
    # set weights for output layers
    from_conv1x1_name = "conv2d_{}".format(list_findings.index(finding) + 1)
    to_conv1x1_name = "1x1conv_{}".format(finding)
    multiheads_network.get_layer(to_conv1x1_name).set_weights(finding_network.get_layer(from_conv1x1_name).get_weights())
    return multiheads_network

    
def copy_weights(from_network, to_network, starting_index=2, to_network_offset=0, from_network_offset=0):
    for index in range(starting_index, min(len(from_network.layers) - from_network_offset, len(to_network.layers) - to_network_offset)):
        to_network_weights = to_network.layers[index + to_network_offset].get_weights()
        from_network_weights = from_network.layers[index + from_network_offset].get_weights()
        if len(to_network_weights) == len(from_network_weights) and np.array([to_network_weights[i].shape == from_network_weights[i].shape for i in range(len(to_network_weights))]).all():
            to_network.layers[index + to_network_offset].set_weights(from_network.layers[index + from_network_offset].get_weights())
            print(to_network.layers[index + to_network_offset].name, from_network.layers[index + from_network_offset].name)
#         to_network.layers[index + to_network_offset].trainable = False
        else:
            break
    return to_network


def region_encoding_all_in_one(region_str):
    region_code = {"":0, "M":1, "T":2, "ST":3, "IT":4, "SN":5, "IN":6, "SD":7, "ID":8}
    n_regions = len(region_code) - 1
    codified_regions = np.zeros(n_regions + 1)
    for region in [chunk.strip() for chunk in region_str.split("|")]:
        codified_regions[region_code[region]] = 1
    return codified_regions
    

def add_sample_weight_kaggle2015_finetune(df):
    
    # entire normal ratio: 1/2N * N = 0.5
    neg_pos_weights = np.array([1., 1.])
    
    # set sample weight
    df["sample_weight"] = 0
    bc = np.bincount(df["label"])
    cw = 1.* neg_pos_weights / (np.sum(neg_pos_weights) * bc)
    assert len(cw) == 2
    for i, weight in enumerate(cw):
        df.loc[df["label"] == i, "sample_weight"] += weight
    df.loc[:, "sample_weight"] /= np.sum(df["sample_weight"])
    return df

def balanced_class_weights(labels):
    labels = np.array(labels)
    cw = class_weight.compute_class_weight(
        "balanced", np.unique(labels), labels)
    cw /= cw.sum()
    p = np.zeros(len(labels))
    for i, weight in enumerate(cw):
        p[labels == i] = weight
    p /= p.sum()
    return cw, p

def add_sample_weight_all_in_one(df, dict_scale, is_one_target=False):
    if "Hemorrhage" in dict_scale:
        target = 'finding'
    elif "EarlyDR" in dict_scale:
        target = 'disease'
    elif "label" in dict_scale:
        target = 'normality'
    else:
        raise ValueError("target should be either finding or disease")
    
    # entire normal ratio: 1/2N * N = 0.5
    if is_one_target:
        neg_pos_weights = np.array([1., 1.])
    else:
        neg_pos_weights = np.array([1, len(list_findings) * 2 - 1]) if target == 'finding' else np.array([1, len(list_diseases) * 2 - 1])
    if target == 'normality':
        list_target = ["label"]
    elif target == 'finding':
        list_target = list_findings
    else:
        list_target = list_diseases
    
    # set sample weight
    df["sample_weight"] = 0
    for el in list_target:
        legit_labels = list(df[df[el] != -1][el])
        cw = 1.* neg_pos_weights / (np.sum(neg_pos_weights) * np.bincount(legit_labels))
        assert len(cw) == 2
        for i, weight in enumerate(cw):
            df.loc[np.round(df[el]) == i, "sample_weight"] += dict_scale[el] * weight
    df.loc[:, "sample_weight"] /= np.sum(df["sample_weight"])
    return df


def bce(y, y_hat):
    epsilon = 1e-6
    return max(0, -(1 - y) * np.log(1 - y_hat + epsilon) - y * np.log(y_hat + epsilon))


def add_sample_weight_all_in_one_hardexample(df, dict_scale):
    if "Hemorrhage" in dict_scale:
        target = 'finding'
    elif "EarlyDR" in dict_scale:
        target = 'diagnosis'
    elif "label" in dict_scale:
        target = 'normality'
    else:
        raise ValueError("target should be either finding or diagnosis")
    
    # entire normal ratio: 1/2N * N = 0.5
    if target == 'normality':
        list_target = ["label"]
        hardexample_weights = np.array([0.1, 0.1])  # FP, FN
        neg_pos_weights = np.array([0.4, 0.4])
    elif target == 'finding':
        list_target = list_findings
        for finding, weight in dict_scale.iteritems():
            if weight != 0:
                target_finding = finding
        if target_finding == "Hemorrhage":
            hardexample_weights = np.array([0.1, 0.1])  # FP, FN
            neg_pos_weights = np.array([0.4, 0.4])
        elif target_finding == "Membrane":
            hardexample_weights = np.array([0.1, 0.1])  # FP, FN
            neg_pos_weights = np.array([0.4, 0.4])
        elif target_finding == "CWP":
            hardexample_weights = np.array([0.1, 0])  # FP, FN
            neg_pos_weights = np.array([0.4, 0.5])
        elif target_finding == "RNFLDefect":
            hardexample_weights = np.array([0, 0.1])  # FP, FN
            neg_pos_weights = np.array([0.5, 0.4])
        elif target_finding == "ChroioretinalAtrophy":
            hardexample_weights = np.array([0.1, 0])  # FP, FN
            neg_pos_weights = np.array([0.4, 0.5])
        elif target_finding == "MyelinatedNerveFiber":
            hardexample_weights = np.array([0.1, 0])  # FP, FN
            neg_pos_weights = np.array([0.4, 0.5])
    elif target == 'diagnosis':
        list_target = list_diseases
        for diagnosis, weight in dict_scale.iteritems():
            if weight != 0:
                target_diagnosis = diagnosis
        if target_diagnosis == "EarlyDR":
            hardexample_weights = np.array([0.1, 0.1])  # FP, FN
            neg_pos_weights = np.array([0.4, 0.4])
        elif target_diagnosis == "AdvancedDR":
            hardexample_weights = np.array([0.1, 0.1])  # FP, FN
            neg_pos_weights = np.array([0.4, 0.4])
        elif target_diagnosis == "BRVOHemiCRVO":
            hardexample_weights = np.array([0.1, 0.1])  # FP, FN
            neg_pos_weights = np.array([0.4, 0.4])
        elif target_diagnosis == "CRVO":
            hardexample_weights = np.array([0.1, 0])  # FP, FN
            neg_pos_weights = np.array([0.4, 0.5])
        elif target_diagnosis == "DryAMD":
            hardexample_weights = np.array([0.1, 0])  # FP, FN
            neg_pos_weights = np.array([0.4, 0.5])
        elif target_diagnosis == "WetAMD":
            hardexample_weights = np.array([0.1, 0.1])  # FP, FN
            neg_pos_weights = np.array([0.4, 0.4])
        elif target_diagnosis == "GlaucomaSuspect":
            hardexample_weights = np.array([0.1, 0.1])  # FP, FN
            neg_pos_weights = np.array([0.4, 0.4])
        elif target_diagnosis == "EpiretinalMembrane":
            hardexample_weights = np.array([0.1, 0.1])  # FP, FN
            neg_pos_weights = np.array([0.4, 0.4])
        
    
    print("hardexample_weights: {}, neg_pos_weights: {}".format(hardexample_weights, neg_pos_weights))
    # set sample weight
    df.loc[:, "sample_weight"] = 0
    for el in list_target:
        if dict_scale[el] != 0:
            df.loc[:, "bce"] = df.apply(lambda x:bce(x[el], x["{}_pred".format(el)]), axis=1)
            for label in range(2):
                df_target = df[np.round(df[el]) == label]
                sum_bce = np.sum(df_target["bce"])
                df.loc[np.round(df[el]) == label, "sample_weight"] += hardexample_weights[label] * dict_scale[el] * df_target["bce"] / sum_bce
            cw = 1. / np.bincount(np.round(df[el]).astype(int))
            assert len(cw) == 2
            for i, weight in enumerate(cw):
                df.loc[np.round(df[el]) == i, "sample_weight"] += neg_pos_weights[i] * dict_scale[el] * weight
    df.loc[:, "sample_weight"] /= np.sum(df["sample_weight"])
    return df


def crop_resize(img, h_target, w_target):
    # read the image and resize 
    h_ori, w_ori, _ = np.shape(img)
    red_threshold = 20
    roi_check_len = h_ori // 5
    
    # Find Connected Components with intensity above the threshold
    blobs_labels, n_blobs = measure.label(img[:, :, 0] > red_threshold, return_num=True)
    if n_blobs == 0:
        raise ValueError("crop failed for %s " % (filename)
                + "[no blob found]")
   
    # Find the Index of Connected Components of the Fundus Area (the central area)
    majority_vote = np.argmax(np.bincount(blobs_labels[h_ori // 2 - roi_check_len // 2:h_ori // 2 + roi_check_len // 2,
                                                            w_ori // 2 - roi_check_len // 2:w_ori // 2 + roi_check_len // 2].flatten()))
    if majority_vote == 0:
        raise ValueError("crop failed for %s " % (filename)
                + "[invisible areas (intensity in red channel less than " + str(red_threshold) + ") are dominant in the center]")
    
    row_inds, col_inds = np.where(blobs_labels == majority_vote)
    row_max = np.max(row_inds)
    row_min = np.min(row_inds)
    col_max = np.max(col_inds)
    col_min = np.min(col_inds)
    if row_max - row_min < 100 or col_max - col_min < 100:
        for i in range(1, n_blobs + 1):
            print(len(blobs_labels[blobs_labels == i]))
        raise ValueError("crop failed for %s " % (filename)
                + "[too small areas]")
    
    # crop the image 
    crop_img = img[row_min:row_max, col_min:col_max]
    max_len = max(crop_img.shape[0], crop_img.shape[1])
    img_h, img_w, _ = crop_img.shape
    padded = np.zeros((max_len, max_len, 3), dtype=np.uint8)
    padded[(max_len - img_h) // 2:(max_len - img_h) // 2 + img_h, (max_len - img_w) // 2:(max_len - img_w) // 2 + img_w, ...] = crop_img
    resized_img = imresize(padded, (h_target, w_target), 'bilinear')
    
    return resized_img


def intermediate_out_func(network, list_input_layer_names, list_output_layer_names):
    list_inputs, list_outputs = [], []
    for input_layer_name in list_input_layer_names:
        list_inputs.append(network.get_layer(input_layer_name).input)
    for output_layer_name in list_output_layer_names:
        list_outputs.append(network.get_layer(output_layer_name).output)
    return K.function(list_inputs, list_outputs)


def label_weight_naive(df, round=True, pos_coeff=1):
    if round:
        dict_data_ratio = np.round(df["label"]).value_counts(normalize=True)
        dict_data_weight = [(l, 1 / dict_data_ratio[0]) if l < 0.5 else (l, pos_coeff / dict_data_ratio[1])  for l in df["label"]]
    else:
        dict_data_ratio = df["label"].value_counts(normalize=True)
        dict_data_weight = [(k, 1 / v) for k, v in dict_data_ratio.iteritems()]
    return dict_data_weight


def mkdir_if_not_exist(out_dir):
    if not os.path.isdir(out_dir):
        os.makedirs(out_dir)


def analyze_imgs_draw_activations(filenames, true_labels, pred_labels, activations, out_dir):
    assert len(true_labels) == len(pred_labels) and len(filenames) == len(true_labels)
    dir_FN = os.path.join(out_dir, "FN")
    dir_FP = os.path.join(out_dir, "FP")
    dir_TP = os.path.join(out_dir, "TP")
    
    mkdir_if_not_exist(dir_FN)
    mkdir_if_not_exist(dir_FP)
    mkdir_if_not_exist(dir_TP)
    pred_labels = outputs2labels(pred_labels, 0, 1)

    for i in range(len(filenames)):
        if true_labels[i] == 1 and pred_labels[i] == 0:  # FN
            out_path = os.path.join(dir_FN, os.path.basename(filenames[i]))
        elif true_labels[i] == 0 and pred_labels[i] == 1:  # FP
            out_path = os.path.join(dir_FP, os.path.basename(filenames[i]))
        else:
            continue

        img = np.asarray(Image.open(filenames[i]))
        Image.fromarray(img.astype(np.uint8)).save(out_path + "_original.png")
        h_target, w_target, _ = img.shape
        activation = activations[i, ..., 0]
        heatmap = (activation - np.min(activation)) / (np.max(activation) - np.min(activation))
        heatmap = imresize(heatmap, (h_target, w_target), 'bilinear')
        heatmap_pil_img = Image.fromarray(heatmap)
        heatmap_pil_img = heatmap_pil_img.filter(ImageFilter.GaussianBlur(32))
        heatmap_blurred = np.asarray(heatmap_pil_img)
        heatmap_blurred = np.uint8(cm.jet(heatmap_blurred)[..., :3] * 255)
        overlapped = (img * 0.7 + heatmap_blurred * 0.3).astype(np.uint8)
        Image.fromarray(overlapped).save(out_path)


def analyze_imgs_external_data(filenames, true_labels, pred_labels, activations, out_dir):
    assert len(true_labels) == len(pred_labels) and len(filenames) == len(true_labels)
    dir_FN = os.path.join(out_dir, "FN")
    dir_FP = os.path.join(out_dir, "FP")
    dir_TP = os.path.join(out_dir, "TP")
    
    dir_FN_activation = os.path.join(out_dir, "FN_activation")
    dir_FP_activation = os.path.join(out_dir, "FP_activation")
    dir_TP_activation = os.path.join(out_dir, "TP_activation")
    mkdir_if_not_exist(dir_FN)
    mkdir_if_not_exist(dir_FP)
    mkdir_if_not_exist(dir_TP)
    mkdir_if_not_exist(dir_FN_activation)
    mkdir_if_not_exist(dir_FP_activation)
    mkdir_if_not_exist(dir_TP_activation)
    pred_labels = outputs2labels(pred_labels, 0, 1)

    all_activation_tp = 0
    activation_inside_tp = 0    
    all_activation_fn = 0
    activation_inside_fn = 0    
    for i in range(len(filenames)):
        if not (true_labels[i] == 0 and pred_labels[i] == 0):
            if true_labels[i] == 1 and pred_labels[i] == 0:  # FN
                pipes = Popen(["cp", filenames[i], os.path.join(dir_FN, os.path.basename(filenames[i]))],
                                stdout=PIPE, stderr=PIPE)
                np.save(os.path.join(dir_FN_activation, os.path.basename(filenames[i])), activations[i, :, :, 0])
            elif true_labels[i] == 0 and pred_labels[i] == 1:  # FP
                pipes = Popen(["cp", filenames[i], os.path.join(dir_FP, os.path.basename(filenames[i]))],
                                stdout=PIPE, stderr=PIPE)
                np.save(os.path.join(dir_FP_activation, os.path.basename(filenames[i])), activations[i, :, :, 0])
            elif true_labels[i] == 1 and pred_labels[i] == 1:  # TP        
                pipes = Popen(["cp", filenames[i], os.path.join(dir_TP, os.path.basename(filenames[i]))],
                                stdout=PIPE, stderr=PIPE)
                np.save(os.path.join(dir_TP_activation, os.path.basename(filenames[i])), activations[i, :, :, 0])
            
            std_out, std_err = pipes.communicate()


def analyze_imgs(filenames, true_labels, pred_labels, activations, masks, out_dir, AIR=False):
    assert len(true_labels) == len(pred_labels) and len(filenames) == len(true_labels)
    dir_FN = os.path.join(out_dir, "FN")
    dir_FN_mask = dir_FN + "_mask"
    dir_FP = os.path.join(out_dir, "FP")
    dir_FP_mask = dir_FP + "_mask"
    dir_TP = os.path.join(out_dir, "TP")
    dir_TP_mask = dir_TP + "_mask"
    
    dir_FN_activation = os.path.join(out_dir, "FN_activation")
    dir_FP_activation = os.path.join(out_dir, "FP_activation")
    dir_TP_activation = os.path.join(out_dir, "TP_activation")
    mkdir_if_not_exist(dir_FN)
    mkdir_if_not_exist(dir_FN_mask)
    mkdir_if_not_exist(dir_FP)
    mkdir_if_not_exist(dir_FP_mask)
    mkdir_if_not_exist(dir_TP)
    mkdir_if_not_exist(dir_TP_mask)
    mkdir_if_not_exist(dir_FN_activation)
    mkdir_if_not_exist(dir_FP_activation)
    mkdir_if_not_exist(dir_TP_activation)
    pred_labels = outputs2labels(pred_labels, 0, 1)
    masks_activation_size = np.expand_dims(masks[:, ::32, ::32], axis=3)

    all_activation_tp = 0
    activation_inside_tp = 0    
    all_activation_fn = 0
    activation_inside_fn = 0    
    for i in range(len(filenames)):
        if not (true_labels[i] == 0 and pred_labels[i] == 0):
            img = np.array(Image.open(filenames[i])).astype(np.float32)
            img = img / 2
            img += np.expand_dims(masks[i, ...], axis=2) * 38
            img = img.astype(np.uint8)
            if true_labels[i] == 1 and pred_labels[i] == 0:  # FN
                m = masks_activation_size[i, ..., 0]
                if len(m[m == 1]) > 0:
                    activation_inside_fn += np.sum(activations[i, :, :, 0] * m)
                    all_activation_fn += np.sum(activations[i, :, :, 0])
                pipes = Popen(["cp", filenames[i], os.path.join(dir_FN, os.path.basename(filenames[i]))],
                                stdout=PIPE, stderr=PIPE)
                np.save(os.path.join(dir_FN_activation, os.path.basename(filenames[i])), activations[i, :, :, 0])
                Image.fromarray(img).save(os.path.join(dir_FN_mask, os.path.basename(filenames[i]).replace(".tiff", "") + "_mask_superimposed.png"))
            elif true_labels[i] == 0 and pred_labels[i] == 1:  # FP
                pipes = Popen(["cp", filenames[i], os.path.join(dir_FP, os.path.basename(filenames[i]))],
                                stdout=PIPE, stderr=PIPE)
                np.save(os.path.join(dir_FP_activation, os.path.basename(filenames[i])), activations[i, :, :, 0])
                Image.fromarray(img).save(os.path.join(dir_FP_mask, os.path.basename(filenames[i]).replace(".tiff", "") + "_mask_superimposed.png"))
            elif true_labels[i] == 1 and pred_labels[i] == 1:  # TP        
                m = masks_activation_size[i, ..., 0]
                if len(m[m == 1]) > 0:
                    activation_inside_tp += np.sum(activations[i, :, :, 0] * m)
                    all_activation_tp += np.sum(activations[i, :, :, 0])
                pipes = Popen(["cp", filenames[i], os.path.join(dir_TP, os.path.basename(filenames[i]))],
                                stdout=PIPE, stderr=PIPE)
                np.save(os.path.join(dir_TP_activation, os.path.basename(filenames[i])), activations[i, :, :, 0])
                Image.fromarray(img).save(os.path.join(dir_TP_mask, os.path.basename(filenames[i]).replace(".tiff", "") + "_mask_superimposed.png"))
            
            std_out, std_err = pipes.communicate()
   
    if AIR:
        print("(true positive) inside/all activation ratio: {}".format(1.* activation_inside_tp / all_activation_tp))
        print("(false negative) inside/all activation ratio: {}".format(1.* activation_inside_fn / all_activation_fn))


def img_analyses_img_quality(filenames, true_labels, pred_labels, out_dir):
    assert len(true_labels) == len(pred_labels) and len(filenames) == len(true_labels)
    dir_FN = os.path.join(out_dir, "FN")
    dir_FP = os.path.join(out_dir, "FP")
    dir_TP = os.path.join(out_dir, "TP")
    dir_FN_activation = os.path.join(out_dir, "FN_activation")
    dir_FP_activation = os.path.join(out_dir, "FP_activation")
    dir_TP_activation = os.path.join(out_dir, "TP_activation")
    mkdir_if_not_exist(dir_FN)
    mkdir_if_not_exist(dir_FP)
    mkdir_if_not_exist(dir_TP)
    mkdir_if_not_exist(dir_FN_activation)
    mkdir_if_not_exist(dir_FP_activation)
    mkdir_if_not_exist(dir_TP_activation)

    for i in range(len(filenames)):
        if not (true_labels[i] == 0 and pred_labels[i] == 0):
            if true_labels[i] == 1 and pred_labels[i] == 0:  # FN
                pipes = Popen(["cp", filenames[i], os.path.join(dir_FN, os.path.basename(filenames[i]))],
                                stdout=PIPE, stderr=PIPE)
            elif true_labels[i] == 0 and pred_labels[i] == 1:  # FP
                pipes = Popen(["cp", filenames[i], os.path.join(dir_FP, os.path.basename(filenames[i]))],
                                stdout=PIPE, stderr=PIPE)
            elif true_labels[i] == 1 and pred_labels[i] == 1:  # TP        
                pipes = Popen(["cp", filenames[i], os.path.join(dir_TP, os.path.basename(filenames[i]))],
                                stdout=PIPE, stderr=PIPE)
            
            std_out, std_err = pipes.communicate()
   
            
def all_files_under(path, extension=None, append_path=True, sort=True):
    if append_path:
        if extension is None:
            filenames = [os.path.join(path, fname) for fname in os.listdir(path)]
        else:
            filenames = [os.path.join(path, fname) for fname in os.listdir(path) if fname.endswith(extension)]
    else:
        if extension is None:
            filenames = [os.path.basename(fname) for fname in os.listdir(path)]
        else:
            filenames = [os.path.basename(fname) for fname in os.listdir(path) if fname.endswith(extension)]
    
    if sort:
        filenames = sorted(filenames)
    
    return filenames


def get_mask_of_regions(img_size, disc_center, macula_center, regions):
    region_codes = ["None", "M", "T", "ST", "IT", "SN", "IN", "SD", "ID"]
    mask = np.zeros(img_size)
    for index in range(1, len(regions)):
        if regions[index] == 1:
            mask[(mask == 1) | (mask_region.mask(img_size, disc_center, macula_center, region_codes[index]) == 1)] = 1
    return mask


def imagefiles2arrs(filenames):
    img_shape = image_shape(filenames[0])
    if len(img_shape) == 3:
        images_arr = np.zeros((len(filenames), img_shape[0], img_shape[1], img_shape[2]), dtype=np.float32)
    elif len(img_shape) == 2:
        images_arr = np.zeros((len(filenames), img_shape[0], img_shape[1]), dtype=np.float32)
    
    for file_index in range(len(filenames)):
        images_arr[file_index] = np.array(Image.open(filenames[file_index]))
    
    return images_arr


def normalize(imgs):
    images_arr = 1.*imgs / 255.0
    return images_arr


def load_augmented_diagnosis(fname, normalize, augment):
    # read image file
    lms = np.array([500, 200, 300, 400])  # dummy landmarks
    _, img_aug, lms_aug = load_imgs_lms([fname], [lms], normalize, augment)
    img_aug = img_aug[0, ...]
    lms_aug = lms_aug[0, ...]
    assert len(img_aug.shape) == 3 and lms_aug.shape == (4,)
    return img_aug


# def class_weight(df):
#     data_ratio = df["label"].value_counts(normalize=True)
#     weight = np.array([1. / data_ratio[0], 1. / data_ratio[1]])
#     weight /= sum(weight)
#     print("data number")
#     print(df["label"].value_counts(normalize=False))
#     print("class weight : {}".format(weight))
#     return weight


def image_shape(filename):
    img = Image.open(filename)
    img_arr = np.asarray(img)
    img_shape = img_arr.shape
    return img_shape


def codify_regions(list_region):
    # input : list of strings of regions
    # output : numpy matrix of codified regions
    n_regions = 8
    region_code = {"":0, "M":1, "T":2, "ST":3, "IT":4, "SN":5, "IN":6, "SD":7, "ID":8}

    codified_regions = np.zeros((len(list_region), n_regions + 1))
    for index, region_str in enumerate(list_region):
        for region in [chunk.strip() for chunk in region_str.split("|")]:
            codified_regions[index, region_code[region]] = 1
    return codified_regions


def rotate_pt(origin, point, angle):
    """
    Rotate a point counterclockwise by a given angle around a given origin.
    """
    rad = angle * math.pi / 180
    oy, ox = origin
    py, px = point
    qx = ox + math.cos(rad) * (px - ox) - math.sin(rad) * (py - oy)
    qy = oy + math.sin(rad) * (px - ox) + math.cos(rad) * (py - oy)
    return qy, qx


def dist(pt1, pt2):
    return np.sqrt((pt2[0] - pt1[0]) ** 2 + (pt2[1] - pt1[1]) ** 2)


def out_circle(pt, c_pt, r):
    return dist(pt, c_pt) > r


def random_perturbation(img, rescale_factor=1.5):
    
    im = Image.fromarray(img.astype(np.uint8))
    en_color = ImageEnhance.Color(im)
    im = en_color.enhance(random.uniform(1. / rescale_factor, rescale_factor))
    en_cont = ImageEnhance.Contrast(im)
    im = en_cont.enhance(random.uniform(1. / rescale_factor, rescale_factor))
    en_bright = ImageEnhance.Brightness(im)
    im = en_bright.enhance(random.uniform(1. / rescale_factor, rescale_factor))
    en_sharpness = ImageEnhance.Sharpness(im)
    im = en_sharpness.enhance(random.uniform(1. / rescale_factor, rescale_factor))
    return np.asarray(im).astype(np.uint8)


def augment_imgs(img, mask):
    assert img.shape[0] == mask.shape[0] and img.shape[1] == mask.shape[1]
    h, w, d = img.shape

    aug_img = random_perturbation(img)
    shift_y, shift_x = np.array(aug_img.shape[:2]) / 2.
    shift_to_ori = AffineTransform(translation=[-shift_x, -shift_y])
    shift_to_img_center = AffineTransform(translation=[shift_x, shift_y])
    r_angle = random.randint(0, 359)
    scale_factor = 1.15
    scale = random.uniform(1. / scale_factor, scale_factor)
    tform = AffineTransform(scale=(scale, scale), rotation=np.deg2rad(r_angle))
    aug_img = warp(aug_img, (shift_to_ori + (tform + shift_to_img_center)).inverse, output_shape=(aug_img.shape[0], aug_img.shape[1]))
    aug_img = (aug_img * 255).astype(np.uint8)
    aug_mask = warp(mask, (shift_to_ori + (tform + shift_to_img_center)).inverse, output_shape=(aug_img.shape[0], aug_img.shape[1]))
    aug_mask = np.round(aug_mask)

#     # GridDistortion
#     if random.uniform(0, 1) < PROB_AUG:
#         img = transforms.GridDistortion(num_steps=5, distort_limit=0.5, value=None, mask_value=None, always_apply=True, p=1)(image=img)["image"].astype(np.uint8)
#     
#     # CoarseDropout
#     if random.uniform(0, 1) < PROB_AUG:
#         img = transforms.CoarseDropout(max_holes=5, max_height=img.shape[0] // 10, max_width=img.shape[0] // 10, min_holes=None, min_height=None, min_width=None, p=1)(image=img)["image"].astype(np.uint8)
    
    # blur 
    blur_limit = 10
    if random.uniform(0, 1) < PROB_AUG:
        aug_img = transforms.Blur(blur_limit=blur_limit, p=1)(image=aug_img)["image"].astype(np.uint8)
     
    # GaussNoise
    GaussNoise_val_limit = 80
    if random.uniform(0, 1) < PROB_AUG:
        aug_img = transforms.GaussNoise(var_limit=GaussNoise_val_limit, p=1)(image=aug_img)["image"].astype(np.uint8)
    
    # ISONoise
    ISONoise_color_shift_max = 0.1
    if random.uniform(0, 1) < PROB_AUG:
        aug_img = transforms.ISONoise(color_shift=(0.01, ISONoise_color_shift_max), p=1)(image=aug_img)["image"].astype(np.uint8)
     
    # JpegCompression
    JpegCompression_quality_lower = 60
    if random.uniform(0, 1) < PROB_AUG:
        aug_img = transforms.JpegCompression(quality_lower=JpegCompression_quality_lower, quality_upper=100, p=1)(image=aug_img)["image"].astype(np.uint8)
     
    # RGBShift
    RGBShift_val = 40
    if random.uniform(0, 1) < PROB_AUG:
        aug_img = transforms.RGBShift(r_shift_limit=RGBShift_val, g_shift_limit=RGBShift_val, b_shift_limit=RGBShift_val, p=1)(image=aug_img)["image"].astype(np.uint8)
     
    # RandomGamma
    RandomGamma_min = 50
    RandomGamma_max = 150
    if random.uniform(0, 1) < PROB_AUG:
        aug_img = transforms.RandomGamma(gamma_limit=(RandomGamma_min, RandomGamma_max), p=1)(image=aug_img)["image"].astype(np.uint8)
      
    # RandomSunFlare
    RandomSunFlare_radius = int(random.uniform(256, 512))
    RandomSunFlare_draw = random.uniform(0, 1)
    if RandomSunFlare_draw < 0.25:
        flare_roi = (0, 0, 0.25, 0.25)
    elif RandomSunFlare_draw < 0.5:
        flare_roi = (0.75, 0, 1, 0.25)
    elif RandomSunFlare_draw < 0.75:
        flare_roi = (0, 0.75, 0.25, 1)
    else:
        flare_roi = (0.75, 0.75, 1, 1)
    if random.uniform(0, 1) < PROB_AUG:
        aug_img = transforms.RandomSunFlare(flare_roi=flare_roi, src_radius=RandomSunFlare_radius, num_flare_circles_lower=0, num_flare_circles_upper=1, p=1)(image=aug_img)["image"].astype(np.uint8)
      
    # ElasticTransform
    ElasticTransform_alpha = int(random.uniform(512, 1024))
    ElasticTransform_sigma = ElasticTransform_alpha * random.uniform(0.03, 0.05)
    if random.uniform(0, 1) < PROB_AUG:
        aug_img = transforms.ElasticTransform(alpha=ElasticTransform_alpha, sigma=ElasticTransform_sigma, alpha_affine=0, p=1)(image=aug_img)["image"].astype(np.uint8)
       
    # downsample
    if random.uniform(0, 1) < PROB_AUG:
        downsize_factor = random.uniform(1, 2)
        aug_img = imresize(aug_img, (int(h / downsize_factor), int(w / downsize_factor)), interp="nearest")
        aug_img = imresize(aug_img, (h, w), interp="nearest").astype(np.uint8)

    aug_img = aug_img.astype(np.float32) / 255.
    
    return aug_img, aug_mask


def load_imgs_lms(filenames, lms, normalize=None, augment=True):
    # filenames, lms : list of arrays
    img_shape = image_shape(filenames[0])
    if len(img_shape) == 3:
        images_arr = np.zeros((len(filenames), img_shape[0], img_shape[1], img_shape[2]), dtype=np.float32)
        ori_images_arr = np.zeros((len(filenames), img_shape[0], img_shape[1], img_shape[2]), dtype=np.float32)
    elif len(img_shape) == 2:
        images_arr = np.zeros((len(filenames), img_shape[0], img_shape[1]), dtype=np.float32)
        ori_images_arr = np.zeros((len(filenames), img_shape[0], img_shape[1], img_shape[2]), dtype=np.float32)
    lm_pts = np.zeros((len(filenames), 4), dtype=np.float32)
    for index, lm in enumerate(lms):
        lm_pts[index, :] = lm
    
    for file_index in range(len(filenames)):
        img = np.array(Image.open(filenames[file_index])).astype(np.uint8)
        # remove low values & erase time [:80,...] tag 
        img[img < 10] = 0
        img[:80, ...] = 0
        x, y = np.ogrid[:img.shape[0], :img.shape[1]]
        h, w, d = img.shape

        if augment:
            # random color, contrast, brightness perturbation
            img = random_perturbation(img)
            # random flip
            if random.getrandbits(1):
                img = img[:, ::-1, :]  # flip an image
                lm_pts[file_index, 1] = w - lm_pts[file_index, 1]  # x_disc
                lm_pts[file_index, 3] = w - lm_pts[file_index, 3]  # x_fovea
           
            # affine transform (scale, rotation)
            shift_y, shift_x = np.array(img.shape[:2]) / 2.
            shift_to_ori = AffineTransform(translation=[-shift_x, -shift_y])
            shift_to_img_center = AffineTransform(translation=[shift_x, shift_y])
            r_angle = random.randint(0, 359)
            scale_factor = 1.15
            scale = random.uniform(1. / scale_factor, scale_factor)
            tform = AffineTransform(scale=(scale, scale), rotation=np.deg2rad(r_angle))
            img = warp(img, (shift_to_ori + (tform + shift_to_img_center)).inverse, output_shape=(img.shape[0], img.shape[1]))
            img *= 255
            img = img.astype(np.uint8)
            img_color = np.copy(img)
            
            # rotate landmarks
            lm_pts[file_index, 0] = scale * (lm_pts[file_index, 0] - shift_y)
            lm_pts[file_index, 1] = scale * (lm_pts[file_index, 1] - shift_x)
            lm_pts[file_index, 2] = scale * (lm_pts[file_index, 2] - shift_y)
            lm_pts[file_index, 3] = scale * (lm_pts[file_index, 3] - shift_x)
            lm_pts[file_index, :2] = rotate_pt((0, 0), (lm_pts[file_index, 0], lm_pts[file_index, 1]), r_angle)
            lm_pts[file_index, 2:] = rotate_pt((0, 0), (lm_pts[file_index, 2], lm_pts[file_index, 3]), r_angle)
            lm_pts[file_index, 0] += shift_y
            lm_pts[file_index, 1] += shift_x
            lm_pts[file_index, 2] += shift_y
            lm_pts[file_index, 3] += shift_x
            
            # GridDistortion
            if random.uniform(0, 1) < PROB_AUG:
                img = transforms.GridDistortion(num_steps=5, distort_limit=0.5, value=None, mask_value=None, always_apply=True, p=1)(image=img)["image"].astype(np.uint8)

            # CoarseDropout
            n_max_holes = 8
            max_height = img.shape[0] // 10
            max_width = img.shape[1] // 10
            if random.uniform(0, 1) < PROB_AUG:
                img = transforms.CoarseDropout(max_holes=n_max_holes, max_height=max_height, max_width=max_width, min_holes=None, min_height=None, min_width=None, p=1)(image=img)["image"].astype(np.uint8)

            # blur 
            blur_limit = 10
            if random.uniform(0, 1) < PROB_AUG:
                img = transforms.Blur(blur_limit=blur_limit, p=1)(image=img)["image"].astype(np.uint8)
              
            # GaussNoise
            GaussNoise_val_limit = 80
            if random.uniform(0, 1) < PROB_AUG:
                img = transforms.GaussNoise(var_limit=GaussNoise_val_limit, p=1)(image=img)["image"].astype(np.uint8)
             
            # ISONoise
            ISONoise_color_shift_max = 0.1
            if random.uniform(0, 1) < PROB_AUG:
                img = transforms.ISONoise(color_shift=(0.01, ISONoise_color_shift_max), p=1)(image=img)["image"].astype(np.uint8)
              
            # JpegCompression
            JpegCompression_quality_lower = 60
            if random.uniform(0, 1) < PROB_AUG:
                img = transforms.JpegCompression(quality_lower=JpegCompression_quality_lower, quality_upper=100, p=1)(image=img)["image"].astype(np.uint8)
              
            # RGBShift
            RGBShift_val = 30
            if random.uniform(0, 1) < PROB_AUG:
                img = transforms.RGBShift(r_shift_limit=RGBShift_val, g_shift_limit=RGBShift_val, b_shift_limit=RGBShift_val, p=1)(image=img)["image"].astype(np.uint8)
             
            # RandomGamma
            RandomGamma_min = 50
            RandomGamma_max = 150
            if random.uniform(0, 1) < PROB_AUG:
                img = transforms.RandomGamma(gamma_limit=(RandomGamma_min, RandomGamma_max), p=1)(image=img)["image"].astype(np.uint8)
               
            # RandomSunFlare
            RandomSunFlare_radius = int(random.uniform(256, 512))
            RandomSunFlare_draw = random.uniform(0, 1)
            if RandomSunFlare_draw < 0.25:
                flare_roi = (0, 0, 0.25, 0.25)
            elif RandomSunFlare_draw < 0.5:
                flare_roi = (0.75, 0, 1, 0.25)
            elif RandomSunFlare_draw < 0.75:
                flare_roi = (0, 0.75, 0.25, 1)
            else:
                flare_roi = (0.75, 0.75, 1, 1)
            if random.uniform(0, 1) < PROB_AUG:
                img = transforms.RandomSunFlare(flare_roi=flare_roi, src_radius=RandomSunFlare_radius, num_flare_circles_lower=0, num_flare_circles_upper=1, p=1)(image=img)["image"].astype(np.uint8)
               
            # ElasticTransform
            ElasticTransform_alpha = int(random.uniform(512, 1024))
            ElasticTransform_sigma = ElasticTransform_alpha * random.uniform(0.03, 0.05)
            if random.uniform(0, 1) < PROB_AUG:
                img = transforms.ElasticTransform(alpha=ElasticTransform_alpha, sigma=ElasticTransform_sigma, alpha_affine=0, p=1)(image=img)["image"].astype(np.uint8)
                
            # downsample
            if random.uniform(0, 1) < PROB_AUG:
                downsize_factor = random.uniform(1, 2)
                img = imresize(img, (int(h / downsize_factor), int(w / downsize_factor)), interp="nearest")
                img = imresize(img, (h, w), interp="nearest").astype(np.uint8)
                        
            # tailored augmentation
            if normalize == "data":
                # perturb colors
                for i in range(3):
                    img += np.random.normal(0, 1) * sqrt_eigenvalues[i] * eigenvectors[i, ...]
            elif normalize == "instance_mean":
                img = img * (1 + random.random() * 0.1) if random.getrandbits(1) else img * (1 - random.random() * 0.1)
                img = np.clip(img, 0, 255)
        else:
            img_color = np.copy(img)
      
        ori_images_arr[file_index] = img_color

        if normalize == "instance_std":
            means = np.zeros(3)
            stds = np.array([255.0, 255.0, 255.0])
            for i in range(3):
                if len(img_color[..., i][img_color[..., i] > 10]) > 1:
                    means[i] = np.mean(img_color[..., i][img_color[..., i] > 10])
                    std_val = np.std(img_color[..., i][img_color[..., i] > 10])
                    stds[i] = std_val if std_val > 0 else 255
            images_arr[file_index] = (img - means) / stds
        elif normalize == "instance_mean":
            means = np.zeros(3)
            for i in range(3):
                if len(img_color[..., i][img_color[..., i] > 10]) > 1:
                    means[i] = np.mean(img_color[..., i][img_color[..., i] > 10])
            images_arr[file_index] = img - means
        elif normalize == "normalize":
            images_arr[file_index] = 1.*img.astype(float) / 255.0
        elif normalize == "Lab": 
            img_lab = color.rgb2lab(img.astype(np.uint8))
            if augment:
                intensity_scale_factor, color_scale_factor = 1.2, 1.2
                L_scale = random.uniform(1. / intensity_scale_factor, intensity_scale_factor)
                a_scale = random.uniform(1. / color_scale_factor, color_scale_factor)
                b_scale = random.uniform(1. / color_scale_factor, color_scale_factor)
                img_lab[..., 0] *= L_scale
                img_lab[..., 1] *= a_scale
                img_lab[..., 2] *= b_scale
            img_lab[..., 0] = (img_lab[..., 0] - 50) / 50.
            img_lab[..., 1:] /= 128.
            images_arr[file_index] = img_lab
        else:
            images_arr[file_index] = img.astype(np.float32)
        
    return ori_images_arr, images_arr, lm_pts

 
def get_mask_of_regions_aux_loss(img_size, disc_center, macula_center, regions):
    region_codes = ["None", "M", "T", "ST", "IT", "SN", "IN", "SD", "ID"]
    n_findings, n_region_codes = regions.shape
    mask = np.zeros((n_findings,) + img_size)
    for finding_index in range(n_findings):
        for region_code_index in range(1, n_region_codes):
            if regions[finding_index, region_code_index] == 1:
                mask[finding_index][(mask[finding_index] == 1) | (mask_region.mask(img_size, disc_center, macula_center, region_codes[region_code_index]) == 1)] = 1
    return mask


def load_augmented_aux_loss(fname, lms, regions, normalize, augment, mask_shape=(16, 16)):
    # read image file
    ori_img, img_aug, lms_aug = load_imgs_lms([fname], [lms], normalize, augment)
    ori_img = ori_img[0, ...]
    img_aug = img_aug[0, ...]
    lms_aug = lms_aug[0, ...]
    ratio = 1.*np.array(mask_shape) / np.array(ori_img.shape[:2])
    region_mask = get_mask_of_regions_aux_loss(mask_shape, (lms_aug[0] * ratio[0], lms_aug[1] * ratio[1]), (lms_aug[2] * ratio[0], lms_aug[3] * ratio[1]), regions)
    for finding_index in range(regions.shape[0]):
        region_mask[finding_index, ori_img[::int(1. / ratio[0]), ::int(1. / ratio[1]), 0] < 10] = 0
    assert len(img_aug.shape) == 3 and lms_aug.shape == (4,) and len(region_mask.shape) == 3
    return img_aug, lms_aug, region_mask


def outputs2labels(outputs, min_val, max_val):
    return np.clip(np.round(outputs), min_val, max_val)


def print_stats(y_true, y_pred):
    y_true = np.array(y_true)
    y_pred = np.array(y_pred)
    if len(y_true[y_true == 1]) > 0:
        cm = confusion_matrix(y_true, outputs2labels(y_pred, 0, 1))
        spe = 1.*cm[0, 0] / (cm[0, 1] + cm[0, 0])
        sen = 1.*cm[1, 1] / (cm[1, 0] + cm[1, 1])
        HM = 2 * spe * sen / (spe + sen)
        AUC_ROC = roc_auc_score(y_true, y_pred)
        print(cm)
        print("specificity : {},  sensitivity : {}, harmonic mean : {},  ROC_AUC : {} ".format(spe, sen, HM, AUC_ROC))   
    else:
        print("specificity: {}".format(1.*len(y_true)))


def auroc(y_true, y_pred):
    return roc_auc_score(y_true, y_pred)
    

def get_metric(metrics, y_true, y_pred):
    values = []
    for metric in metrics:
        if metric == "HM":
            cm = confusion_matrix(y_true, outputs2labels(y_pred, 0, 1))
            spe = 1.*cm[0, 0] / (cm[0, 1] + cm[0, 0])
            sen = 1.*cm[1, 1] / (cm[1, 0] + cm[1, 1])
            HM = 2 * spe * sen / (spe + sen)
            values.append(HM)
        elif metric == "auroc":
            AUC_ROC = roc_auc_score(y_true, y_pred)
            values.append(AUC_ROC)

    return values


def plot_imgs(imgs, out_dir):
    if not os.path.isdir(out_dir):
        os.makedirs(out_dir)
    for i in range(imgs.shape[0]):
        Image.fromarray((imgs[i, ...]).astype(np.uint8)).save(os.path.join(out_dir, "imgs_{}.png".format(i + 1)))
        Image.fromarray(imgs[i, ::32, ::32].astype(np.uint8)).save(os.path.join(out_dir, "imgs_discrete_{}.png".format(i + 1)))
        resized_img = imresize(imgs[i, ...], (16, 16), 'bilinear')
        Image.fromarray(resized_img.astype(np.uint8)).save(os.path.join(out_dir, "imgs_resized_{}.png".format(i + 1)))

        
class Scheduler:

    def __init__(self, schedules, init_lr):
        self.schedules = schedules
        self.lr = init_lr

    def get_lr(self):
        return self.lr
        
    def update_steps(self, n_round):
        key = str(n_round)
        if key in self.schedules['lr']:
            self.lr = self.schedules['lr'][key]

