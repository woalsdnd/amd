# python2.7
# python inference_AMD.py  --gpu_index=3 --load_model_path=../models/miccai/miccai_AMD_AMD/weight_best_training_loss.h5 --validation=True
import utils
import os
import argparse
import time
from keras import backend as K
import sys
import iterator_diagnosis
import models
import numpy as np
import random
import pandas as pd
import efficient_net_modify

from PIL import Image


list_findings = ["Hemorrhage", "HardExudate", "CWP", "Drusen", "RetinalPigmentaryChange", "VascularAbnormality", "Membrane", "FluidAccumulation",
                 "ChroioretinalAtrophy", "ChoroidalLesion", "MyelinatedNerveFiber", "RNFLDefect", "GlaucomatousDiscChange", "NonGlaucomatousDiscChange", "MacularHole"]

list_diagnoses = ['EarlyDR', 'AdvancedDR', 'BRVOHemiCRVO', 'CRVO', 'DryAMD', 'WetAMD', 'GlaucomaSuspect', 'EpiretinalMembrane']  # ignore macular hole

label_options = {"Hemorrhage": "Hemorrhage", "HardExudate":'Hard Exudate', "CWP":'Cotton Wool Patch',
         "Drusen":'Drusen & Drusenoid Deposits', "RetinalPigmentaryChange":'Retinal Pigmentary Change',
          "MacularHole":'Macular Hole', "VascularAbnormality":'Vascular Abnormality', "Membrane":'Membrane',
          "FluidAccumulation":'Fluid Accumulation', "ChroioretinalAtrophy":'Chroioretinal Atrophy/Scar',
          "ChoroidalLesion":'Choroidal Lesion', "MyelinatedNerveFiber":'Myelinated Nerve Fiber',
          "RNFLDefect":'RNFL Defect', "GlaucomatousDiscChange":'Glaucomatous Disc Change',
          "NonGlaucomatousDiscChange":'Non-glaucomatous Disc Change',
          "DryAMD": "Dry AMD", "WetAMD": "Wet AMD", "EarlyDR":"Early DR", "AdvancedDR":"Advanced DR", "DR":"DR", "CRVO":"CRVO",
          "BRVOHemiCRVO":"BRVO / Hemi-CRVO", "EpiretinalMembrane":"Epiretinal membrane", "GlaucomaSuspect":"Glaucoma Suspect"}

# arrange arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    '--gpu_index',
    type=int,
    help="GPU index",
    required=True
    )
parser.add_argument(
    '--validation',
    type=str,
    required=False,
    default=None
    )
parser.add_argument(
    '--Test',
    type=str,
    required=False,
    default=None
    )
FLAGS, _ = parser.parse_known_args()

# set lambda, gpu, path
os.environ['CUDA_VISIBLE_DEVICES'] = str(FLAGS.gpu_index)
list_weight_path = utils.all_files_under("../AMD/model_selected/")
#list_weight_path = ["../AMD/model_selected/weight_19th_epoch.h5",  "../AMD/model_selected/weight_26th_epoch.h5",  "../AMD/model_selected/weight_34th_epoch.h5", "../AMD/model_selected/weight_42th_epoch.h5"]

# create & save network
img_shape = (1024, 1024, 3)
list_network = []
for weight_path in list_weight_path:
    network = efficient_net_modify.EfficientNet_multiheads(input_shape=img_shape)
    network = models.network_diagnosis(network, "AMD", 0, 0)
    network.load_weights(weight_path)
    print("model loaded from {}".format(weight_path))
    list_network.append(network)

# training settings
if FLAGS.validation == "True":
    fpaths = utils.all_files_under("/media/ext/external_data/AMD/Test-400-images/") if FLAGS.Test=="True" else utils.all_files_under("/media/ext/external_data/AMD/Validation-400-images/")
    list_pred = []
    for fpath in fpaths:
        img = np.array(Image.open(fpath))
        resized_img = utils.crop_resize(img, img_shape[0], img_shape[1])
        resized_img = 1.*resized_img / 255
        network_input = np.expand_dims(resized_img, axis=0)
        outputs=[]
        for network in list_network:
            pred = network.predict(network_input)[0,0]
            outputs.append(pred)
        list_pred.append(np.mean(outputs))
        print(fpath, pred)
    df = pd.DataFrame({"FileName":[os.path.basename(fpath) for fpath in fpaths], "AMD Risk":list_pred})
    df.to_csv("../AMD/AMD.csv", index=False)
else:
    df_label = pd.read_csv("/media/ext/external_data/AMD/AMD.csv")
    df_label.loc[:, "filename"] = df_label["filename"].map(lambda x:os.path.join("/media/ext/external_data/AMD/resized_1024x1024", x))

    # set -1 to nan (-1 will be ignored in loss computation)
    df_label.loc[:,"sample_weight"]=None
    batch_fetcher = {"val": iterator_diagnosis.BatchFetcher(df_label, 1, "normalize", augment=False)}

    # evaluate on val set
    list_diagnosis_target, list_diagnosis_pred = [], []
    for fns, batch_x, diagnosis_labels in batch_fetcher["val"]():
        preds = network.predict(batch_x)
        list_diagnosis_target += list(diagnosis_labels)
        list_diagnosis_pred += list(preds[:, 0])
    true_labels = np.array(list_diagnosis_target)
    pred_labels = np.array(list_diagnosis_pred)
    utils.print_stats(true_labels[true_labels != -1], pred_labels[true_labels != -1])
