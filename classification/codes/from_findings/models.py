# python2.7
import os

import numpy as np
import tensorflow as tf

from keras import backend as K
from keras.layers import Conv2D, GlobalAveragePooling2D, MaxPooling2D, Dense, Add
from keras.layers.core import Activation
from keras.models import Model
from keras.optimizers import Adam, SGD
from keras.optimizers import Optimizer
from keras.legacy import interfaces
from keras import objectives
from keras.layers.merge import Concatenate

from keras.layers.core import Lambda
from keras.initializers import RandomUniform

os.environ['KERAS_BACKEND'] = 'tensorflow'
import keras.backend as K
from keras.legacy import interfaces
from keras.optimizers import Optimizer
from keras import regularizers


def set_output_findings_diagnoses_normality(network):
    list_findings = ["Hemorrhage", "HardExudate", "CWP", "Drusen", "RetinalPigmentaryChange", "VascularAbnormality", "Membrane", "FluidAccumulation",
                     "ChroioretinalAtrophy", "ChoroidalLesion", "MyelinatedNerveFiber", "RNFLDefect", "GlaucomatousDiscChange", "NonGlaucomatousDiscChange", "MacularHole"]
    list_diagnoses = ['EarlyDR', 'AdvancedDR', 'BRVOHemiCRVO', 'CRVO', 'DryAMD', 'WetAMD', 'GlaucomaSuspect', 'EpiretinalMembrane']  # ignore macular hole

    list_GAP_layers = []
    for finding in list_findings:
        list_GAP_layers.append(GlobalAveragePooling2D()(network.get_layer("top_activation_{}".format(finding)).output))
    featuremap_concated = Concatenate(axis=1)(list_GAP_layers)
    outputs = Dense(1, activation="sigmoid", name="dense_normality")(featuremap_concated)
    diagnosis_model = Model(network.input, network.output + [outputs])
    
    return diagnosis_model


def set_output_findings_diagnoses_miccai(network):
    list_findings = ["Hemorrhage", "HardExudate", "CWP", "Drusen", "RetinalPigmentaryChange", "VascularAbnormality", "Membrane", "FluidAccumulation",
                     "ChroioretinalAtrophy", "ChoroidalLesion", "MyelinatedNerveFiber", "RNFLDefect", "GlaucomatousDiscChange", "NonGlaucomatousDiscChange", "MacularHole"]
    list_diagnoses = ['EarlyDR', 'AdvancedDR', 'AMD', 'Glaucoma']  # ignore macular hole

    list_GAP_layers = []
    for finding in list_findings:
        list_GAP_layers.append(GlobalAveragePooling2D()(network.get_layer("top_activation_{}".format(finding)).output))
    featuremap_concated = Concatenate(axis=1)(list_GAP_layers)
    list_diagnosis_outputs = []
    for diagnosis in list_diagnoses:
        outputs = Dense(1, activation="sigmoid", name="dense_{}".format(diagnosis))(featuremap_concated)
        list_diagnosis_outputs.append(outputs)
    diagnosis_model = Model(network.input, network.output + list_diagnosis_outputs)
    
    return diagnosis_model


def set_output_findings_diagnoses(network):
    list_findings = ["Hemorrhage", "HardExudate", "CWP", "Drusen", "RetinalPigmentaryChange", "VascularAbnormality", "Membrane", "FluidAccumulation",
                     "ChroioretinalAtrophy", "ChoroidalLesion", "MyelinatedNerveFiber", "RNFLDefect", "GlaucomatousDiscChange", "NonGlaucomatousDiscChange", "MacularHole"]
    list_diagnoses = ['EarlyDR', 'AdvancedDR', 'BRVOHemiCRVO', 'CRVO', 'DryAMD', 'WetAMD', 'GlaucomaSuspect', 'EpiretinalMembrane']  # ignore macular hole

    list_GAP_layers = []
    for finding in list_findings:
        list_GAP_layers.append(GlobalAveragePooling2D()(network.get_layer("top_activation_{}".format(finding)).output))
    featuremap_concated = Concatenate(axis=1)(list_GAP_layers)
    list_diagnosis_outputs = []
    for diagnosis in list_diagnoses:
        outputs = Dense(1, activation="sigmoid", name="dense_{}".format(diagnosis))(featuremap_concated)
        list_diagnosis_outputs.append(outputs)
    diagnosis_model = Model(network.input, network.output + list_diagnosis_outputs)
    
    return diagnosis_model


def set_output_findings_diagnoses_from_finding_outputs(network):
    list_findings = ["Hemorrhage", "HardExudate", "CWP", "Drusen", "RetinalPigmentaryChange", "VascularAbnormality", "Membrane", "FluidAccumulation",
                     "ChroioretinalAtrophy", "ChoroidalLesion", "MyelinatedNerveFiber", "RNFLDefect", "GlaucomatousDiscChange", "NonGlaucomatousDiscChange", "MacularHole"]
    list_diagnoses = ['EarlyDR', 'AdvancedDR', 'BRVOHemiCRVO', 'CRVO', 'DryAMD', 'WetAMD', 'GlaucomaSuspect', 'EpiretinalMembrane']  # ignore macular hole

    list_GAP_layers = []
    for finding in list_findings:
        list_GAP_layers.append(GlobalAveragePooling2D()(network.get_layer("1x1conv_{}".format(finding)).output))
    featuremap_concated = Concatenate(axis=1)(list_GAP_layers)
    list_diagnosis_outputs = []
    for diagnosis in list_diagnoses:
        outputs = Dense(1, activation="sigmoid", name="dense_{}".format(diagnosis))(featuremap_concated)
        list_diagnosis_outputs.append(outputs)
    diagnosis_model = Model(network.input, network.output + list_diagnosis_outputs)
    
    return diagnosis_model


def network_diagnosis_finding_output(network, diagnosis, accum_iters):
    list_findings = ["Hemorrhage", "HardExudate", "CWP", "Drusen", "RetinalPigmentaryChange", "VascularAbnormality", "Membrane", "FluidAccumulation",
                     "ChroioretinalAtrophy", "ChoroidalLesion", "MyelinatedNerveFiber", "RNFLDefect", "GlaucomatousDiscChange", "NonGlaucomatousDiscChange", "MacularHole"]
    list_GAP_layers = []
    for finding in list_findings:
        list_GAP_layers.append(GlobalAveragePooling2D()(network.get_layer("1x1conv_{}".format(finding)).output))
    featuremap_concated = Concatenate(axis=1)(list_GAP_layers)
    outputs = Dense(1, activation="sigmoid", name="dense_{}".format(diagnosis), kernel_regularizer=regularizers.l2(5e-4), bias_regularizer=regularizers.l2(5e-4))(featuremap_concated)
                          
    diagnosis_model = Model(network.input, outputs)
    if accum_iters > 0:
        SGD_gradient_accumulation = SGDAccum(lr=0, momentum=0.9, nesterov=True, accum_iters=accum_iters)
        diagnosis_model.compile(optimizer=SGD_gradient_accumulation, loss=objectives.binary_crossentropy, metrics=['accuracy'])
    else:        
        diagnosis_model.compile(optimizer=SGD(lr=0, momentum=0.9, nesterov=True), loss=objectives.binary_crossentropy, metrics=['accuracy'])
    
    return diagnosis_model


def network_diagnosis(network, diagnosis, lr_start_value, accum_iters):
    list_findings = ["Hemorrhage", "HardExudate", "CWP", "Drusen", "RetinalPigmentaryChange", "VascularAbnormality", "Membrane", "FluidAccumulation",
                     "ChroioretinalAtrophy", "ChoroidalLesion", "MyelinatedNerveFiber", "RNFLDefect", "GlaucomatousDiscChange", "NonGlaucomatousDiscChange", "MacularHole"]
    list_GAP_layers = []
    for finding in list_findings:
        list_GAP_layers.append(GlobalAveragePooling2D()(network.get_layer("top_activation_{}".format(finding)).output))
    featuremap_concated = Concatenate(axis=1)(list_GAP_layers)
    outputs = Dense(1, activation="sigmoid", name="dense_{}".format(diagnosis), kernel_regularizer=regularizers.l2(5e-4), bias_regularizer=regularizers.l2(5e-4))(featuremap_concated)
                          
    diagnosis_model = Model(network.input, outputs)
    if accum_iters > 0:
        SGD_gradient_accumulation = SGDAccum(lr=lr_start_value, momentum=0.9, nesterov=True, accum_iters=accum_iters)
        diagnosis_model.compile(optimizer=SGD_gradient_accumulation, loss=objectives.binary_crossentropy, metrics=['accuracy'])
    else:        
        diagnosis_model.compile(optimizer=SGD(lr=lr_start_value, momentum=0.9, nesterov=True), loss=objectives.binary_crossentropy, metrics=['accuracy'])
    
    return diagnosis_model


def network_diagnosis_activity_regularizer(network, diagnosis, accum_iters):
    list_findings = ["Hemorrhage", "HardExudate", "CWP", "Drusen", "RetinalPigmentaryChange", "VascularAbnormality", "Membrane", "FluidAccumulation",
                     "ChroioretinalAtrophy", "ChoroidalLesion", "MyelinatedNerveFiber", "RNFLDefect", "GlaucomatousDiscChange", "NonGlaucomatousDiscChange", "MacularHole"]
    list_outputs_finding = []
    for finding in list_findings:
        gap_finding = GlobalAveragePooling2D()(network.get_layer("top_activation_{}".format(finding)).output)
        weight_limit = np.sqrt(6 / (len(list_findings) * gap_finding._keras_shape[1] + 1))
        output_finding = Dense(1, name="dense_{}_{}".format(diagnosis, finding), kernel_regularizer=regularizers.l2(5e-4),
                               bias_regularizer=regularizers.l2(5e-4), activity_regularizer=regularizers.l1(1e-3),
                               kernel_initializer=RandomUniform(minval=-weight_limit, maxval=weight_limit))(gap_finding)
        list_outputs_finding.append(output_finding)
    sum_outputs_finding = Add()(list_outputs_finding)
    outputs = Activation("sigmoid")(sum_outputs_finding)
                          
    diagnosis_model = Model(network.input, outputs)
    if accum_iters > 0:
        SGD_gradient_accumulation = SGDAccum(lr=0, momentum=0.9, nesterov=True, accum_iters=accum_iters)
        diagnosis_model.compile(optimizer=SGD_gradient_accumulation, loss=objectives.binary_crossentropy, metrics=['accuracy'])
    else:        
        diagnosis_model.compile(optimizer=SGD(lr=0, momentum=0.9, nesterov=True), loss=objectives.binary_crossentropy, metrics=['accuracy'])
    
    return diagnosis_model


class RAdamAccumulate(Optimizer):
    """RAdam optimizer with gradient accumulation.
    # Arguments
        learning_rate: float >= 0. Learning rate.
        beta_1: float, 0 < beta < 1. Generally close to 1.
        beta_2: float, 0 < beta < 1. Generally close to 1.
        epsilon: float >= 0. Fuzz factor. If `None`, defaults to `K.epsilon()`.
        decay: float >= 0. Learning rate decay over each update.
        weight_decay: float >= 0. Weight decay for each param.
        amsgrad: boolean. Whether to apply the AMSGrad variant of this
            algorithm from the paper "On the Convergence of Adam and
            Beyond".
        total_steps: int >= 0. Total number of training steps. Enable warmup by setting a positive value.
        warmup_proportion: 0 < warmup_proportion < 1. The proportion of increasing steps.
        min_lr: float >= 0. Minimum learning rate after warmup.
    # References
        - [Adam - A Method for Stochastic Optimization](https://arxiv.org/abs/1412.6980v8)
        - [On the Convergence of Adam and Beyond](https://openreview.net/forum?id=ryQu7f-RZ)
        - [On The Variance Of The Adaptive Learning Rate And Beyond](https://arxiv.org/pdf/1908.03265v1.pdf)
    """

    def __init__(self, learning_rate=0.001, beta_1=0.9, beta_2=0.999,
                 epsilon=None, decay=0., weight_decay=0., amsgrad=False, accum_iters=1,
                 total_steps=0, warmup_proportion=0.1, min_lr=0., **kwargs):
        if accum_iters < 1:
            raise ValueError('accum_iters must be >= 1')
        learning_rate = kwargs.pop('lr', learning_rate)
        super(RAdamAccumulate, self).__init__(**kwargs)
        with K.name_scope(self.__class__.__name__):
            self.iterations = K.variable(0, dtype='int64', name='iterations')
            self.learning_rate = K.variable(learning_rate, name='learning_rate')
            self.beta_1 = K.variable(beta_1, name='beta_1')
            self.beta_2 = K.variable(beta_2, name='beta_2')
            self.decay = K.variable(decay, name='decay')
            self.weight_decay = K.variable(weight_decay, name='weight_decay')
            self.total_steps = K.variable(total_steps, name='total_steps')
            self.warmup_proportion = K.variable(warmup_proportion, name='warmup_proportion')
            self.min_lr = K.variable(min_lr, name='min_lr')
        if epsilon is None:
            epsilon = K.epsilon()
        self.epsilon = epsilon
        self.initial_decay = decay
        self.initial_weight_decay = weight_decay
        self.initial_total_steps = total_steps
        self.amsgrad = amsgrad
        self.accum_iters = K.variable(accum_iters, K.dtype(self.iterations))
        self.accum_iters_float = K.cast(self.accum_iters, K.floatx())

    def get_updates(self, loss, params):
        grads = self.get_gradients(loss, params)
        self.updates = [K.update_add(self.iterations, 1)]

        lr = self.lr
        
        completed_updates = K.cast(self.iterations / self.accum_iters, K.floatx())

        if self.initial_decay > 0:
            lr = lr * (1. / (1. + self.decay * completed_updates))

        t = completed_updates + 1

        lr_t = lr * (K.sqrt(1. - K.pow(self.beta_2, t)) / (1. - K.pow(self.beta_1, t)))

        # self.iterations incremented after processing a batch
        # batch:              1 2 3 4 5 6 7 8 9
        # self.iterations:    0 1 2 3 4 5 6 7 8
        # update_switch = 1:        x       x    (if accum_iters=4)  
        update_switch = K.equal((self.iterations + 1) % self.accum_iters, 0)
        update_switch = K.cast(update_switch, K.floatx())
        
        if self.initial_total_steps > 0:
            warmup_steps = self.total_steps * self.warmup_proportion
            decay_steps = K.maximum(self.total_steps - warmup_steps, 1)
            decay_rate = (self.min_lr - lr) / decay_steps
            lr = K.switch(
                t <= warmup_steps,
                lr * (t / warmup_steps),
                lr + decay_rate * K.minimum(t - warmup_steps, decay_steps),
            )

        ms = [K.zeros(K.int_shape(p), dtype=K.dtype(p), name='m_' + str(i)) for (i, p) in enumerate(params)]
        vs = [K.zeros(K.int_shape(p), dtype=K.dtype(p), name='v_' + str(i)) for (i, p) in enumerate(params)]
        gs = [K.zeros(K.int_shape(p), dtype=K.dtype(p), name='g_' + str(i)) for (i, p) in enumerate(params)]

        if self.amsgrad:
            vhats = [K.zeros(K.int_shape(p), dtype=K.dtype(p), name='vhat_' + str(i)) for (i, p) in enumerate(params)]
        else:
            vhats = [K.zeros(1, name='vhat_' + str(i)) for i in range(len(params))]

        self.weights = [self.iterations] + ms + vs + vhats

        beta_1_t = K.pow(self.beta_1, t)
        beta_2_t = K.pow(self.beta_2, t)

        sma_inf = 2.0 / (1.0 - self.beta_2) - 1.0
        sma_t = sma_inf - 2.0 * t * beta_2_t / (1.0 - beta_2_t)

        for p, g, m, v, vhat, tg in zip(params, grads, ms, vs, vhats, gs):
            sum_grad = tg + g
            avg_grad = sum_grad / self.accum_iters_float

            m_t = (self.beta_1 * m) + (1. - self.beta_1) * avg_grad
            v_t = (self.beta_2 * v) + (1. - self.beta_2) * K.square(avg_grad)

            m_corr_t = m_t / (1.0 - beta_1_t)
            if self.amsgrad:
                vhat_t = K.maximum(vhat, v_t)
                v_corr_t = K.sqrt(vhat_t / (1.0 - beta_2_t))
                self.updates.append(K.update(vhat, vhat_t))
            else:
                v_corr_t = K.sqrt(v_t / (1.0 - beta_2_t))

            r_t = K.sqrt((sma_t - 4.0) / (sma_inf - 4.0) * 
                         (sma_t - 2.0) / (sma_inf - 2.0) * 
                         sma_inf / sma_t)

            p_t = K.switch(sma_t >= 5, r_t * m_corr_t / (v_corr_t + self.epsilon), m_corr_t)

            if self.initial_weight_decay > 0:
                p_t += self.weight_decay * p

            p_t = p - lr * p_t

            self.updates.append(K.update(m, (1 - update_switch) * m + update_switch * m_t))
            self.updates.append(K.update(v, (1 - update_switch) * v + update_switch * v_t))
            self.updates.append(K.update(tg, (1 - update_switch) * sum_grad))
            new_p = p_t

            # Apply constraints.
            if getattr(p, 'constraint', None) is not None:
                new_p = p.constraint(new_p)

            self.updates.append(K.update(p, (1 - update_switch) * p + update_switch * new_p))

        return self.updates

    @property
    def lr(self):
        return self.learning_rate

    @lr.setter
    def lr(self, learning_rate):
        self.learning_rate = learning_rate

    def get_config(self):
        config = {
            'learning_rate': float(K.get_value(self.learning_rate)),
            'beta_1': float(K.get_value(self.beta_1)),
            'beta_2': float(K.get_value(self.beta_2)),
            'decay': float(K.get_value(self.decay)),
            'weight_decay': float(K.get_value(self.weight_decay)),
            'epsilon': self.epsilon,
            'amsgrad': self.amsgrad,
            'total_steps': float(K.get_value(self.total_steps)),
            'warmup_proportion': float(K.get_value(self.warmup_proportion)),
            'min_lr': float(K.get_value(self.min_lr)),
        }
        base_config = super(RAdam, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))


class AdamAccumulate(Optimizer):

    def __init__(self, lr=0.001, beta_1=0.9, beta_2=0.999,
                 epsilon=None, decay=0., amsgrad=False, accum_iters=1, **kwargs):
        if accum_iters < 1:
            raise ValueError('accum_iters must be >= 1')
        super(AdamAccumulate, self).__init__(**kwargs)
        with K.name_scope(self.__class__.__name__):
            self.iterations = K.variable(0, dtype='int64', name='iterations')
            self.lr = K.variable(lr, name='lr')
            self.beta_1 = K.variable(beta_1, name='beta_1')
            self.beta_2 = K.variable(beta_2, name='beta_2')
            self.decay = K.variable(decay, name='decay')
        if epsilon is None:
            epsilon = K.epsilon()
        self.epsilon = epsilon
        self.initial_decay = decay
        self.amsgrad = amsgrad
        self.accum_iters = K.variable(accum_iters, K.dtype(self.iterations))
        self.accum_iters_float = K.cast(self.accum_iters, K.floatx())

    @interfaces.legacy_get_updates_support
    def get_updates(self, loss, params):
        grads = self.get_gradients(loss, params)
        self.updates = [K.update_add(self.iterations, 1)]

        lr = self.lr

        completed_updates = K.cast(self.iterations / self.accum_iters, K.floatx())

        if self.initial_decay > 0:
            lr = lr * (1. / (1. + self.decay * completed_updates))

        t = completed_updates + 1

        lr_t = lr * (K.sqrt(1. - K.pow(self.beta_2, t)) / (1. - K.pow(self.beta_1, t)))

        # self.iterations incremented after processing a batch
        # batch:              1 2 3 4 5 6 7 8 9
        # self.iterations:    0 1 2 3 4 5 6 7 8
        # update_switch = 1:        x       x    (if accum_iters=4)  
        update_switch = K.equal((self.iterations + 1) % self.accum_iters, 0)
        update_switch = K.cast(update_switch, K.floatx())

        ms = [K.zeros(K.int_shape(p), dtype=K.dtype(p)) for p in params]
        vs = [K.zeros(K.int_shape(p), dtype=K.dtype(p)) for p in params]
        gs = [K.zeros(K.int_shape(p), dtype=K.dtype(p)) for p in params]

        if self.amsgrad:
            vhats = [K.zeros(K.int_shape(p), dtype=K.dtype(p)) for p in params]
        else:
            vhats = [K.zeros(1) for _ in params]

        self.weights = [self.iterations] + ms + vs + vhats

        for p, g, m, v, vhat, tg in zip(params, grads, ms, vs, vhats, gs):

            sum_grad = tg + g
            avg_grad = sum_grad / self.accum_iters_float

            m_t = (self.beta_1 * m) + (1. - self.beta_1) * avg_grad
            v_t = (self.beta_2 * v) + (1. - self.beta_2) * K.square(avg_grad)

            if self.amsgrad:
                vhat_t = K.maximum(vhat, v_t)
                p_t = p - lr_t * m_t / (K.sqrt(vhat_t) + self.epsilon)
                self.updates.append(K.update(vhat, (1 - update_switch) * vhat + update_switch * vhat_t))
            else:
                p_t = p - lr_t * m_t / (K.sqrt(v_t) + self.epsilon)

            self.updates.append(K.update(m, (1 - update_switch) * m + update_switch * m_t))
            self.updates.append(K.update(v, (1 - update_switch) * v + update_switch * v_t))
            self.updates.append(K.update(tg, (1 - update_switch) * sum_grad))
            new_p = p_t

            # Apply constraints.
            if getattr(p, 'constraint', None) is not None:
                new_p = p.constraint(new_p)

            self.updates.append(K.update(p, (1 - update_switch) * p + update_switch * new_p))
        return self.updates

    def get_config(self):
        config = {'lr': float(K.get_value(self.lr)),
                  'beta_1': float(K.get_value(self.beta_1)),
                  'beta_2': float(K.get_value(self.beta_2)),
                  'decay': float(K.get_value(self.decay)),
                  'epsilon': self.epsilon,
                  'amsgrad': self.amsgrad}
        base_config = super(AdamAccumulate, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))


class SGDAccum(Optimizer):
    """Stochastic gradient descent optimizer.

    Includes support for momentum,
    learning rate decay, and Nesterov momentum.

    # Arguments
        lr: float >= 0. Learning rate.
        momentum: float >= 0. Parameter updates momentum.
        decay: float >= 0. Learning rate decay over each update.
        nesterov: boolean. Whether to apply Nesterov momentum.
    """

    def __init__(self, lr=0, momentum=0., decay=0.,
                 nesterov=False, accum_iters=1, **kwargs):
        super(SGDAccum, self).__init__(**kwargs)
        with K.name_scope(self.__class__.__name__):
            self.iterations = K.variable(0, name='iterations')
            self.lr = K.variable(lr, name='lr')
            self.momentum = K.variable(momentum, name='momentum')
            self.decay = K.variable(decay, name='decay')
            self.accum_iters = K.variable(accum_iters)
        self.initial_decay = decay
        self.nesterov = nesterov

    @interfaces.legacy_get_updates_support
    def get_updates(self, loss, params):
        grads = self.get_gradients(loss, params)
        self.updates = [K.update_add(self.iterations, 1)]

        lr = self.lr
        if self.initial_decay > 0:
            lr *= (1. / (1. + self.decay * K.cast(self.iterations,
                                                  K.dtype(self.decay))))

        accum_switch = K.equal(self.iterations % self.accum_iters, 0)
        accum_switch = K.cast(accum_switch, dtype='float32')

        # momentum
        shapes = [K.int_shape(p) for p in params]
        moments = [K.zeros(shape) for shape in shapes]
        temp_grads = [K.zeros(shape) for shape in shapes]
        self.weights = [self.iterations] + moments
        for p, cg, m, tg in zip(params, grads, moments, temp_grads):
            g = cg + tg
            v = self.momentum * m - (lr * g / self.accum_iters)  # velocity
            self.updates.append(K.update(m, (1 - accum_switch) * m + accum_switch * v))
            self.updates.append(K.update(tg, (1 - accum_switch) * g))

            if self.nesterov:
                new_p = p + self.momentum * v - (lr * g / self.accum_iters)
            else:
                new_p = p + v

            # Apply constraints.
            if getattr(p, 'constraint', None) is not None:
                new_p = p.constraint(new_p)

            self.updates.append(K.update(p, (1 - accum_switch) * p + accum_switch * new_p))
        return self.updates

    def get_config(self):
        config = {'lr': float(K.get_value(self.lr)),
                  'momentum': float(K.get_value(self.momentum)),
                  'decay': float(K.get_value(self.decay)),
                  'nesterov': self.nesterov,
                  'accum_iters': int(K.get_value(self.accum_iters))}
        base_config = super(SGDAccum, self).get_config()
        return dict(list(base_config.items()) + list(config.items()))


def set_optimizer_all_in_one(network, guide_weight, n_output=15, accum_iters=0, relax="both", loss_pos_weights=[10] * 15):
    
    def weighted_relaxed_bce_per_finding(loss_pos_weight):

        def weighted_relaxed_bce(y_true, y_pred):
            legit_labels = K.cast(K.not_equal(y_true, -1), 'float32')
            less_than_upper_limit = K.cast(K.less(y_pred, 0.9), 'float32')
            greater_than_lower_limit = K.cast(K.greater(y_pred, 0.1), 'float32')
            mask_true_positive = K.cast(K.equal(y_true, 1), 'float32')
            mask_true_negative = K.cast(K.equal(y_true, 0), 'float32')
            positive_up = loss_pos_weight
    
            if relax == "both":
                weighted_relaxed_bce_loss = -legit_labels * less_than_upper_limit * positive_up * y_true * K.log(y_pred + K.epsilon()) - legit_labels * greater_than_lower_limit * (1 - y_true) * K.log(1 - y_pred + K.epsilon())
            elif relax == "neg":
                weighted_relaxed_bce_loss = -legit_labels * positive_up * y_true * K.log(y_pred + K.epsilon()) - legit_labels * greater_than_lower_limit * (1 - y_true) * K.log(1 - y_pred + K.epsilon())
            elif relax == "pos":
                weighted_relaxed_bce_loss = -legit_labels * less_than_upper_limit * positive_up * y_true * K.log(y_pred + K.epsilon()) - legit_labels * (1 - y_true) * K.log(1 - y_pred + K.epsilon())
            elif relax == "none":
                weighted_relaxed_bce_loss = -legit_labels * positive_up * y_true * K.log(y_pred + K.epsilon()) - legit_labels * (1 - y_true) * K.log(1 - y_pred + K.epsilon())
            
            return weighted_relaxed_bce_loss

        return weighted_relaxed_bce

    def loss_seg(y_true, y_pred):
        return K.mean(K.sum((1 - y_true) * K.log(K.maximum(y_pred, 0.01)), axis=3), axis=(1, 2))

    list_loss = list(np.array(zip([weighted_relaxed_bce_per_finding(loss_pos_weights[index]) for index in range(n_output)], [loss_seg] * n_output)).flatten())
    list_loss_weights = [1, guide_weight] * n_output
    if accum_iters > 0:
#         optimizer = RAdamAccumulate(lr=0, accum_iters=accum_iters)
#         optimizer = AdamAccumulate(lr=0, accum_iters=accum_iters)
        optimizer = SGDAccum(lr=0, momentum=0.9, nesterov=True, accum_iters=accum_iters)
        network.compile(optimizer=optimizer, loss=list_loss, loss_weights=list_loss_weights, metrics=['accuracy'])
    else:        
        network.compile(optimizer=SGD(lr=0, momentum=0.9, nesterov=True), loss=list_loss, loss_weights=list_loss_weights, metrics=['accuracy'])

    return network


def set_optimizer_all_in_one_focal_loss(network, guide_weight, n_output=15, accum_iters=0, relax="both", loss_pos_weights=[10] * 15):
    
    def weighted_relaxed_bce_per_finding(loss_pos_weight):

        def focal_loss(y_true, y_pred):
            legit_labels = K.cast(K.not_equal(y_true, -1), 'float32')
            less_than_upper_limit = K.cast(K.less(y_pred, 0.9), 'float32')
            greater_than_lower_limit = K.cast(K.greater(y_pred, 0.1), 'float32')
            mask_true_positive = K.cast(K.equal(y_true, 1), 'float32')
            mask_true_negative = K.cast(K.equal(y_true, 0), 'float32')
            positive_up = loss_pos_weight
    
            gamma = 2
            alpha = 0.25
    
            loss = -legit_labels * alpha * K.pow(1. - y_pred, gamma) * positive_up * y_true * K.log(y_pred + K.epsilon()) - legit_labels * (1 - alpha) * K.pow(y_pred, gamma) * (1 - y_true) * K.log(1 - y_pred + K.epsilon())
            
            return loss

        return focal_loss

    def loss_seg(y_true, y_pred):
        return K.mean(K.sum((1 - y_true) * K.log(K.maximum(y_pred, 0.01)), axis=3), axis=(1, 2))

    list_loss = list(np.array(zip([weighted_relaxed_bce_per_finding(loss_pos_weights[index]) for index in range(n_output)], [loss_seg] * n_output)).flatten())
    list_loss_weights = [1, guide_weight] * n_output
    if accum_iters > 0:
        optimizer = SGDAccum(lr=0, momentum=0.9, nesterov=True, accum_iters=accum_iters)
        network.compile(optimizer=optimizer, loss=list_loss, loss_weights=list_loss_weights, metrics=['accuracy'])
    else:        
        network.compile(optimizer=SGD(lr=0, momentum=0.9, nesterov=True), loss=list_loss, loss_weights=list_loss_weights, metrics=['accuracy'])

    return network


def set_optimizer_all_in_one_finding_disease(network, guide_weight, loss_pos_weights, bce_loss_ratio, accum_iters=0):
    n_finding_outputs = 15
    n_disease_outputs = 8
    
    def weighted_bce(loss_pos_weight):

        def func_weighted_bce(y_true, y_pred):
            legit_labels = K.cast(K.not_equal(y_true, -1), 'float32')
            less_than_upper_limit = K.cast(K.less(y_pred, 0.9), 'float32')
            greater_than_lower_limit = K.cast(K.greater(y_pred, 0.1), 'float32')
            mask_true_positive = K.cast(K.equal(y_true, 1), 'float32')
            mask_true_negative = K.cast(K.equal(y_true, 0), 'float32')
            positive_up = loss_pos_weight
    
            weighted_bce_loss = -legit_labels * positive_up * y_true * K.log(y_pred + K.epsilon()) - legit_labels * (1 - y_true) * K.log(1 - y_pred + K.epsilon())
            
            return weighted_bce_loss

        return func_weighted_bce

    def loss_seg(y_true, y_pred):
        return K.mean(K.sum((1 - y_true) * K.log(K.maximum(y_pred, 0.01)), axis=3), axis=(1, 2))

    list_loss = list(np.array(zip([weighted_bce(loss_pos_weights[index]) for index in range(n_finding_outputs)], [loss_seg] * n_finding_outputs)).flatten()) + [weighted_bce(loss_pos_weights[index]) for index in range(n_disease_outputs)] 
    list_loss_weights = list(np.array(zip(bce_loss_ratio[:n_finding_outputs], [guide_weight] * n_finding_outputs)).flatten()) + bce_loss_ratio[n_finding_outputs:]

    if accum_iters > 0:
        SGD_gradient_accumulation = SGDAccum(lr=0, momentum=0.9, nesterov=True, accum_iters=accum_iters)
        network.compile(optimizer=SGD_gradient_accumulation, loss=list_loss, loss_weights=list_loss_weights, metrics=['accuracy'])
    else:        
        network.compile(optimizer=SGD(lr=0, momentum=0.9, nesterov=True), loss=list_loss, loss_weights=list_loss_weights, metrics=['accuracy'])

    return network


def set_output_all_in_one(network, n_output=15, add_maxpool=False):
    list_findings = ["Hemorrhage", "HardExudate", "CWP", "Drusen", "RetinalPigmentaryChange", "VascularAbnormality", "Membrane", "FluidAccumulation",
                     "ChroioretinalAtrophy", "ChoroidalLesion", "MyelinatedNerveFiber", "RNFLDefect", "GlaucomatousDiscChange", "NonGlaucomatousDiscChange", "MacularHole"]
    final_block = network.layers[-1].output
    list_output = []
    for index in range(n_output):
        final_result = Conv2D(1, (1, 1), padding="same")(final_block)
        activation_outs = Activation('sigmoid')(final_result)
        if add_maxpool:
            if list_findings[index] == "Hemorrhage" or list_findings[index] == "HardExudate" or list_findings[index] == "CWP" or list_findings[index] == "Drusen":  
                final_result = MaxPooling2D(pool_size=(8, 8))(final_result)
#                 final_result = Lambda(lambda x:x / 8.)(final_result)
        gap = GlobalAveragePooling2D()(final_result)
        outputs = Activation('sigmoid')(gap)
        list_output.append(outputs)
        list_output.append(activation_outs)
  
    model = Model(network.input, list_output)
    return model  


def network_one_output(network, finding_index):
    model = Model(network.input, network.output[2 * finding_index:2 * (finding_index + 1)])
    return model  


def set_output_end2end(network, accum_iters):
    final_block = network.get_layer("top_activation").output
    final_result = Conv2D(1, (1, 1), padding="same")(final_block)
    gap = GlobalAveragePooling2D()(final_result)
    outputs = Activation('sigmoid')(gap)
  
    model = Model(network.input, outputs)
    if accum_iters > 0:
        SGD_gradient_accumulation = SGDAccum(lr=0, momentum=0.9, nesterov=True, accum_iters=accum_iters)
        model.compile(optimizer=SGD_gradient_accumulation, loss=objectives.binary_crossentropy, metrics=['accuracy'])
    else:        
        model.compile(optimizer=SGD(lr=0, momentum=0.9, nesterov=True), loss=objectives.binary_crossentropy, metrics=['accuracy'])
    
    return model  


def add_disease_output(network):
    final_block = network.get_layer("top_activation").output
    n_additional_output = 8
    list_output = []
    for _ in range(n_additional_output):
        final_result = Conv2D(1, (1, 1), padding="same")(final_block)
        gap = GlobalAveragePooling2D()(final_result)
        outputs = Activation('sigmoid')(gap)
        list_output.append(outputs)

    model = Model(network.input, network.output + list_output)
    return model  
    
    
def set_optimizer_one_output(network, weight, lr_start_value, accum_iters=0):
    
    def loss_class(y_true, y_pred):
        return objectives.binary_crossentropy(y_true, y_pred)
    
    def loss_seg(y_true, y_pred):
        return K.mean(K.sum((1 - y_true) * K.log(K.maximum(y_pred, 0.01)), axis=3), axis=(1, 2))

    if accum_iters > 0:
        SGD_gradient_accumulation = SGDAccum(lr=lr_start_value, momentum=0.9, nesterov=True, accum_iters=accum_iters)
        network.compile(optimizer=SGD_gradient_accumulation, loss=[loss_class, loss_seg], loss_weights=[1, weight], metrics=['accuracy'])
    else:        
        network.compile(optimizer=SGD(lr=lr_start_value, momentum=0.9, nesterov=True), loss=[loss_class, loss_seg], loss_weights=[1, weight], metrics=['accuracy'])
    return network

