# python2.7
import multiprocessing
import threading
import Queue
from uuid import uuid4

import numpy as np
import SharedArray

import utils

img_h, img_w = 1024, 1024


def load_shared(args):
    i, img_array_name, normalize, augment, fname = args
    img_array = SharedArray.attach(img_array_name)
    img_array[i] = utils.load_augmented_diagnosis(fname, normalize, augment)


class BatchIterator(object):

    def __call__(self, *args):
        self.X, self.labels = args
        return self
        
    def __iter__(self):
        n_samples = self.X.shape[0]
        bs = self.batch_size
        for i in range(int(np.ceil(1.*n_samples / bs))):
            sl = slice(i * bs, (i + 1) * bs)
            X = self.X[sl]
            labels = self.labels[sl]
            yield self.transform(X, labels)


class QueueIterator(BatchIterator):

    def __iter__(self):
        queue = Queue.Queue(maxsize=30)
        end_marker = object()

        def producer():
            for filenames, Xb, labels in super(QueueIterator, self).__iter__():
                queue.put((np.array(filenames), np.array(Xb), np.array(labels)))
            queue.put(end_marker)
        
        thread = threading.Thread(target=producer)
        thread.daemon = True
        thread.start()

        item = queue.get()
        while item is not end_marker:
            yield item
            queue.task_done()
            item = queue.get()


class SharedIterator(QueueIterator):

    def __init__(self):
        self.pool = multiprocessing.Pool()

    def transform(self, fnames, labels):
        img_shared_array_name = str(uuid4())
        try:
            img_shared_array = SharedArray.create(
                img_shared_array_name, [len(fnames), img_h, img_w, 3], dtype=np.float32)
                                        
            args = []
            
            for i, fname in enumerate(fnames):
                args.append((i, img_shared_array_name, self.normalize, self.augment, fname))

            self.pool.map(load_shared, args)
            imgs = np.array(img_shared_array, dtype=np.float32)
            
        finally:
            SharedArray.delete(img_shared_array_name)
            
        return fnames, imgs, labels

    
class BatchFetcher(SharedIterator):

    def __init__(self, filenames_labels, batch_size, normalize, augment):
        self.n_data = len(filenames_labels)
        self.train_files = np.array(filenames_labels.filename.tolist())
        self.train_labels = np.array(filenames_labels.label.tolist())
        self.sample_weight = np.array(filenames_labels.sample_weight.tolist())
        self.normalize = normalize
        self.augment = augment
        self.batch_size = batch_size
        super(BatchFetcher, self).__init__()
        
    def __call__(self):
        if self.augment:
            indices = np.random.choice(np.arange(len(self.sample_weight)), size=len(self.sample_weight), replace=True, p=self.sample_weight)
        else:
            indices = range(self.n_data)
        X = self.train_files[indices]
        labels = self.train_labels[indices]
        return super(BatchFetcher, self).__call__(X, labels)

    def get_neg_pos_ratio(self):
        sample_indices = np.random.choice(np.arange(len(self.sample_weight)), size=len(self.sample_weight), replace=True, p=self.sample_weight)
        list_neg_pos_ratio = []
        labels = self.train_labels[sample_indices]
        
        for label_index in range(self.train_labels.shape[1]):
            labels = self.train_labels[sample_indices, label_index]
            labels = labels[labels != -1]
            bc = np.bincount(np.round(labels).astype(np.int))
            list_neg_pos_ratio.append(1.*bc[0] / bc[1])
            
            list_diseases = ['EarlyDR', 'AdvancedDR', 'BRVOHemiCRVO', 'CRVO', 'DryAMD', 'WetAMD', 'GlaucomaSuspect', 'EpiretinalMembrane']  # ignore macular hole
            list_label = list_findings + list_diseases
            print list_label[label_index], bc
            
        return list_neg_pos_ratio
    
    def update_sample_weight(self, sample_weight):
        self.sample_weight = sample_weight
    
