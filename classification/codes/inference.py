# python inference.py --gpu_index=4 --img_dir=../../../data/AMD/Validation-400-images/ --model_path=../model/B4/dr_pretrained/weight_311epoch.h5 --save_heatmap=True --submission_version=baseline
import os
import argparse
import time

import numpy as np
import pandas as pd
from PIL import Image

import utils

from model.efficient_net import EfficientNetB4, set_single_output
from keras.models import Model

# arrange arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    '--gpu_index',
    type=str,
    required=True
    )
parser.add_argument(
    '--img_dir',
    type=str,
    required=True
    )
parser.add_argument(
    '--model_path',
    type=str,
    required=True
    )
parser.add_argument(
    '--save_heatmap',
    type=str,
    required=False,
    default="False"
    )
parser.add_argument(
    '--submission_version',
    type=str,
    required=True
    )

FLAGS, _ = parser.parse_known_args()

# misc params
input_size = (512, 512, 3)  # (h,w)
os.environ['CUDA_VISIBLE_DEVICES'] = FLAGS.gpu_index
out_dir_home="/home/vuno/development/AMD/classification/submission/{}".format(FLAGS.submission_version)
utils.makedirs(out_dir_home)

# load a network
network = EfficientNetB4(include_top=False, weights=None, use_resnet_init_conv=True, input_shape=input_size)
network = set_single_output(network)
network.load_weights(FLAGS.model_path)
print("load weights from {}".format(FLAGS.model_path))
weights = network.get_layer("dense_1").get_weights()[0][:, 0]
weights = np.expand_dims(np.expand_dims(np.expand_dims(weights, axis=0), axis=0), axis=0)  # (1,1,1,1280)
network = Model(inputs=network.input, outputs=[network.output, network.get_layer("top_activation").output])

# run inference
all_filepaths = utils.all_files_under(FLAGS.img_dir)
filepaths, preds = [], []
for filepath in all_filepaths:
    print("processing {}...".format(filepath))
    try:
        network_input = utils.network_input_fundus(filepath)
        pred, featuremap = network.predict(network_input)

        filepaths.append(filepath)
        preds += list(pred[:, 0])
        if FLAGS.save_heatmap == "True":
            activation = np.sum(featuremap * weights, axis=-1, keepdims=True)
            utils.save_heatmap_funuds_img(resized_img, activation[0, ..., 0], os.path.join(out_dir_home, "heatmap"), filepath)
    except:
        continue
# save inference results
df = pd.DataFrame({"FileName":[os.path.basename(fpath) for fpath in filepaths], "AMD Risk":preds})
# df.to_csv(os.path.join(out_dir_home, "Classification_Results.csv"), index=False)
df.to_csv(os.path.join(out_dir_home, "{}_{}_{}.csv".format(FLAGS.img_dir.split("/")[-4],FLAGS.img_dir.split("/")[-3], FLAGS.img_dir.split("/")[-2])), index=False)