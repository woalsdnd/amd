import numpy as np
from PIL import Image
import argparse
import json
import os

import utils

# arrange arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    '--pred_seg',
    type=str,
    required=True
    )
FLAGS, _ = parser.parse_known_args()

list_path_mask = utils.all_files_under("/home/vuno/development/data/AMD/Training400_disc_fovea/Disc_Masks")
list_path_pred = utils.all_files_under(FLAGS.pred_seg)

list_gt_mask, list_pred_seg = [], []
dict_dice={val:[] for val in range(255)}
for index in range(len(list_path_mask)):
    path_mask = list_path_mask[index]
    path_pred = list_path_pred[index]
    gt_mask = np.array(Image.open(path_mask)).flatten()
    gt_mask[gt_mask==0]=1
    gt_mask[gt_mask==255]=0
    
    if not set(np.unique(gt_mask)) == set([0, 1]):
        continue

    pred_seg = np.array(Image.open(path_pred)).flatten()
    for thresh in range(255):
        pred_thresholded = np.copy(pred_seg)
        pred_thresholded[pred_seg >= thresh]=1
        pred_thresholded[pred_seg < thresh]=0
        dice=utils.dice(gt_mask, pred_thresholded)
        dict_dice[thresh].append(dice)

dict_mean_dice = {thresh:np.mean(list_val) for thresh, list_val in dict_dice.items()}
with open(os.path.join(os.path.dirname(FLAGS.pred_seg), 'threshold_result.json'), 'w') as fp:
    json.dump(dict_mean_dice, fp)

list_dice = [dict_mean_dice[thresh] for thresh in range(255)]
print("optimal threshold: {}, dice: {}".format(np.argmax(list_dice), np.max(list_dice)))
