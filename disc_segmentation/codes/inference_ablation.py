# python inference_ablation.py --gpu_index=0 --img_dir=../../../data/AMD/Validation-400-images/ --submission_version=ablation
import os
import argparse
import time

import numpy as np
import pandas as pd
from PIL import Image

from model.efficient_net import EfficientNetB0, EfficientNetB4, set_output, set_output_biFPN

import utils

# arrange arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    '--gpu_index',
    type=str,
    required=True
    )
parser.add_argument(
    '--img_dir',
    type=str,
    required=True
    )
parser.add_argument(
    '--submission_version',
    type=str,
    required=True
    )    
FLAGS, _ = parser.parse_known_args()

# misc params
os.environ['CUDA_VISIBLE_DEVICES'] = FLAGS.gpu_index
vessel_segmentor_model_path = "/home/vuno/development/AMD/fovea_localization/vessel_segmentation/model/vessel"
input_size = (640, 640, 3)
final_featuremap_resolution = 80
out_dir_home="/home/vuno/development/AMD/disc_segmentation/submission/{}".format(FLAGS.submission_version)
list_weight_path = utils.all_files_under("../model/ablation_study_using_AMD_data_only_selected")

out_pred = os.path.join(out_dir_home, "pred_map")
out_submission = os.path.join(out_dir_home, "final_mask")
utils.makedirs(out_pred)
utils.makedirs(out_submission)

# load a network
list_network = []
for weight_path in list_weight_path:
    fundus_network = EfficientNetB4(include_top=False, weights=None, use_resnet_init_conv=True, input_shape=input_size)
    vessel_network = EfficientNetB0(include_top=False, weights=None, use_resnet_init_conv=True, input_shape=input_size)
    network = set_output(fundus_network, vessel_network)
    network.load_weights(weight_path)
    print("model loaded from {}".format(weight_path))
    list_network.append(network)
vessel_segmentor = utils.load_network(vessel_segmentor_model_path)

# run inference
filepaths = utils.all_files_under(FLAGS.img_dir)
threshold = 122 # ensemble ablation (trained using only ADAM dataset)
for filepath in filepaths:
    print("processing {}...".format(filepath))

    # prepare input
    img = np.array(Image.open(filepath))
    h_ori, w_ori, _ = img.shape
    resized_img = utils.resize_img(img, input_size[0], input_size[1])
    standardized_resized_img = utils.standardize_fundus(resized_img)
    network_input = np.expand_dims(standardized_resized_img, axis=0)
    vessel_segmentation = vessel_segmentor.predict(network_input)
    
    # run 
    list_mask = []
    for network in list_network:
        mask, mask_vessel = network.predict([network_input, np.repeat((vessel_segmentation * 255).astype(np.uint8) / 255., 3, axis=-1)])
        list_mask.append(mask)
    
    # save verbatim output
    masks = np.array(list_mask)
    mask = np.mean(masks, axis=0)
    mask = mask[0,...,0] * 255
    mask = utils.resize_img(mask, h_ori, w_ori, method="bilinear")
    Image.fromarray((mask).astype(np.uint8)).save(os.path.join(out_pred, os.path.basename(filepath).replace(".jpg", ".png")))
    
    # postprocessing mask
    postprocessed_mask = np.copy(mask)
    postprocessed_mask[mask>=threshold]=1
    postprocessed_mask[mask<threshold]=0
    if len(postprocessed_mask[postprocessed_mask==1]) > 0: # post-processing
        postprocessed_mask = utils.fill_holes(postprocessed_mask)
        postprocessed_mask = utils.remain_largest_blob(postprocessed_mask)
        # postprocessed_mask = utils.convex_hull(postprocessed_mask)
    postprocessed_mask = 1 - postprocessed_mask # flip 0 and 1
    Image.fromarray((postprocessed_mask*255).astype(np.uint8)).save(os.path.join(out_submission, os.path.basename(filepath).replace(".jpg", ".png")))
