# python train.py  --config_file=../config/segmentation_overfit.cfg
import os
import logging
import argparse
import configparser
import time

import numpy as np
import pandas as pd
from PIL import Image

import utils
import iterator_shared_array
from model.efficient_net import EfficientNetB0, EfficientNetB4, set_output, set_output_biFPN, set_optimizer_detection


# arrange arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    '--config_file',
    type=str,
    required=True
    )
FLAGS, _ = parser.parse_known_args()

# set config
config = configparser.ConfigParser(interpolation=configparser.ExtendedInterpolation())
config.read_file(open(FLAGS.config_file))
if config["Train"]["use_all_data"] == "False":
    list_fundus_dir = [config["Path"]["path_fundus{}".format(index)] for index in range(2,12)]
    list_vessel_dir = [config["Path"]["path_vessel{}".format(index)] for index in range(2,12)]
    list_mask_dir = [config["Path"]["path_mask{}".format(index)] for index in range(2,12)]
    dir_val_fundus = config["Path"]["path_fundus1"]
    dir_val_vessel = config["Path"]["path_vessel1"]
    dir_val_mask = config["Path"]["path_mask1"]
else:
    # list_fundus_dir = [config["Path"]["path_fundus{}".format(index)] for index in range(1,2)]
    # list_vessel_dir = [config["Path"]["path_vessel{}".format(index)] for index in range(1,2)]
    # list_mask_dir = [config["Path"]["path_mask{}".format(index)] for index in range(1,2)]
    list_fundus_dir = [config["Path"]["path_fundus{}".format(index)] for index in range(1,2)]
    list_vessel_dir = [config["Path"]["path_vessel{}".format(index)] for index in range(1,2)]
    list_mask_dir = [config["Path"]["path_mask{}".format(index)] for index in range(1,2)]
    dir_val_fundus = config["Path"]["path_fundus1"]
    dir_val_vessel = config["Path"]["path_vessel1"]
    dir_val_mask = config["Path"]["path_mask1"]
dir_save_model = config["Path"]["dir_save_model"]
path_load_model = config["Path"]["path_load_model"] if "path_load_model" in config["Path"] else None
path_pretrained_weight = config["Path"]["path_pretrained_weight"] if "path_pretrained_weight" in config["Path"] else None
dir_experimental_result = config["Path"]["dir_experimental_result"]
dir_logger = config["Path"]["dir_logger"]
path_logger = os.path.join(dir_logger, os.path.basename(FLAGS.config_file).replace(".cfg", ".log")) 
batch_size = int(config["Train"]["batch_size"])
n_epochs = int(config["Train"]["n_epochs"])
lr_decay_tolerance = int(config["Train"]["lr_decay_tolerance"])
lr_decay_factor = float(config["Train"]["lr_decay_factor"])
lr_start_value = float(config["Train"]["lr_start"])
lr_min_value = float(config["Train"]["lr_min_value"])
optimizer = config["Train"]["optimizer"]
gpu_index = config["Train"]["gpu_index"]
generate_pretrained_weight = config["Train"]["generate_pretrained_weight"] if "generate_pretrained_weight" in config["Train"] else None
output_type = config["Train"]["output_type"] if "output_type" in config["Train"] else None
input_size = (int(config["Input"]["height"]), int(config["Input"]["width"]), int(config["Input"]["depth"]))
vessel_featuremap_resolution = int(config["Output"]["vessel_featuremap_resolution"])
utils.makedirs(dir_save_model)
utils.makedirs(dir_experimental_result)
utils.makedirs(dir_logger)

# set logger and tensorboard
logger = logging.getLogger()
logger.setLevel(logging.INFO)
formatter = logging.Formatter(fmt='%(asctime)s %(levelname)-8s %(message)s', datefmt='[%Y-%m-%d %H:%M:%S]')
handler = logging.FileHandler(path_logger)
handler.setFormatter(formatter)
logger.addHandler(handler)
tensorboard = utils.CustomTensorBoard(
  log_dir=os.path.join(dir_logger, "tensorboard", os.path.basename(FLAGS.config_file).replace(".cfg", ".log")),
  write_graph=False,
  batch_size=batch_size
)

# set gpu index
os.environ['CUDA_VISIBLE_DEVICES'] = gpu_index

# split data
list_df_train=[]
for index in range(len(list_fundus_dir)):
    list_fundus_path = utils.all_files_under(list_fundus_dir[index])
    list_vessel_path = utils.all_files_under(list_vessel_dir[index])
    list_mask_path = utils.all_files_under(list_mask_dir[index])
    df_fundus_path = pd.DataFrame({"fundus_path":list_fundus_path, "id":[os.path.basename(fpath).replace(".png", "").replace("prime", "") for fpath in list_fundus_path]})
    df_vessel_path = pd.DataFrame({"vessel_path":list_vessel_path, "id":[os.path.basename(fpath).replace(".png", "").replace("prime", "") for fpath in list_vessel_path]})
    df_mask_path = pd.DataFrame({"mask_path":list_mask_path, "id":[os.path.basename(fpath).replace(".png", "").replace(".bmp","").replace("_OD", "").split("-")[0] for fpath in list_mask_path]})
    df_merged = pd.merge(df_mask_path, df_fundus_path, on="id")
    df_merged = pd.merge(df_merged, df_vessel_path, on="id")
    list_df_train.append(df_merged)
df_train = pd.concat(list_df_train, ignore_index=True)
if config["Train"]["regular_validation"] == "True":
    list_val_fundus_path = utils.all_files_under(dir_val_fundus)
    list_val_vessel_path = utils.all_files_under(dir_val_vessel)
    list_val_mask_path = utils.all_files_under(dir_val_mask)
    df_fundus_path = pd.DataFrame({"fundus_path":list_val_fundus_path, "id":[os.path.basename(fpath).replace(".png", "") for fpath in list_val_fundus_path]})
    df_vessel_path = pd.DataFrame({"vessel_path":list_val_vessel_path, "id":[os.path.basename(fpath).replace(".png", "") for fpath in list_val_vessel_path]})
    df_mask_path = pd.DataFrame({"mask_path":list_val_mask_path, "id":[os.path.basename(fpath).replace(".png", "").replace(".bmp","").replace("_OD", "").split("-")[0] for fpath in list_val_mask_path]})
    df_val = pd.merge(df_mask_path, df_fundus_path, on="id")
    df_val = pd.merge(df_val, df_vessel_path, on="id")
if config["Train"]["regular_validation"] == "True":
    logger.info("#train: {}, #val: {}".format(len(df_train), len(df_val)))
else:
    logger.info("#train: {}".format(len(df_train)))

# set filenames and masks and iterators
train_fundus_data, train_vessel_data, train_mask_data = df_train["fundus_path"], df_train["vessel_path"], df_train["mask_path"]
train_batch_fetcher = iterator_shared_array.BatchFetcher((train_fundus_data, train_vessel_data, train_mask_data), [1./len(train_fundus_data)]*len(train_fundus_data),
                                           utils.fundus_segmentation_processing_func_train, batch_size, sample=True, replace=True, shared_array_shape=[input_size, input_size[:2], input_size[:2]])
if config["Train"]["regular_validation"] == "True":
    val_fundus_data, val_vessel_data, val_mask_data = df_val["fundus_path"], df_val["vessel_path"],  df_val["mask_path"]
    val_batch_fetcher = iterator_shared_array.BatchFetcher((val_fundus_data, val_vessel_data, val_mask_data), None,
                                            utils.fundus_segmentation_processing_func_val, 2*batch_size, sample=False, replace=False, shared_array_shape=[input_size, input_size[:2], input_size[:2]])

# define network
if path_load_model:
    fundus_network = EfficientNetB4(include_top=False, weights=None, use_resnet_init_conv=True, input_shape=input_size)
    vessel_network = EfficientNetB0(include_top=False, weights=None, use_resnet_init_conv=True, input_shape=input_size)
    if generate_pretrained_weight:
        fundus_network.load_weights("../model/baseline/imagenet_pretrained_fundus.h5")
        vessel_network.load_weights("../model/baseline/imagenet_pretrained_vessel.h5")
        network = set_output_biFPN(fundus_network, vessel_network) if output_type=="biFPN" else set_output(fundus_network, vessel_network)
        network.save_weights(os.path.join(dir_save_model, "network_imagenet_pretrained_weight.h5"))
    else:
        network = set_output_biFPN(fundus_network, vessel_network) if output_type=="biFPN" else set_output(fundus_network, vessel_network)
        network.load_weights(path_load_model)
        logger.info("model loaded from {}".format(path_load_model))
else:
    from keras.applications.resnet50 import ResNet50
    resnet = ResNet50(weights='imagenet')
    fundus_network = EfficientNetB4(include_top=False, weights=None, use_resnet_init_conv=True, input_shape=input_size)
    vessel_network = EfficientNetB0(include_top=False, weights=None, use_resnet_init_conv=True, input_shape=input_size)
    fundus_network.layers[1].set_weights([resnet.layers[2].get_weights()[0]])
    vessel_network.layers[1].set_weights([resnet.layers[2].get_weights()[0]])
    fundus_network_imagenet_pretrained = EfficientNetB4(include_top=False, weights="imagenet", input_shape=input_size)
    fundus_network = utils.copy_weights(fundus_network_imagenet_pretrained, fundus_network)
    vessel_network_imagenet_pretrained = EfficientNetB0(include_top=False, weights="imagenet", input_shape=input_size)
    vessel_network = utils.copy_weights(vessel_network_imagenet_pretrained, vessel_network)
    utils.makedirs(dir_save_model)
    fundus_network.save_weights(os.path.join(dir_save_model, "imagenet_pretrained_fundus.h5"))
    vessel_network.save_weights(os.path.join(dir_save_model, "imagenet_pretrained_vessel.h5"))
    network = set_output(fundus_network, vessel_network)
    network.save_weights(os.path.join(dir_save_model, "network_imagenet_pretrained_weight.h5"))
network = set_optimizer_detection(network, [1, 0.1])
network.summary()
tensorboard.set_model(network)  # set tensorboard callback associated with network

lr_scheduler = utils.LrScheduler(lr_object=network.optimizer.lr, lr_start_value=lr_start_value,
                                 lr_min_value=lr_min_value, lr_decay_tolerance=lr_decay_tolerance,
                                 lr_decay_factor=lr_decay_factor, logger=logger, score_func=lambda x:x["dice_640x640"])
vessel_seg_stride = 640//vessel_featuremap_resolution
for epoch in range(n_epochs):
    # train loop
    list_training_loss, list_training_loss_vessel, list_training_dice, list_training_dice_vessel = [], [], [], []
    for data, list_arr in train_batch_fetcher:
        tensorboard.draw_imgs("Training Fundus", epoch, (list_arr[0]*30+127).astype(np.uint8), plot_once=True)
        tensorboard.draw_imgs("Training Vessel", epoch, (list_arr[1]*255).astype(np.uint8), plot_once=True)
        tensorboard.draw_imgs("Training Mask", epoch, (list_arr[2]*255).astype(np.uint8), plot_once=True)
        # train & record
        bs = list_arr[0].shape[0]
        loss_total, loss_ce, loss_ce_vessel, dice, dice_vessel = network.train_on_batch([list_arr[0], np.repeat(np.expand_dims(list_arr[1], axis=-1), 3, axis=-1)], [np.expand_dims(list_arr[2], axis=-1), np.expand_dims(list_arr[2][:,::vessel_seg_stride,::vessel_seg_stride], axis=-1)])    
        utils.stack_list(head1=list_training_loss, tail1=[loss_ce] * bs, head2=list_training_loss_vessel, tail2=[loss_ce_vessel] * bs, head3=list_training_dice, tail3=[dice] * bs, head4=list_training_dice_vessel, tail4=[dice_vessel] * bs)
    train_metrics = {"loss":np.mean(list_training_loss), "loss_vessel":np.mean(list_training_loss_vessel), "dice":np.mean(list_training_dice), "dice_vessel":np.mean(list_training_dice_vessel)}
    utils.log_summary(logger, phase="training", epoch=epoch, **train_metrics)
    tensorboard.on_epoch_end(epoch, train_metrics)

    # val loop
    list_pred_seg, list_gt_mask, list_pred_seg_vessel, list_gt_mask_vessel = [], [], [], []
    if config["Train"]["regular_validation"] == "True":
        for data, list_arr in val_batch_fetcher:
            tensorboard.draw_imgs("Validation Fundus", epoch, (list_arr[0]*30+127).astype(np.uint8), plot_once=True)
            tensorboard.draw_imgs("Validation Vessel", epoch, (list_arr[1]*255).astype(np.uint8), plot_once=True)
            tensorboard.draw_imgs("Validation Mask", epoch, (list_arr[2]*255).astype(np.uint8), plot_once=True)
            # run validation
            mask, mask_vessel = network.predict([list_arr[0], np.repeat(np.expand_dims(list_arr[1], axis=-1), 3, axis=-1)])
            list_pred_seg.append(np.round(mask[...,0]).astype(np.uint8))
            list_pred_seg_vessel.append(np.round(mask_vessel[...,0]).astype(np.uint8))
            list_gt_mask.append(np.round(list_arr[2]).astype(np.uint8))
            list_gt_mask_vessel.append(np.round(list_arr[2])[:,::vessel_seg_stride,::vessel_seg_stride].astype(np.uint8))

        # compute errors
        dice = utils.dice(np.concatenate(list_gt_mask), np.concatenate(list_pred_seg))
        dice_vessel = utils.dice(np.concatenate(list_gt_mask_vessel), np.concatenate(list_pred_seg_vessel))
        # record val errors
        logger.info("validation at {}th epoch -- dice: {}, dice_vessel: {} in 640x640".format(epoch, dice, dice_vessel))
        val_metrics = {"dice_640x640":np.float64(dice), "dice_vessel_640x640":np.float64(dice_vessel)}
        utils.log_summary(logger, phase="validation", epoch=epoch, **val_metrics)
        tensorboard.on_epoch_end(epoch, val_metrics)

    # adjust learning rate
    if config["Train"]["regular_validation"] == "True":
        lr_scheduler.adjust_lr(epoch, val_metrics)

    # save network
    network.save_weights(os.path.join(dir_save_model, "weight_{}epoch.h5".format(epoch)))
