import os
import logging
import argparse
import configparser
import time

import numpy as np
import pandas as pd
from PIL import Image

import utils
import iterator_shared_array
from model.efficient_net_modify import EfficientNet_multiheads, set_output_lesion_segmentation, set_output_lesion_segmentation_v2, set_output_lesion_segmentation_v3, set_output_lesion_segmentation_v4, set_optimizer_lesion_segmentation

from keras.utils.training_utils import multi_gpu_model

lesions = ["drusen", "exudate", "hemorrhage", "others", "scar"]

def balanced_class_sample_weight(fpaths):
    ratio_pos_nge = 10
    df = pd.DataFrame({"fpath":[os.path.basename(fpath) for fpath in fpaths]})
    for lesion in lesions:
        df.loc[:, lesion]=0
        fnames = [fname.replace(".bmp",".png") for fname in utils.all_files_under(os.path.join("/home/vuno/development/data/AMD/Training400-Lesion/Lesion_Masks", lesion), append_path=False)]
        df.loc[df["fpath"].isin(fnames), lesion]=1
    
    n_neg = len(df[df["weights"]==0])
    df.loc[df["weights"]==0, "weights"] = 5./(n_neg * ratio_pos_nge)
    for lesion in lesions:
        n_pos = len(df[df[lesion]==1])
        df.loc[df[lesion]==1, "weights"]+=1./n_pos
    
    df["weights"]=df["weights"]/np.sum(df["weights"])

    return df["weights"]


def set_multiple_gpu(network, lr_start_value, n_multigpu, kernel_size):
    # new_network = set_output_lesion_segmentation(network)
    # new_network = set_output_lesion_segmentation_v2(network)
    # new_network = set_output_lesion_segmentation_v3(network)
    new_network = set_output_lesion_segmentation_v4(network, kernel_size)
    if n_multigpu > 1:
        new_network = multi_gpu_model(new_network, gpus=n_multigpu)
    new_network = set_optimizer_lesion_segmentation(new_network, lr_start_value)
    return new_network


def set_trainable(network, val):
    network.trainable = val
    for l in network.layers:
        l.trainable = val

def make_one_hot_mask(arr1, arr2, arr3, arr4, arr5):
    one_hot_mask = np.ones(arr1.shape+(6,))
    one_hot_mask[...,1]=arr1
    one_hot_mask[...,2]=arr2
    one_hot_mask[...,3]=arr3
    one_hot_mask[...,4]=arr4
    one_hot_mask[...,5]=arr5
    return one_hot_mask


# arrange arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    '--config_file',
    type=str,
    required=True
)
FLAGS, _ = parser.parse_known_args()

# set config
config = configparser.ConfigParser(interpolation=configparser.ExtendedInterpolation())
config.read_file(open(FLAGS.config_file))
dir_training_fundus = config["Path"]["path_fundus"]
dir_training_mask_drusen = config["Path"]["path_drusen_mask"]
dir_training_mask_exudate = config["Path"]["path_exudate_mask"]
dir_training_mask_hemorrhage = config["Path"]["path_hemorrhage_mask"]
dir_training_mask_others = config["Path"]["path_others_mask"]
dir_training_mask_scar = config["Path"]["path_scar_mask"]
dir_save_model = config["Path"]["dir_save_model"]
path_load_model = config["Path"]["path_load_model"] if "path_load_model" in config["Path"] else None
dir_experimental_result = config["Path"]["dir_experimental_result"]
dir_logger = config["Path"]["dir_logger"]
path_logger = os.path.join(dir_logger, os.path.basename(FLAGS.config_file).replace(".cfg", ".log"))
batch_size = int(config["Train"]["batch_size"])
n_epochs = int(config["Train"]["n_epochs"])
lr_decay_tolerance = int(config["Train"]["lr_decay_tolerance"])
lr_decay_factor = float(config["Train"]["lr_decay_factor"])
lr_start_value = float(config["Train"]["lr_start"])
lr_min_value = float(config["Train"]["lr_min_value"])
optimizer = config["Train"]["optimizer"]
gpu_index = config["Train"]["gpu_index"]
n_multigpu = len(gpu_index.split(","))
input_size = (int(config["Input"]["height"]), int(config["Input"]["width"]), int(config["Input"]["depth"]))
kernel_size = int(config["Train"]["kernel_size"])
utils.makedirs(dir_save_model)
utils.makedirs(dir_experimental_result)
utils.makedirs(dir_logger)

# set gpu index
os.environ['CUDA_VISIBLE_DEVICES'] = gpu_index

# set logger and tensorboard
logger = logging.getLogger()
logger.setLevel(logging.INFO)
formatter = logging.Formatter(fmt='%(asctime)s %(levelname)-8s %(message)s', datefmt='[%Y-%m-%d %H:%M:%S]')
handler = logging.FileHandler(path_logger)
handler.setFormatter(formatter)
logger.addHandler(handler)
tensorboard = utils.CustomTensorBoard(
    log_dir=os.path.join(dir_logger, "tensorboard", os.path.basename(FLAGS.config_file).replace(".cfg", ".log")),
    write_graph=False,
    batch_size=batch_size
)

# prepare data
fpaths_fundus = utils.all_files_under(dir_training_fundus)
fpaths_drusen = utils.all_files_under(dir_training_mask_drusen)
fpaths_exudate = utils.all_files_under(dir_training_mask_exudate)
fpaths_hemorrhage = utils.all_files_under(dir_training_mask_hemorrhage)
fpaths_others = utils.all_files_under(dir_training_mask_others)
fpaths_scar = utils.all_files_under(dir_training_mask_scar)
assert len(fpaths_fundus) == len(fpaths_drusen)
assert len(fpaths_fundus) == len(fpaths_exudate) 
assert len(fpaths_fundus) == len(fpaths_hemorrhage) 
assert len(fpaths_fundus) == len(fpaths_others)
assert len(fpaths_fundus) == len(fpaths_scar)

# set iterators
sample_weight = balanced_class_sample_weight(fpaths_fundus)
train_batch_fetcher = iterator_shared_array.BatchFetcher((fpaths_fundus, fpaths_drusen, fpaths_exudate, fpaths_hemorrhage, fpaths_others, fpaths_scar), sample_weight,
                                           utils.fundus_lesion_segmentation_processing_func_train, batch_size, sample=True, replace=True, shared_array_shape=[input_size, input_size[:2], input_size[:2], input_size[:2], input_size[:2],input_size[:2]])
val_batch_fetcher = iterator_shared_array.BatchFetcher((fpaths_fundus, fpaths_drusen, fpaths_exudate, fpaths_hemorrhage, fpaths_others, fpaths_scar), None,
                                           utils.fundus_lesion_segmentation_processing_func_val, batch_size, sample=False, replace=False, shared_array_shape=[input_size, input_size[:2], input_size[:2], input_size[:2], input_size[:2],input_size[:2]])


# debug batch fetcher
# epoch=0
# for data, list_arr in train_batch_fetcher:
#     # list_arr = [img1, mask1, mask2, mask3, mask4, mask5]
#     mask_one_hot = make_one_hot_mask(list_arr[1], list_arr[2], list_arr[3], list_arr[4], list_arr[5])
#     tensorboard.draw_imgs("Training Image", epoch, (mask_one_hot[...,0] * 255).astype(np.uint8), plot_once=False)
#     tensorboard.draw_imgs("Training drusen", epoch, (mask_one_hot[...,1] * 255).astype(np.uint8), plot_once=False)
#     tensorboard.draw_imgs("Training exudate", epoch, (mask_one_hot[...,2] * 255).astype(np.uint8), plot_once=False)
#     tensorboard.draw_imgs("Training hemorrhage", epoch, (mask_one_hot[...,3] * 255).astype(np.uint8), plot_once=False)
#     tensorboard.draw_imgs("Training others", epoch, (mask_one_hot[...,4] * 255).astype(np.uint8), plot_once=False)
#     tensorboard.draw_imgs("Training scar", epoch, (mask_one_hot[...,5] * 255).astype(np.uint8), plot_once=False)
#     pass
# for data, list_arr in val_batch_fetcher:
#     # list_arr = [img1, mask1, mask2, mask3, mask4, mask5]
#     tensorboard.draw_imgs("Training Image", 0, (list_arr[0] * 255).astype(np.uint8), plot_once=False)
#     tensorboard.draw_imgs("Training drusen", epoch, (list_arr[1] * 255).astype(np.uint8), plot_once=False)
#     tensorboard.draw_imgs("Training exudate", epoch, (list_arr[2] * 255).astype(np.uint8), plot_once=False)
#     tensorboard.draw_imgs("Training hemorrhage", epoch, (list_arr[3] * 255).astype(np.uint8), plot_once=False)
#     tensorboard.draw_imgs("Training others", epoch, (list_arr[4] * 255).astype(np.uint8), plot_once=False)
#     tensorboard.draw_imgs("Training scar", epoch, (list_arr[5] * 255).astype(np.uint8), plot_once=False)
#     pass

# define network
network = EfficientNet_multiheads(input_shape=input_size)
network.load_weights(path_load_model)
set_trainable(network, False)
network = set_multiple_gpu(network, lr_start_value, n_multigpu, kernel_size)
logger.info("load weights from {}".format(path_load_model))
network.summary()
tensorboard.set_model(network)  # set tensorboard callback associated with network

# train loop
for epoch in range(n_epochs):
    list_loss, list_dice = [], []
    for data, list_arr in train_batch_fetcher:
        # list_arr = [img1, mask1, mask2, mask3, mask4, mask5]
        tensorboard.draw_imgs("Training Image", epoch, (list_arr[0] * 255).astype(np.uint8), plot_once=True)
        # resize for 64x64 output
        for index_mask in range(1,6):
            list_arr[index_mask] = list_arr[index_mask][:,::16,::16]
        # background, drusen, exudate, hemorrhage, others, scar
        mask_one_hot = make_one_hot_mask(list_arr[1], list_arr[2], list_arr[3], list_arr[4], list_arr[5])
        loss, dice = network.train_on_batch(list_arr[0], mask_one_hot)
        list_loss += [loss]*len(list_arr[0])
        list_dice += [dice]*len(list_arr[0])
        print(loss)
    train_metrics = {"loss": np.mean(list_loss), "dice":np.mean(list_dice)}
    utils.log_summary(logger, phase="training", epoch=epoch, **train_metrics)
    tensorboard.on_epoch_end(epoch, train_metrics)
    
    # val
    list_list_dice = [[] for lesion in lesions]
    for data, list_arr in val_batch_fetcher:
        # list_arr: [img1, mask1, mask2, mask3, mask4, mask5]
        tensorboard.draw_imgs("Validation Image", epoch, (list_arr[0] * 255).astype(np.uint8), plot_once=True)
        # resize for 64x64 output
        for index_mask in range(1,6):
            list_arr[index_mask] = np.round(list_arr[index_mask][:,::16,::16])
        # background, drusen, exudate, hemorrhage, others, scar
        preds_onehot = network.predict(list_arr[0])
        preds = np.argmax(preds_onehot, axis=-1)
        for i in range(1, len(list_arr)):
            if np.sum(list_arr[i]==1) + np.sum(preds==i)>0:
                dice = (2.*np.sum((list_arr[i]==1)&(preds==i))+1)/(np.sum(list_arr[i]==1)+np.sum(preds==i)+1)
                list_list_dice[i-1].append(dice)
    val_metrics={lesions[index]:np.mean(list_dice) for index, list_dice in enumerate(list_list_dice)}
    utils.log_summary(logger, phase="validation", epoch=epoch, **val_metrics)
    tensorboard.on_epoch_end(epoch, {key: val for key, val in val_metrics.items()})

    # save network
    if n_multigpu > 1:
        network.layers[-2].save_weights(os.path.join(dir_save_model, "weight_{}epoch.h5".format(epoch)))
    else:
        if epoch > 10 and epoch % 5 == 0:
            network.save_weights(os.path.join(dir_save_model, "weight_{}epoch.h5".format(epoch)))
