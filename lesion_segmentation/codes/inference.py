# python inference.py  --gpu_index=2 --model_path=../model/train_overfit_64x64/weight_20epoch.h5  --img_dir=../../../data/AMD/Training400/ori_images --submission_version=20th_epoch_train
import os
import argparse
import time

import numpy as np
import pandas as pd
from PIL import Image

import utils

from model.efficient_net_modify import EfficientNet_multiheads, set_output_lesion_segmentation, set_output_lesion_segmentation_v2, set_output_lesion_segmentation_v3

from keras.models import Model

# arrange arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    '--gpu_index',
    type=str,
    required=True
    )
parser.add_argument(
    '--img_dir',
    type=str,
    required=True
    )
parser.add_argument(
    '--model_path',
    type=str,
    required=True
    )
parser.add_argument(
    '--submission_version',
    type=str,
    required=True
    )
FLAGS, _ = parser.parse_known_args()

# misc params
lesions = ["drusen", "exudate", "hemorrhage", "others", "scar"]
dir_lesions = ["Drusen", "Exudate", "Hemorrhage", "Others", "Scar"]
input_size = (1024, 1024, 3)  # (h,w)
os.environ['CUDA_VISIBLE_DEVICES'] = FLAGS.gpu_index
out_dir_home="/home/vuno/development/AMD/lesion_segmentation/submission/{}".format(FLAGS.submission_version)
for i in range(len(lesions)):
    utils.makedirs(os.path.join(out_dir_home, dir_lesions[i]))
utils.makedirs(os.path.join(out_dir_home, "background"))

# load a network
network = EfficientNet_multiheads(input_shape=input_size)
network = set_output_lesion_segmentation_v3(network)
network.load_weights(FLAGS.model_path)
print("load weights from {}".format(FLAGS.model_path))

# run inference
all_filepaths = utils.all_files_under(FLAGS.img_dir)
for filepath in all_filepaths:
    print("processing {}...".format(filepath))
    img = np.array(Image.open(filepath))
    ori_h, ori_w, _ = img.shape
    resized_img = utils.resize_img(img, input_size[0], input_size[1])
    normalized_resized_img = utils.normalize(resized_img)

    pred_onehot = network.predict(np.expand_dims(normalized_resized_img, axis=0))[0,...]
    pred = np.argmax(pred_onehot, axis=-1)

    for i in range(len(lesions)):
        lesion_mask = (np.ones(pred.shape)*255).astype(np.uint8)
        lesion_mask[pred==i+1]=0
        # Image.fromarray(lesion_mask).save(os.path.join(out_dir_home, dir_lesions[i], os.path.basename(filepath).replace(".jpg", ".png")))
        mask_ori_size = utils.resize_img(lesion_mask, ori_h, ori_w, method="nearest")
        Image.fromarray(mask_ori_size).save(os.path.join(out_dir_home, dir_lesions[i], os.path.basename(filepath).replace(".jpg", ".png")))
    # lesion_mask = (np.ones(pred.shape)*255).astype(np.uint8)
    # lesion_mask[pred==0]=0
    # Image.fromarray(lesion_mask).save(os.path.join(out_dir_home, "background", os.path.basename(filepath).replace(".jpg", ".png")))
        