# python train_finetune_individual.py --config_file=../config/segmentation_overfit_64x64_drusen.cfg
# python train_finetune_individual.py --config_file=../config/segmentation_overfit_64x64_exudate.cfg
# python train_finetune_individual.py --config_file=../config/segmentation_overfit_64x64_hemorrhage.cfg
# python train_finetune_individual.py --config_file=../config/segmentation_overfit_64x64_others.cfg
# python train_finetune_individual.py --config_file=../config/segmentation_overfit_64x64_scar.cfg
import os
import logging
import argparse
import configparser
import time

import numpy as np
import pandas as pd
from PIL import Image

import utils
from scipy.misc import imresize
import iterator_shared_array
from model.efficient_net_modify import EfficientNet_multiheads, set_output_lesion_segmentation_individual, set_optimizer_lesion_segmentation_individual

from keras.utils.training_utils import multi_gpu_model

lesions = ["drusen", "exudate", "hemorrhage", "others", "scar"]

def balanced_class_sample_weight(fpaths, task):
    df = pd.DataFrame({"fpath":[os.path.basename(fpath) for fpath in fpaths]})
    df.loc[:, task]=0
    fnames = [fname.replace(".bmp",".png") for fname in utils.all_files_under(os.path.join("/home/vuno/development/data/AMD/Training400-Lesion/Lesion_Masks", task), append_path=False)]
    df.loc[df["fpath"].isin(fnames), task]=1

    df.loc[:, "weights"]=0
    n_pos = len(df[df[task]==1])
    n_neg = len(df[df[task]==0])
    df.loc[df[task]==1, "weights"] = 1./(2*n_pos)
    df.loc[df[task]==0, "weights"] = 1./(2*n_neg)
    
    return df["weights"]


def resize_mask(masks):
    n, ori_h, ori_w = masks.shape
    resized_masks = np.zeros((n, 64, 64))
    for index, mask in enumerate(masks):
        resized_mask = imresize(mask, (64, 64), interp="bilinear")
        resized_mask[resized_mask>0] = 1
        resized_masks[index] = resized_mask
    return resized_masks    



def set_multiple_gpu(network, lr_start_value, n_multigpu, kernel_size):
    new_network = set_output_lesion_segmentation_individual(network, kernel_size)
    if n_multigpu > 1:
        new_network = multi_gpu_model(new_network, gpus=n_multigpu)
    new_network = set_optimizer_lesion_segmentation_individual(new_network, lr_start_value)
    return new_network


def set_trainable(network, val):
    network.trainable = val
    for l in network.layers:
        l.trainable = val


# arrange arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    '--config_file',
    type=str,
    required=True
)
FLAGS, _ = parser.parse_known_args()

# set config
config = configparser.ConfigParser(interpolation=configparser.ExtendedInterpolation())
config.read_file(open(FLAGS.config_file))
dir_training_fundus = config["Path"]["path_fundus"]
dir_training_mask_drusen = config["Path"]["path_drusen_mask"]
dir_training_mask_exudate = config["Path"]["path_exudate_mask"]
dir_training_mask_hemorrhage = config["Path"]["path_hemorrhage_mask"]
dir_training_mask_others = config["Path"]["path_others_mask"]
dir_training_mask_scar = config["Path"]["path_scar_mask"]
dir_save_model = config["Path"]["dir_save_model"]
path_load_model = config["Path"]["path_load_model"] if "path_load_model" in config["Path"] else None
dir_experimental_result = config["Path"]["dir_experimental_result"]
dir_logger = config["Path"]["dir_logger"]
path_logger = os.path.join(dir_logger, os.path.basename(FLAGS.config_file).replace(".cfg", ".log"))
task = config["Train"]["task"]
batch_size = int(config["Train"]["batch_size"])
n_epochs = int(config["Train"]["n_epochs"])
lr_decay_tolerance = int(config["Train"]["lr_decay_tolerance"])
lr_decay_factor = float(config["Train"]["lr_decay_factor"])
lr_start_value = float(config["Train"]["lr_start"])
lr_min_value = float(config["Train"]["lr_min_value"])
optimizer = config["Train"]["optimizer"]
gpu_index = config["Train"]["gpu_index"]
n_multigpu = len(gpu_index.split(","))
input_size = (int(config["Input"]["height"]), int(config["Input"]["width"]), int(config["Input"]["depth"]))
kernel_size = int(config["Train"]["kernel_size"])
utils.makedirs(dir_save_model)
utils.makedirs(dir_experimental_result)
utils.makedirs(dir_logger)

# set gpu index
os.environ['CUDA_VISIBLE_DEVICES'] = gpu_index

# set logger and tensorboard
logger = logging.getLogger()
logger.setLevel(logging.INFO)
formatter = logging.Formatter(fmt='%(asctime)s %(levelname)-8s %(message)s', datefmt='[%Y-%m-%d %H:%M:%S]')
handler = logging.FileHandler(path_logger)
handler.setFormatter(formatter)
logger.addHandler(handler)
tensorboard = utils.CustomTensorBoard(
    log_dir=os.path.join(dir_logger, "tensorboard", os.path.basename(FLAGS.config_file).replace(".cfg", ".log")),
    write_graph=False,
    batch_size=batch_size
)

# prepare data
fpaths_fundus = utils.all_files_under(dir_training_fundus)
fpaths_drusen = utils.all_files_under(dir_training_mask_drusen)
fpaths_exudate = utils.all_files_under(dir_training_mask_exudate)
fpaths_hemorrhage = utils.all_files_under(dir_training_mask_hemorrhage)
fpaths_others = utils.all_files_under(dir_training_mask_others)
fpaths_scar = utils.all_files_under(dir_training_mask_scar)
assert len(fpaths_fundus) == len(fpaths_drusen)
assert len(fpaths_fundus) == len(fpaths_exudate) 
assert len(fpaths_fundus) == len(fpaths_hemorrhage) 
assert len(fpaths_fundus) == len(fpaths_others)
assert len(fpaths_fundus) == len(fpaths_scar)

if task == "drusen":
    fpaths_mask = fpaths_drusen
elif task == "exudate":
    fpaths_mask = fpaths_exudate
elif task == "hemorrhage":
    fpaths_mask = fpaths_hemorrhage
elif task == "others":
    fpaths_mask = fpaths_others
elif task == "scar":
    fpaths_mask = fpaths_scar


# set iterators
sample_weight = balanced_class_sample_weight(fpaths_fundus, task)
train_batch_fetcher = iterator_shared_array.BatchFetcher((fpaths_fundus, fpaths_mask), sample_weight,
                                           utils.fundus_lesion_segmentation_individual_processing_func_train, batch_size, sample=True, replace=True, shared_array_shape=[input_size, input_size[:2]])
val_batch_fetcher = iterator_shared_array.BatchFetcher((fpaths_fundus, fpaths_mask), None,
                                           utils.fundus_lesion_segmentation_individual_processing_func_val, batch_size, sample=False, replace=False, shared_array_shape=[input_size, input_size[:2]])


# debug batch fetcher
# epoch=0
# for data, list_arr in train_batch_fetcher:
#     # list_arr = [img, mask]
#     tensorboard.draw_imgs("Training Image", epoch, (list_arr[0] * 255).astype(np.uint8), plot_once=False)
#     tensorboard.draw_imgs("Training mask", epoch, (list_arr[1] * 255).astype(np.uint8), plot_once=False)
#     tensorboard.draw_imgs("Training resized mask", epoch, (resize_mask(list_arr[1]) * 255).astype(np.uint8), plot_once=False)
#     pass
# for data, list_arr in val_batch_fetcher:
#     # list_arr = [img, mask]
#     tensorboard.draw_imgs("Validation Image", epoch, (list_arr[0] * 255).astype(np.uint8), plot_once=False)
#     tensorboard.draw_imgs("Validation mask", epoch, (list_arr[1] * 255).astype(np.uint8), plot_once=False)
#     pass

# define network
network = EfficientNet_multiheads(input_shape=input_size)
network.load_weights(path_load_model)
set_trainable(network, False)
network = set_multiple_gpu(network, lr_start_value, n_multigpu, kernel_size)
# network.load_weights(path_load_model)
logger.info("load weights from {}".format(path_load_model))
network.summary()
tensorboard.set_model(network)  # set tensorboard callback associated with network

# train loop
lr_scheduler = utils.LrScheduler(lr_object=network.optimizer.lr, lr_start_value=lr_start_value,
                                 lr_min_value=lr_min_value, lr_decay_tolerance=lr_decay_tolerance,
                                 lr_decay_factor=lr_decay_factor, logger=logger, score_func=lambda x:x["mean_dice"])
for epoch in range(n_epochs):
    list_loss, list_dice = [], []
    for data, list_arr in train_batch_fetcher:
        # list_arr = [img, mask]
        tensorboard.draw_imgs("Training Image", epoch, (list_arr[0] * 255).astype(np.uint8), plot_once=True)
        # resize for 64x64 output
        loss, dice = network.train_on_batch(list_arr[0], np.expand_dims(resize_mask(list_arr[1]), axis=-1))
        list_loss += [loss]*len(list_arr[0])
        list_dice += [dice]*len(list_arr[0])
    train_metrics = {"loss": np.mean(list_loss), "dice":np.mean(list_dice)}
    utils.log_summary(logger, phase="training", epoch=epoch, **train_metrics)
    tensorboard.on_epoch_end(epoch, train_metrics)
    
    # val
    list_dice = []
    for data, list_arr in val_batch_fetcher:
        # list_arr: [img1, mask1, mask2, mask3, mask4, mask5] => drusen, exudate, hemorrhage, others, scar
        tensorboard.draw_imgs("Validation Image", epoch, (list_arr[0] * 255).astype(np.uint8), plot_once=True)
        # resize for 64x64 output
        gt_mask = list_arr[1][:,::16,::16]
        preds = network.predict(list_arr[0])[..., 0]
        preds_mask = np.round(preds)
        if np.sum(gt_mask==1) > 0:
            dice = (2.*np.sum((gt_mask==1)&(preds_mask==1))+1)/(np.sum(gt_mask==1)+np.sum(preds_mask==1)+1)
            list_dice.append([dice]*len(list_arr[0]))
    val_metrics={"mean_dice":np.mean(list_dice)}
    utils.log_summary(logger, phase="validation", epoch=epoch, **val_metrics)
    tensorboard.on_epoch_end(epoch, {key: val for key, val in val_metrics.items()})

    # adjust learning rate
    lr_scheduler.adjust_lr(epoch, val_metrics)
    
    # save network
    if n_multigpu > 1:
        network.layers[-2].save_weights(os.path.join(dir_save_model, "weight_{}epoch.h5".format(epoch)))
    else:
        if epoch > 20 and epoch % 5 == 0:
            network.save_weights(os.path.join(dir_save_model, "weight_{}epoch.h5".format(epoch)))
