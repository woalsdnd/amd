from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os
import json
import math
import string
import collections
import numpy as np

from six.moves import xrange
from keras_applications.imagenet_utils import _obtain_input_shape
from keras_applications.imagenet_utils import decode_predictions
from keras_applications.imagenet_utils import preprocess_input as _preprocess_input
import keras.layers as layers
import keras.models as models
import keras.utils as keras_utils
import keras.backend as backend
from keras.layers.merge import Concatenate
from keras.layers import Activation, UpSampling2D, Conv2D
from keras.optimizers import Adam, SGD
from keras import objectives
from keras.models import Model
from keras import backend as K


LIST_FINDINGS = ["Hemorrhage", "HardExudate", "CWP", "Drusen", "RetinalPigmentaryChange", "VascularAbnormality", "Membrane", "FluidAccumulation",
                 "ChroioretinalAtrophy", "ChoroidalLesion", "MyelinatedNerveFiber", "RNFLDefect", "GlaucomatousDiscChange", "NonGlaucomatousDiscChange", "MacularHole"]

BASE_WEIGHTS_PATH = (
    'https://github.com/Callidior/keras-applications/'
    'releases/download/efficientnet/')

WEIGHTS_HASHES = {
    'efficientnet-b0': ('163292582f1c6eaca8e7dc7b51b01c61'
                        '5b0dbc0039699b4dcd0b975cc21533dc',
                        'c1421ad80a9fc67c2cc4000f666aa507'
                        '89ce39eedb4e06d531b0c593890ccff3'),
    'efficientnet-b1': ('d0a71ddf51ef7a0ca425bab32b7fa7f1'
                        '6043ee598ecee73fc674d9560c8f09b0',
                        '75de265d03ac52fa74f2f510455ba64f'
                        '9c7c5fd96dc923cd4bfefa3d680c4b68'),
    'efficientnet-b2': ('bb5451507a6418a574534aa76a91b106'
                        'f6b605f3b5dde0b21055694319853086',
                        '433b60584fafba1ea3de07443b74cfd3'
                        '2ce004a012020b07ef69e22ba8669333'),
    'efficientnet-b3': ('03f1fba367f070bd2545f081cfa7f3e7'
                        '6f5e1aa3b6f4db700f00552901e75ab9',
                        'c5d42eb6cfae8567b418ad3845cfd63a'
                        'a48b87f1bd5df8658a49375a9f3135c7'),
    'efficientnet-b4': ('98852de93f74d9833c8640474b2c698d'
                        'b45ec60690c75b3bacb1845e907bf94f',
                        '7942c1407ff1feb34113995864970cd4'
                        'd9d91ea64877e8d9c38b6c1e0767c411'),
    'efficientnet-b5': ('30172f1d45f9b8a41352d4219bf930ee'
                        '3339025fd26ab314a817ba8918fefc7d',
                        '9d197bc2bfe29165c10a2af8c2ebc675'
                        '07f5d70456f09e584c71b822941b1952'),
    'efficientnet-b6': ('f5270466747753485a082092ac9939ca'
                        'a546eb3f09edca6d6fff842cad938720',
                        '1d0923bb038f2f8060faaf0a0449db4b'
                        '96549a881747b7c7678724ac79f427ed'),
    'efficientnet-b7': ('876a41319980638fa597acbbf956a82d'
                        '10819531ff2dcb1a52277f10c7aefa1a',
                        '60b56ff3a8daccc8d96edfd40b204c11'
                        '3e51748da657afd58034d54d3cec2bac')
}

BlockArgs = collections.namedtuple('BlockArgs', [
    'kernel_size', 'num_repeat', 'input_filters', 'output_filters',
    'expand_ratio', 'id_skip', 'strides', 'se_ratio'
])
# defaults will be a public argument for namedtuple in Python 3.7
# https://docs.python.org/3/library/collections.html#collections.namedtuple
BlockArgs.__new__.__defaults__ = (None,) * len(BlockArgs._fields)

DEFAULT_BLOCKS_ARGS = [
    BlockArgs(kernel_size=3, num_repeat=1, input_filters=32, output_filters=16,
              expand_ratio=1, id_skip=True, strides=[1, 1], se_ratio=0.25),
    BlockArgs(kernel_size=3, num_repeat=2, input_filters=16, output_filters=24,
              expand_ratio=6, id_skip=True, strides=[2, 2], se_ratio=0.25),
    BlockArgs(kernel_size=5, num_repeat=2, input_filters=24, output_filters=40,
              expand_ratio=6, id_skip=True, strides=[2, 2], se_ratio=0.25),
    BlockArgs(kernel_size=3, num_repeat=3, input_filters=40, output_filters=80,
              expand_ratio=6, id_skip=True, strides=[2, 2], se_ratio=0.25),
    BlockArgs(kernel_size=5, num_repeat=3, input_filters=80, output_filters=112,
              expand_ratio=6, id_skip=True, strides=[1, 1], se_ratio=0.25),
    BlockArgs(kernel_size=5, num_repeat=4, input_filters=112, output_filters=192,
              expand_ratio=6, id_skip=True, strides=[2, 2], se_ratio=0.25),
    BlockArgs(kernel_size=3, num_repeat=1, input_filters=192, output_filters=320,
              expand_ratio=6, id_skip=True, strides=[2, 2], se_ratio=0.25)
]

CONV_KERNEL_INITIALIZER = {
    'class_name': 'VarianceScaling',
    'config': {
        'scale': 2.0,
        'mode': 'fan_out',
        # EfficientNet actually uses an untruncated normal distribution for
        # initializing conv layers, but keras.initializers.VarianceScaling use
        # a truncated distribution.
        # We decided against a custom initializer for better serializability.
        'distribution': 'normal'
    }
}

DENSE_KERNEL_INITIALIZER = {
    'class_name': 'VarianceScaling',
    'config': {
        'scale': 1. / 3.,
        'mode': 'fan_out',
        'distribution': 'uniform'
    }
}


def preprocess_input(x, **kwargs):
    return _preprocess_input(x, mode='torch', **kwargs)


def get_swish(**kwargs):

    def swish(x):
        """Swish activation function: x * sigmoid(x).
        Reference: [Searching for Activation Functions](https://arxiv.org/abs/1710.05941)
        """

        if backend.backend() == 'tensorflow':
            try:
                # The native TF implementation has a more
                # memory-efficient gradient implementation
                return backend.tf.nn.swish(x)
            except AttributeError:
                pass

        return x * backend.sigmoid(x)

    return swish


def get_dropout(**kwargs):
    """Wrapper over custom dropout. Fix problem of ``None`` shape for tf.keras.
    It is not possible to define FixedDropout class as global object,
    because we do not have modules for inheritance at first time.

    Issue:
        https://github.com/tensorflow/tensorflow/issues/30946
    """

    class FixedDropout(layers.Dropout):

        def _get_noise_shape(self, inputs):
            if self.noise_shape is None:
                return self.noise_shape

            symbolic_shape = backend.shape(inputs)
            noise_shape = [symbolic_shape[axis] if shape is None else shape
                           for axis, shape in enumerate(self.noise_shape)]
            return tuple(noise_shape)

    return FixedDropout


def round_filters(filters, width_coefficient, depth_divisor):
    """Round number of filters based on width multiplier."""

    filters *= width_coefficient
    new_filters = int(filters + depth_divisor / 2) // depth_divisor * depth_divisor
    new_filters = max(depth_divisor, new_filters)
    # Make sure that round down does not go down by more than 10%.
    if new_filters < 0.9 * filters:
        new_filters += depth_divisor
    return int(new_filters)


def round_repeats(repeats, depth_coefficient):
    """Round number of repeats based on depth multiplier."""

    return int(math.ceil(depth_coefficient * repeats))


def mb_conv_block(inputs, block_args, activation, drop_rate=None, prefix='',):
    """Mobile Inverted Residual Bottleneck."""

    has_se = (block_args.se_ratio is not None) and (0 < block_args.se_ratio <= 1)
    bn_axis = 3 if backend.image_data_format() == 'channels_last' else 1

    # workaround over non working dropout with None in noise_shape in tf.keras
    Dropout = get_dropout(
        backend=backend,
        layers=layers,
        models=models,
        utils=keras_utils
    )

    # Expansion phase
    filters = block_args.input_filters * block_args.expand_ratio
    if block_args.expand_ratio != 1:
        x = layers.Conv2D(filters, 1,
                          padding='same',
                          use_bias=False,
                          kernel_initializer=CONV_KERNEL_INITIALIZER,
                          name=prefix + 'expand_conv')(inputs)
        x = layers.BatchNormalization(axis=bn_axis, name=prefix + 'expand_bn')(x)
        x = layers.Activation(activation, name=prefix + 'expand_activation')(x)
    else:
        x = inputs

    # Depthwise Convolution
    x = layers.DepthwiseConv2D(block_args.kernel_size,
                               strides=block_args.strides,
                               padding='same',
                               use_bias=False,
                               depthwise_initializer=CONV_KERNEL_INITIALIZER,
                               name=prefix + 'dwconv')(x)
    x = layers.BatchNormalization(axis=bn_axis, name=prefix + 'bn')(x)
    x = layers.Activation(activation, name=prefix + 'activation')(x)

    # Squeeze and Excitation phase
    if has_se:
        num_reduced_filters = max(1, int(
            block_args.input_filters * block_args.se_ratio
        ))
        se_tensor = layers.GlobalAveragePooling2D(name=prefix + 'se_squeeze')(x)

        target_shape = (1, 1, filters) if backend.image_data_format() == 'channels_last' else (filters, 1, 1)
        se_tensor = layers.Reshape(target_shape, name=prefix + 'se_reshape')(se_tensor)
        se_tensor = layers.Conv2D(num_reduced_filters, 1,
                                  activation=activation,
                                  padding='same',
                                  use_bias=True,
                                  kernel_initializer=CONV_KERNEL_INITIALIZER,
                                  name=prefix + 'se_reduce')(se_tensor)
        se_tensor = layers.Conv2D(filters, 1,
                                  activation='sigmoid',
                                  padding='same',
                                  use_bias=True,
                                  kernel_initializer=CONV_KERNEL_INITIALIZER,
                                  name=prefix + 'se_expand')(se_tensor)
        if backend.backend() == 'theano':
            # For the Theano backend, we have to explicitly make
            # the excitation weights broadcastable.
            pattern = ([True, True, True, False] if backend.image_data_format() == 'channels_last'
                       else [True, False, True, True])
            se_tensor = layers.Lambda(
                lambda x: backend.pattern_broadcast(x, pattern),
                name=prefix + 'se_broadcast')(se_tensor)
        x = layers.multiply([x, se_tensor], name=prefix + 'se_excite')

    # Output phase
    x = layers.Conv2D(block_args.output_filters, 1,
                      padding='same',
                      use_bias=False,
                      kernel_initializer=CONV_KERNEL_INITIALIZER,
                      name=prefix + 'project_conv')(x)
    x = layers.BatchNormalization(axis=bn_axis, name=prefix + 'project_bn')(x)
    if block_args.id_skip and all(
            s == 1 for s in block_args.strides
    ) and block_args.input_filters == block_args.output_filters:
        if drop_rate and (drop_rate > 0):
            x = Dropout(drop_rate,
                        noise_shape=(None, 1, 1, 1),
                        name=prefix + 'drop')(x)
        x = layers.add([x, inputs], name=prefix + 'add')

    return x


def EfficientNet_modify(version_tail, changing_point,
                        drop_connect_rate=0.2,
                        depth_divisor=8,
                        blocks_args=DEFAULT_BLOCKS_ARGS,
                        include_top=True,
                        weights=None,
                        input_tensor=None,
                        input_shape=None,
                        pooling=None,
                        classes=1000,
                        **kwargs):
    """Instantiates the EfficientNet architecture using given scaling coefficients.
    Optionally loads weights pre-trained on ImageNet.
    Note that the data format convention used by the model is
    the one specified in your Keras config at `~/.keras/keras.json`.
    # Arguments
        width_coefficient: float, scaling coefficient for network width.
        depth_coefficient: float, scaling coefficient for network depth.
        default_resolution: int, default input image size.
        dropout_rate: float, dropout rate before final classifier layer.
        drop_connect_rate: float, dropout rate at skip connections.
        depth_divisor: int.
        blocks_args: A list of BlockArgs to construct block modules.
        model_name: string, model name.
        include_top: whether to include the fully-connected
            layer at the top of the network.
        weights: one of `None` (random initialization),
              'imagenet' (pre-training on ImageNet),
              or the path to the weights file to be loaded.
        input_tensor: optional Keras tensor
            (i.e. output of `layers.Input()`)
            to use as image input for the model.
        input_shape: optional shape tuple, only to be specified
            if `include_top` is False.
            It should have exactly 3 inputs channels.
        pooling: optional pooling mode for feature extraction
            when `include_top` is `False`.
            - `None` means that the output of the model will be
                the 4D tensor output of the
                last convolutional layer.
            - `avg` means that global average pooling
                will be applied to the output of the
                last convolutional layer, and thus
                the output of the model will be a 2D tensor.
            - `max` means that global max pooling will
                be applied.
        classes: optional number of classes to classify images
            into, only to be specified if `include_top` is True, and
            if no `weights` argument is specified.
    # Returns
        A Keras model instance.
    # Raises
        ValueError: in case of invalid argument for `weights`,
            or invalid input shape.
    """

    width_coefficient = 2.0
    depth_coefficient = 3.1
    dropout_rate = 0.5
    model_name = 'efficientnet_B7_B{}'.format(version_tail)

    if not (weights in {'imagenet', None} or os.path.exists(weights)):
        raise ValueError('The `weights` argument should be either '
                         '`None` (random initialization), `imagenet` '
                         '(pre-training on ImageNet), '
                         'or the path to the weights file to be loaded.')

    if weights == 'imagenet' and include_top and classes != 1000:
        raise ValueError('If using `weights` as `"imagenet"` with `include_top`'
                         ' as true, `classes` should be 1000')

    # Determine proper input shape
    if input_tensor is None:
        img_input = layers.Input(shape=input_shape)
    else:
        if not backend.is_keras_tensor(input_tensor):
            img_input = layers.Input(tensor=input_tensor, shape=input_shape)
        else:
            img_input = input_tensor

    bn_axis = 3 if backend.image_data_format() == 'channels_last' else 1
    activation = get_swish(**kwargs)

    # Build stem
    x = img_input
    if input_shape == (1024, 1024, 3):
        x = layers.Conv2D(64, 7, strides=(4, 4),
                          padding='same',
                          use_bias=False,
                          kernel_initializer=CONV_KERNEL_INITIALIZER,
                          name='stem_conv')(x)
    else:
        x = layers.Conv2D(round_filters(32, width_coefficient, depth_divisor), 3,
                          strides=(2, 2),
                          padding='same',
                          use_bias=False,
                          kernel_initializer=CONV_KERNEL_INITIALIZER,
                          name='stem_conv')(x)
    x = layers.BatchNormalization(axis=bn_axis, name='stem_bn')(x)
    x = layers.Activation(activation, name='stem_activation')(x)

    # Build blocks
    num_blocks_total = sum(block_args.num_repeat for block_args in blocks_args)
    block_num = 0
    for idx, block_args in enumerate(blocks_args):

        if idx == changing_point:
            if version_tail == 0:
                width_coefficient, depth_coefficient, dropout_rate = 1.0, 1.0, 0.2
            elif version_tail == 1:
                width_coefficient, depth_coefficient, dropout_rate = 1.0, 1.1, 0.2
            elif version_tail == 2:
                width_coefficient, depth_coefficient, dropout_rate = 1.1, 1.2, 0.3
            elif version_tail == 3:
                width_coefficient, depth_coefficient, dropout_rate = 1.2, 1.4, 0.3
            elif version_tail == 4:
                width_coefficient, depth_coefficient, dropout_rate = 1.4, 1.8, 0.4
            elif version_tail == 5:
                width_coefficient, depth_coefficient, dropout_rate = 1.6, 2.2, 0.4
            elif version_tail == 6:
                width_coefficient, depth_coefficient, dropout_rate = 1.8, 2.6, 0.5

        assert block_args.num_repeat > 0
        # Update block input and output filters based on depth multiplier.
        block_args = block_args._replace(
            input_filters=round_filters(block_args.input_filters,
                                        width_coefficient, depth_divisor),
            output_filters=round_filters(block_args.output_filters,
                                         width_coefficient, depth_divisor),
            num_repeat=round_repeats(block_args.num_repeat, depth_coefficient))

        # The first block needs to take care of stride and filter size increase.
        drop_rate = drop_connect_rate * float(block_num) / num_blocks_total
        x = mb_conv_block(x, block_args,
                          activation=activation,
                          drop_rate=drop_rate,
                          prefix='block{}a_'.format(idx + 1))
        block_num += 1
        if block_args.num_repeat > 1:
            # pylint: disable=protected-access
            block_args = block_args._replace(
                input_filters=block_args.output_filters, strides=[1, 1])
            # pylint: enable=protected-access
            for bidx in xrange(block_args.num_repeat - 1):
                drop_rate = drop_connect_rate * float(block_num) / num_blocks_total
                block_prefix = 'block{}{}_'.format(
                    idx + 1,
                    string.ascii_lowercase[bidx + 1]
                )
                x = mb_conv_block(x, block_args,
                                  activation=activation,
                                  drop_rate=drop_rate,
                                  prefix=block_prefix)
                block_num += 1

    # Build top
    x = layers.Conv2D(round_filters(1280, width_coefficient, depth_divisor), 1,
                      padding='same',
                      use_bias=False,
                      kernel_initializer=CONV_KERNEL_INITIALIZER,
                      name='top_conv')(x)
    x = layers.BatchNormalization(axis=bn_axis, name='top_bn')(x)
    x = layers.Activation(activation, name='top_activation')(x)
    if include_top:
        x = layers.GlobalAveragePooling2D(name='avg_pool')(x)
        if dropout_rate and dropout_rate > 0:
            x = layers.Dropout(dropout_rate, name='top_dropout')(x)
        x = layers.Dense(classes,
                         activation='softmax',
                         kernel_initializer=DENSE_KERNEL_INITIALIZER,
                         name='probs')(x)
    else:
        if pooling == 'avg':
            x = layers.GlobalAveragePooling2D(name='avg_pool')(x)
        elif pooling == 'max':
            x = layers.GlobalMaxPooling2D(name='max_pool')(x)

    # Ensure that the model takes into account
    # any potential predecessors of `input_tensor`.
    if input_tensor is not None:
        inputs = keras_utils.get_source_inputs(input_tensor)
    else:
        inputs = img_input

    # Create model.
    model = models.Model(inputs, x, name=model_name)

    # Load weights.
    if weights == 'imagenet':
        if include_top:
            file_name = model_name + '_weights_tf_dim_ordering_tf_kernels_autoaugment.h5'
            file_hash = WEIGHTS_HASHES[model_name][0]
        else:
            file_name = model_name + '_weights_tf_dim_ordering_tf_kernels_autoaugment_notop.h5'
            file_hash = WEIGHTS_HASHES[model_name][1]
        weights_path = keras_utils.get_file(file_name,
                                            BASE_WEIGHTS_PATH + file_name,
                                            cache_subdir='models',
                                            file_hash=file_hash)
        model.load_weights(weights_path)
    elif weights is not None:
        model.load_weights(weights)

    return model


def EfficientNet_multiheads(drop_connect_rate=0.2,
                            depth_divisor=8,
                            blocks_args=DEFAULT_BLOCKS_ARGS,
                            include_top=True,
                            weights=None,
                            input_tensor=None,
                            input_shape=None,
                            pooling=None,
                            classes=1000,
                            **kwargs):

    width_coefficient = 2.0
    depth_coefficient = 3.1
    dropout_rate = 0.5
    model_name = 'efficientnet_B7_B0_multiheads'
    changing_point = 3

    if not (weights in {'imagenet', None} or os.path.exists(weights)):
        raise ValueError('The `weights` argument should be either '
                         '`None` (random initialization), `imagenet` '
                         '(pre-training on ImageNet), '
                         'or the path to the weights file to be loaded.')

    if weights == 'imagenet' and include_top and classes != 1000:
        raise ValueError('If using `weights` as `"imagenet"` with `include_top`'
                         ' as true, `classes` should be 1000')

    # Determine proper input shape
    if input_tensor is None:
        img_input = layers.Input(shape=input_shape)
    else:
        if not backend.is_keras_tensor(input_tensor):
            img_input = layers.Input(tensor=input_tensor, shape=input_shape)
        else:
            img_input = input_tensor

    bn_axis = 3 if backend.image_data_format() == 'channels_last' else 1
    activation = get_swish(**kwargs)

    # Build stem
    x = img_input
    if input_shape == (1024, 1024, 3):
        x = layers.Conv2D(64, 7, strides=(4, 4),
                          padding='same',
                          use_bias=False,
                          kernel_initializer=CONV_KERNEL_INITIALIZER,
                          name='stem_conv')(x)
    else:
        x = layers.Conv2D(round_filters(32, width_coefficient, depth_divisor), 3,
                          strides=(2, 2),
                          padding='same',
                          use_bias=False,
                          kernel_initializer=CONV_KERNEL_INITIALIZER,
                          name='stem_conv')(x)
    x = layers.BatchNormalization(axis=bn_axis, name='stem_bn')(x)
    x = layers.Activation(activation, name='stem_activation')(x)

    # Build blocks
    num_blocks_total = sum(block_args.num_repeat for block_args in blocks_args)
    block_num = 0
    list_output = []
    for idx, block_args in enumerate(blocks_args):
        if idx == changing_point:
            break
        assert block_args.num_repeat > 0
        # Update block input and output filters based on depth multiplier.
        block_args = block_args._replace(
            input_filters=round_filters(block_args.input_filters,
                                        width_coefficient, depth_divisor),
            output_filters=round_filters(block_args.output_filters,
                                         width_coefficient, depth_divisor),
            num_repeat=round_repeats(block_args.num_repeat, depth_coefficient))

        # The first block needs to take care of stride and filter size increase.
        drop_rate = drop_connect_rate * float(block_num) / num_blocks_total
        x = mb_conv_block(x, block_args,
                          activation=activation,
                          drop_rate=drop_rate,
                          prefix='block{}a_'.format(idx + 1))
        block_num += 1
        if block_args.num_repeat > 1:
            # pylint: disable=protected-access
            block_args = block_args._replace(
                input_filters=block_args.output_filters, strides=[1, 1])
            # pylint: enable=protected-access
            for bidx in xrange(block_args.num_repeat - 1):
                drop_rate = drop_connect_rate * float(block_num) / num_blocks_total
                block_prefix = 'block{}{}_'.format(
                    idx + 1,
                    string.ascii_lowercase[bidx + 1]
                )
                x = mb_conv_block(x, block_args,
                                  activation=activation,
                                  drop_rate=drop_rate,
                                  prefix=block_prefix)
                block_num += 1
    encoder_out = x
    width_coefficient, depth_coefficient, dropout_rate = 1.0, 1.0, 0.2
    for finding in LIST_FINDINGS:
        x = encoder_out
        for idx, block_args in enumerate(blocks_args):

            if idx < changing_point:
                continue

            assert block_args.num_repeat > 0
            # Update block input and output filters based on depth multiplier.
            block_args = block_args._replace(
                input_filters=round_filters(block_args.input_filters,
                                            width_coefficient, depth_divisor),
                output_filters=round_filters(block_args.output_filters,
                                             width_coefficient, depth_divisor),
                num_repeat=round_repeats(block_args.num_repeat, depth_coefficient))

            # The first block needs to take care of stride and filter size increase.
            drop_rate = drop_connect_rate * float(block_num) / num_blocks_total
            x = mb_conv_block(x, block_args,
                              activation=activation,
                              drop_rate=drop_rate,
                              prefix='block{}a_{}_'.format(idx + 1, finding))
            block_num += 1
            if block_args.num_repeat > 1:
                # pylint: disable=protected-access
                block_args = block_args._replace(
                    input_filters=block_args.output_filters, strides=[1, 1])
                # pylint: enable=protected-access
                for bidx in xrange(block_args.num_repeat - 1):
                    drop_rate = drop_connect_rate * float(block_num) / num_blocks_total
                    block_prefix = 'block{}{}_{}_'.format(
                        idx + 1,
                        string.ascii_lowercase[bidx + 1],
                        finding
                    )
                    x = mb_conv_block(x, block_args,
                                      activation=activation,
                                      drop_rate=drop_rate,
                                      prefix=block_prefix)
                    block_num += 1

        # Build top
        x = layers.Conv2D(round_filters(1280, width_coefficient, depth_divisor), 1,
                          padding='same',
                          use_bias=False,
                          kernel_initializer=CONV_KERNEL_INITIALIZER,
                          name='top_conv_{}'.format(finding))(x)
        x = layers.BatchNormalization(axis=bn_axis, name='top_bn_{}'.format(finding))(x)
        x = layers.Activation(activation, name='top_activation_{}'.format(finding))(x)

        final_result = layers.Conv2D(1, (1, 1), padding="same", name="1x1conv_{}".format(finding))(x)
        activation_outs = layers.Activation('sigmoid', name="activation_{}".format(finding))(final_result)
        gap = layers.GlobalAveragePooling2D(name="GAP_{}".format(finding))(final_result)
        outputs = layers.Activation('sigmoid', name="result_{}".format(finding))(gap)
        list_output.append(outputs)
        list_output.append(activation_outs)

    model = models.Model(img_input, list_output)
    return model


def set_output_lesion_segmentation_v2(fundus_network):

    division_factor=16
    kernel_size=3
    
    # set outputs and generate model
    list_featuremap = []
    for finding in LIST_FINDINGS:
        featuremap = fundus_network.get_layer("top_activation_{}".format(finding)).output
        featuremap = Conv2D(fundus_network.get_layer("top_activation_{}".format(finding)).output_shape[-1]//division_factor, (1, 1), padding="same", activation=get_swish(), name="final_layer_{}".format(finding))(featuremap)
        list_featuremap.append(featuremap)
    down_layer = Concatenate(axis=3)(list_featuremap)
    # upper layer 1 (16x16)
    list_featuremap = []
    for finding in LIST_FINDINGS:
        featuremap = fundus_network.get_layer("block7a_{}_expand_activation".format(finding)).output
        featuremap = Conv2D(fundus_network.get_layer("block7a_{}_expand_activation".format(finding)).output_shape[-1]//division_factor, (1, 1), padding="same", activation=get_swish(), name="upper_layer_1_{}".format(finding))(featuremap)
        list_featuremap.append(featuremap)
    upper_layer = Concatenate(axis=3)(list_featuremap)
    down_layer = Concatenate(axis=3)([UpSampling2D(size=(2, 2))(down_layer), upper_layer])
    down_layer = layers.DepthwiseConv2D(kernel_size, padding='same', use_bias=False, depthwise_initializer=CONV_KERNEL_INITIALIZER, name='dwconv_up1_1')(down_layer)
    down_layer = layers.BatchNormalization(axis=-1, name='bn1_1')(down_layer)
    down_layer = layers.Activation(get_swish(), name='activation1_1')(down_layer)
    down_layer = layers.DepthwiseConv2D(kernel_size, padding='same', use_bias=False, depthwise_initializer=CONV_KERNEL_INITIALIZER, name='dwconv_up1_2')(down_layer)
    down_layer = layers.BatchNormalization(axis=-1, name='bn1_2')(down_layer)
    down_layer = layers.Activation(get_swish(), name='activation1_2')(down_layer)
    # upper layer 2 (32x32)
    down_layer = Conv2D(fundus_network.get_layer("block6a_Hemorrhage_expand_activation").output_shape[-1], (1, 1), padding="same", activation=get_swish(), name="down_layer_1x1_2")(down_layer)
    list_featuremap = []
    for finding in LIST_FINDINGS:
        featuremap = fundus_network.get_layer("block6a_{}_expand_activation".format(finding)).output
        featuremap = Conv2D(fundus_network.get_layer("block6a_{}_expand_activation".format(finding)).output_shape[-1]//division_factor, (1, 1), padding="same", activation=get_swish(), name="upper_layer_2_{}".format(finding))(featuremap)
        list_featuremap.append(featuremap)
    upper_layer = Concatenate(axis=3)(list_featuremap)
    down_layer = Concatenate(axis=3)([UpSampling2D(size=(2, 2))(down_layer), upper_layer])
    down_layer = layers.DepthwiseConv2D(kernel_size, padding='same', use_bias=False, depthwise_initializer=CONV_KERNEL_INITIALIZER, name='dwconv_up2_1')(down_layer)
    down_layer = layers.BatchNormalization(axis=-1, name='bn2_1')(down_layer)
    down_layer = layers.Activation(get_swish(), name='activation2_1')(down_layer)
    down_layer = layers.DepthwiseConv2D(kernel_size, padding='same', use_bias=False, depthwise_initializer=CONV_KERNEL_INITIALIZER, name='dwconv_up2_2')(down_layer)
    down_layer = layers.BatchNormalization(axis=-1, name='bn2_2')(down_layer)
    down_layer = layers.Activation(get_swish(), name='activation2_2')(down_layer)
    # upper layer 3 (64x64)
    down_layer = Conv2D(fundus_network.get_layer("block4a_Hemorrhage_expand_activation").output_shape[-1], (1, 1), padding="same", activation=get_swish(), name="down_layer_1x1_3")(down_layer)
    list_featuremap = []
    for finding in LIST_FINDINGS:
        featuremap = fundus_network.get_layer("block4a_{}_expand_activation".format(finding)).output
        featuremap = Conv2D(fundus_network.get_layer("block4a_{}_expand_activation".format(finding)).output_shape[-1]//division_factor, (1, 1), padding="same", activation=get_swish(), name="upper_layer_3_{}".format(finding))(featuremap)
        list_featuremap.append(featuremap)
    upper_layer = Concatenate(axis=3)(list_featuremap)
    down_layer = Concatenate(axis=3)([UpSampling2D(size=(2, 2))(down_layer), upper_layer])
    down_layer = layers.DepthwiseConv2D(kernel_size, padding='same', use_bias=False, depthwise_initializer=CONV_KERNEL_INITIALIZER, name='dwconv_up3_1')(down_layer)
    down_layer = layers.BatchNormalization(axis=-1, name='bn3_1')(down_layer)
    down_layer = layers.Activation(get_swish(), name='activation3_1')(down_layer)
    down_layer = layers.DepthwiseConv2D(kernel_size, padding='same', use_bias=False, depthwise_initializer=CONV_KERNEL_INITIALIZER, name='dwconv_up3_2')(down_layer)
    down_layer = layers.BatchNormalization(axis=-1, name='bn3_2')(down_layer)
    down_layer = layers.Activation(get_swish(), name='activation3_2')(down_layer)
    # upper layer 4 (128x128)
    down_layer = Conv2D(fundus_network.get_layer("block2f_activation").output_shape[-1], (1, 1), padding="same", activation=get_swish(), name="down_layer_1x1_4")(down_layer)
    upper_layer = fundus_network.get_layer("block2f_activation").output
    down_layer = Concatenate(axis=3)([UpSampling2D(size=(2, 2))(down_layer), upper_layer])
    down_layer = layers.DepthwiseConv2D(kernel_size, padding='same', use_bias=False, depthwise_initializer=CONV_KERNEL_INITIALIZER, name='dwconv_up4_1')(down_layer)
    down_layer = layers.BatchNormalization(axis=-1, name='bn4_1')(down_layer)
    down_layer = layers.Activation(get_swish(), name='activation4_1')(down_layer)
    down_layer = layers.DepthwiseConv2D(kernel_size, padding='same', use_bias=False, depthwise_initializer=CONV_KERNEL_INITIALIZER, name='dwconv_up4_2')(down_layer)
    down_layer = layers.BatchNormalization(axis=-1, name='bn4_2')(down_layer)
    down_layer = layers.Activation(get_swish(), name='activation4_2')(down_layer)
    
    # upper layer 5 (256x256)
    down_layer = Conv2D(fundus_network.get_layer("block2a_expand_activation").output_shape[-1], (1, 1), padding="same", activation=get_swish(), name="down_layer_1x1_5")(down_layer)
    upper_layer = fundus_network.get_layer("block2a_expand_activation").output
    down_layer = Concatenate(axis=3)([UpSampling2D(size=(2, 2))(down_layer), upper_layer])
    down_layer = layers.DepthwiseConv2D(kernel_size, padding='same', use_bias=False, depthwise_initializer=CONV_KERNEL_INITIALIZER, name='dwconv_up5_1')(down_layer)
    down_layer = layers.BatchNormalization(axis=-1, name='bn5_1')(down_layer)
    down_layer = layers.Activation(get_swish(), name='activation5_1')(down_layer)
    down_layer = layers.DepthwiseConv2D(kernel_size, padding='same', use_bias=False, depthwise_initializer=CONV_KERNEL_INITIALIZER, name='dwconv_up5_2')(down_layer)
    down_layer = layers.BatchNormalization(axis=-1, name='bn5_2')(down_layer)
    down_layer = layers.Activation(get_swish(), name='activation5_2')(down_layer)
    featuremap = Conv2D(32, (7, 7), padding="same", activation=get_swish(), name="down_layer_1x1_final")(down_layer)
    
    confidence_map = Conv2D(6, (7, 7), strides=(1, 1), padding="same", activation='softmax')(featuremap)

    network = Model(fundus_network.input, confidence_map, name="lesion_segmentor")
    return network


def set_output_lesion_segmentation_v3(fundus_network):

    division_factor=16
    kernel_size=3
    
    # set outputs and generate model
    list_featuremap = []
    for finding in LIST_FINDINGS:
        featuremap = fundus_network.get_layer("top_activation_{}".format(finding)).output
        featuremap = Conv2D(fundus_network.get_layer("top_activation_{}".format(finding)).output_shape[-1]//division_factor, (1, 1), padding="same", activation=get_swish(), name="final_layer_{}".format(finding))(featuremap)
        list_featuremap.append(featuremap)
    down_layer = Concatenate(axis=3)(list_featuremap)
    # upper layer 1 (16x16)
    list_featuremap = []
    for finding in LIST_FINDINGS:
        featuremap = fundus_network.get_layer("block7a_{}_expand_activation".format(finding)).output
        featuremap = Conv2D(fundus_network.get_layer("block7a_{}_expand_activation".format(finding)).output_shape[-1]//division_factor, (1, 1), padding="same", activation=get_swish(), name="upper_layer_1_{}".format(finding))(featuremap)
        list_featuremap.append(featuremap)
    upper_layer = Concatenate(axis=3)(list_featuremap)
    down_layer = Concatenate(axis=3)([UpSampling2D(size=(2, 2))(down_layer), upper_layer])
    down_layer = layers.DepthwiseConv2D(kernel_size, padding='same', use_bias=False, depthwise_initializer=CONV_KERNEL_INITIALIZER, name='dwconv_up1_1')(down_layer)
    down_layer = layers.BatchNormalization(axis=-1, name='bn1_1')(down_layer)
    down_layer = layers.Activation(get_swish(), name='activation1_1')(down_layer)
    down_layer = layers.DepthwiseConv2D(kernel_size, padding='same', use_bias=False, depthwise_initializer=CONV_KERNEL_INITIALIZER, name='dwconv_up1_2')(down_layer)
    down_layer = layers.BatchNormalization(axis=-1, name='bn1_2')(down_layer)
    down_layer = layers.Activation(get_swish(), name='activation1_2')(down_layer)
    # upper layer 2 (32x32)
    down_layer = Conv2D(fundus_network.get_layer("block6a_Hemorrhage_expand_activation").output_shape[-1], (1, 1), padding="same", activation=get_swish(), name="down_layer_1x1_2")(down_layer)
    list_featuremap = []
    for finding in LIST_FINDINGS:
        featuremap = fundus_network.get_layer("block6a_{}_expand_activation".format(finding)).output
        featuremap = Conv2D(fundus_network.get_layer("block6a_{}_expand_activation".format(finding)).output_shape[-1]//division_factor, (1, 1), padding="same", activation=get_swish(), name="upper_layer_2_{}".format(finding))(featuremap)
        list_featuremap.append(featuremap)
    upper_layer = Concatenate(axis=3)(list_featuremap)
    down_layer = Concatenate(axis=3)([UpSampling2D(size=(2, 2))(down_layer), upper_layer])
    down_layer = layers.DepthwiseConv2D(kernel_size, padding='same', use_bias=False, depthwise_initializer=CONV_KERNEL_INITIALIZER, name='dwconv_up2_1')(down_layer)
    down_layer = layers.BatchNormalization(axis=-1, name='bn2_1')(down_layer)
    down_layer = layers.Activation(get_swish(), name='activation2_1')(down_layer)
    down_layer = layers.DepthwiseConv2D(kernel_size, padding='same', use_bias=False, depthwise_initializer=CONV_KERNEL_INITIALIZER, name='dwconv_up2_2')(down_layer)
    down_layer = layers.BatchNormalization(axis=-1, name='bn2_2')(down_layer)
    down_layer = layers.Activation(get_swish(), name='activation2_2')(down_layer)
    down_layer = layers.DepthwiseConv2D(kernel_size, padding='same', use_bias=False, depthwise_initializer=CONV_KERNEL_INITIALIZER, name='dwconv_up2_3')(down_layer)
    down_layer = layers.BatchNormalization(axis=-1, name='bn2_3')(down_layer)
    down_layer = layers.Activation(get_swish(), name='activation2_3')(down_layer)
    # upper layer 3 (64x64)
    down_layer = Conv2D(fundus_network.get_layer("block4a_Hemorrhage_expand_activation").output_shape[-1], (1, 1), padding="same", activation=get_swish(), name="down_layer_1x1_3")(down_layer)
    list_featuremap = []
    for finding in LIST_FINDINGS:
        featuremap = fundus_network.get_layer("block4a_{}_expand_activation".format(finding)).output
        featuremap = Conv2D(fundus_network.get_layer("block4a_{}_expand_activation".format(finding)).output_shape[-1]//division_factor, (1, 1), padding="same", activation=get_swish(), name="upper_layer_3_{}".format(finding))(featuremap)
        list_featuremap.append(featuremap)
    upper_layer = Concatenate(axis=3)(list_featuremap)
    down_layer = Concatenate(axis=3)([UpSampling2D(size=(2, 2))(down_layer), upper_layer])
    down_layer = layers.DepthwiseConv2D(kernel_size, padding='same', use_bias=False, depthwise_initializer=CONV_KERNEL_INITIALIZER, name='dwconv_up3_1')(down_layer)
    down_layer = layers.BatchNormalization(axis=-1, name='bn3_1')(down_layer)
    down_layer = layers.Activation(get_swish(), name='activation3_1')(down_layer)
    down_layer = layers.DepthwiseConv2D(kernel_size, padding='same', use_bias=False, depthwise_initializer=CONV_KERNEL_INITIALIZER, name='dwconv_up3_2')(down_layer)
    down_layer = layers.BatchNormalization(axis=-1, name='bn3_2')(down_layer)
    down_layer = layers.Activation(get_swish(), name='activation3_2')(down_layer)
    down_layer = layers.DepthwiseConv2D(kernel_size, padding='same', use_bias=False, depthwise_initializer=CONV_KERNEL_INITIALIZER, name='dwconv_up3_3')(down_layer)
    down_layer = layers.BatchNormalization(axis=-1, name='bn3_3')(down_layer)
    down_layer = layers.Activation(get_swish(), name='activation3_3')(down_layer)
    down_layer = layers.DepthwiseConv2D(kernel_size, padding='same', use_bias=False, depthwise_initializer=CONV_KERNEL_INITIALIZER, name='dwconv_up3_4')(down_layer)
    down_layer = layers.BatchNormalization(axis=-1, name='bn3_4')(down_layer)
    down_layer = layers.Activation(get_swish(), name='activation3_4')(down_layer)
    featuremap = Conv2D(32, (3, 3), padding="same", activation=get_swish(), name="down_layer_1x1_final")(down_layer)
    
    confidence_map = Conv2D(6, (3, 3), strides=(1, 1), padding="same", activation='softmax')(featuremap)

    network = Model(fundus_network.input, confidence_map, name="lesion_segmentor")
    return network


def set_output_lesion_segmentation_v4(fundus_network, kernel_size=3):

    n_findings = 15
    division_factor = 64
    
    # set outputs and generate model
    list_featuremap = []
    for finding in LIST_FINDINGS:
        featuremap = fundus_network.get_layer("top_activation_{}".format(finding)).output
        featuremap = Conv2D(fundus_network.get_layer("top_activation_{}".format(finding)).output_shape[-1]//division_factor, (1, 1), padding="same", activation=get_swish(), name="final_layer_{}".format(finding))(featuremap)
        list_featuremap.append(featuremap)
    down_layer = Concatenate(axis=3)(list_featuremap)
    down_layer = Conv2D(n_findings * fundus_network.get_layer("block7a_Hemorrhage_expand_activation").output_shape[-1]//division_factor, (kernel_size, kernel_size), strides=(1, 1),name='dwconv_up0_1', padding="same")(down_layer)
    down_layer = layers.BatchNormalization(axis=-1, name='bn0_1')(down_layer)
    down_layer = layers.Activation(get_swish(), name='activation0_1')(down_layer)
    # upper layer 1 (16x16)
    list_featuremap = []
    for finding in LIST_FINDINGS:
        featuremap = fundus_network.get_layer("block7a_{}_expand_activation".format(finding)).output
        featuremap = Conv2D(fundus_network.get_layer("block7a_{}_expand_activation".format(finding)).output_shape[-1]//division_factor, (1, 1), padding="same", activation=get_swish(), name="upper_layer_1_{}".format(finding))(featuremap)
        list_featuremap.append(featuremap)
    upper_layer = Concatenate(axis=3)(list_featuremap)
    down_layer = Concatenate(axis=3)([UpSampling2D(size=(2, 2))(down_layer), upper_layer])
    down_layer = Conv2D(n_findings * fundus_network.get_layer("block6a_Hemorrhage_expand_activation").output_shape[-1]//division_factor, (kernel_size, kernel_size), strides=(1, 1),name='conv_up1_1', padding="same")(down_layer)
    down_layer = layers.BatchNormalization(axis=-1, name='bn1_1')(down_layer)
    down_layer = layers.Activation(get_swish(), name='activation1_1')(down_layer)
    down_layer = Conv2D(n_findings * fundus_network.get_layer("block6a_Hemorrhage_expand_activation").output_shape[-1]//division_factor, (kernel_size, kernel_size), strides=(1, 1),name='conv_up1_2', padding="same")(down_layer)
    down_layer = layers.BatchNormalization(axis=-1, name='bn1_2')(down_layer)
    down_layer = layers.Activation(get_swish(), name='activation1_2')(down_layer)
    # upper layer 2 (32x32)
    list_featuremap = []
    for finding in LIST_FINDINGS:
        featuremap = fundus_network.get_layer("block6a_{}_expand_activation".format(finding)).output
        featuremap = Conv2D(fundus_network.get_layer("block6a_{}_expand_activation".format(finding)).output_shape[-1]//division_factor, (1, 1), padding="same", activation=get_swish(), name="upper_layer_2_{}".format(finding))(featuremap)
        list_featuremap.append(featuremap)
    upper_layer = Concatenate(axis=3)(list_featuremap)
    down_layer = Concatenate(axis=3)([UpSampling2D(size=(2, 2))(down_layer), upper_layer])
    down_layer = Conv2D(n_findings * fundus_network.get_layer("block4a_Hemorrhage_expand_activation").output_shape[-1]//division_factor, (kernel_size, kernel_size), strides=(1, 1),name='conv_up2_1', padding="same")(down_layer)
    down_layer = layers.BatchNormalization(axis=-1, name='bn2_1')(down_layer)
    down_layer = layers.Activation(get_swish(), name='activation2_1')(down_layer)
    down_layer = Conv2D(n_findings * fundus_network.get_layer("block4a_Hemorrhage_expand_activation").output_shape[-1]//division_factor, (kernel_size, kernel_size), strides=(1, 1),name='conv_up2_2', padding="same")(down_layer)
    down_layer = layers.BatchNormalization(axis=-1, name='bn2_2')(down_layer)
    down_layer = layers.Activation(get_swish(), name='activation2_2')(down_layer)
    down_layer = Conv2D(n_findings * fundus_network.get_layer("block4a_Hemorrhage_expand_activation").output_shape[-1]//division_factor, (kernel_size, kernel_size), strides=(1, 1),name='conv_up2_3', padding="same")(down_layer)
    down_layer = layers.BatchNormalization(axis=-1, name='bn2_3')(down_layer)
    down_layer = layers.Activation(get_swish(), name='activation2_3')(down_layer)
    
    # upper layer 3 (64x64)
    list_featuremap = []
    for finding in LIST_FINDINGS:
        featuremap = fundus_network.get_layer("block4a_{}_expand_activation".format(finding)).output
        featuremap = Conv2D(fundus_network.get_layer("block4a_{}_expand_activation".format(finding)).output_shape[-1]//division_factor, (1, 1), padding="same", activation=get_swish(), name="upper_layer_3_{}".format(finding))(featuremap)
        list_featuremap.append(featuremap)
    upper_layer = Concatenate(axis=3)(list_featuremap)
    down_layer = Concatenate(axis=3)([UpSampling2D(size=(2, 2))(down_layer), upper_layer])
    down_layer = Conv2D(n_findings * fundus_network.get_layer("block4a_Hemorrhage_expand_activation").output_shape[-1]//division_factor, (kernel_size, kernel_size), strides=(1, 1),name='conv_up3_1', padding="same")(down_layer)
    down_layer = layers.BatchNormalization(axis=-1, name='bn3_1')(down_layer)
    down_layer = layers.Activation(get_swish(), name='activation3_1')(down_layer)
    down_layer = Conv2D(n_findings * fundus_network.get_layer("block4a_Hemorrhage_expand_activation").output_shape[-1]//division_factor, (kernel_size, kernel_size), strides=(1, 1),name='conv_up3_2', padding="same")(down_layer)
    down_layer = layers.BatchNormalization(axis=-1, name='bn3_2')(down_layer)
    down_layer = layers.Activation(get_swish(), name='activation3_2')(down_layer)
    down_layer = Conv2D(n_findings * fundus_network.get_layer("block4a_Hemorrhage_expand_activation").output_shape[-1]//division_factor, (kernel_size, kernel_size), strides=(1, 1),name='conv_up3_3', padding="same")(down_layer)
    down_layer = layers.BatchNormalization(axis=-1, name='bn3_3')(down_layer)
    down_layer = layers.Activation(get_swish(), name='activation3_3')(down_layer)
    down_layer = Conv2D(n_findings * fundus_network.get_layer("block4a_Hemorrhage_expand_activation").output_shape[-1]//division_factor, (kernel_size, kernel_size), strides=(1, 1),name='conv_up3_4', padding="same")(down_layer)
    down_layer = layers.BatchNormalization(axis=-1, name='bn3_4')(down_layer)
    down_layer = layers.Activation(get_swish(), name='activation3_4')(down_layer)
    down_layer = Conv2D(32, (7, 7), padding="same", activation=get_swish(), name="down_layer_1x1_final")(down_layer)
    
    confidence_map = Conv2D(6, (7, 7), strides=(1, 1), padding="same", activation='softmax')(down_layer)

    network = Model(fundus_network.input, confidence_map, name="lesion_segmentor")
    return network

def set_output_lesion_segmentation_individual(fundus_network, kernel_size):

    division_factor = 64
    n_findings = 15
    
    # set outputs and generate model
    list_featuremap = []
    for finding in LIST_FINDINGS:
        featuremap = fundus_network.get_layer("top_activation_{}".format(finding)).output
        featuremap = Conv2D(fundus_network.get_layer("top_activation_{}".format(finding)).output_shape[-1]//division_factor, (1, 1), padding="same", name="final_layer_{}".format(finding))(featuremap)
        list_featuremap.append(featuremap)
    down_layer = Concatenate(axis=3)(list_featuremap)
    # upper layer 1 (16x16)
    n_ch_ind_finding = fundus_network.get_layer("block7a_Hemorrhage_expand_activation").output_shape[-1]//division_factor
    n_ch_upperlayer1 = n_findings * n_ch_ind_finding
    down_layer = Conv2D(n_ch_upperlayer1, (1, 1), padding="same", name="down_layer_1x1_1_0")(down_layer)
    list_featuremap = []
    for finding in LIST_FINDINGS:
        featuremap = fundus_network.get_layer("block7a_{}_expand_activation".format(finding)).output
        featuremap = Conv2D(n_ch_ind_finding, (1, 1), padding="same", name="upper_layer_1_{}".format(finding))(featuremap)
        list_featuremap.append(featuremap)
    upper_layer = Concatenate(axis=3)(list_featuremap)
    down_layer = Concatenate(axis=3)([UpSampling2D(size=(2, 2))(down_layer), upper_layer])
    down_layer = layers.DepthwiseConv2D(kernel_size, padding='same', use_bias=False, depthwise_initializer=CONV_KERNEL_INITIALIZER, name='dwconv_up1_1')(down_layer)
    down_layer = layers.BatchNormalization(axis=-1, name='bn1_1')(down_layer)
    down_layer = layers.Activation(get_swish(), name='activation1_1')(down_layer)
    down_layer = Conv2D(2 * n_ch_upperlayer1, (1, 1), padding="same", name="down_layer_1x1_1_1")(down_layer)
    down_layer = layers.DepthwiseConv2D(kernel_size, padding='same', use_bias=False, depthwise_initializer=CONV_KERNEL_INITIALIZER, name='dwconv_up1_2')(down_layer)
    down_layer = layers.BatchNormalization(axis=-1, name='bn1_2')(down_layer)
    down_layer = layers.Activation(get_swish(), name='activation1_2')(down_layer)
    # upper layer 2 (32x32)
    n_ch_ind_finding = fundus_network.get_layer("block6a_Hemorrhage_expand_activation").output_shape[-1]//division_factor
    n_ch_upperlayer2 = n_findings * n_ch_ind_finding
    down_layer = Conv2D(n_ch_upperlayer2, (1, 1), padding="same", name="down_layer_1x1_2_0")(down_layer)
    list_featuremap = []
    for finding in LIST_FINDINGS:
        featuremap = fundus_network.get_layer("block6a_{}_expand_activation".format(finding)).output
        featuremap = Conv2D(n_ch_ind_finding, (1, 1), padding="same", name="upper_layer_2_{}".format(finding))(featuremap)
        list_featuremap.append(featuremap)
    upper_layer = Concatenate(axis=3)(list_featuremap)
    down_layer = Concatenate(axis=3)([UpSampling2D(size=(2, 2))(down_layer), upper_layer])
    down_layer = layers.DepthwiseConv2D(kernel_size, padding='same', use_bias=False, depthwise_initializer=CONV_KERNEL_INITIALIZER, name='dwconv_up2_1')(down_layer)
    down_layer = layers.BatchNormalization(axis=-1, name='bn2_1')(down_layer)
    down_layer = layers.Activation(get_swish(), name='activation2_1')(down_layer)
    down_layer = Conv2D(2 * n_ch_upperlayer2, (1, 1), padding="same", name="down_layer_1x1_2_1")(down_layer)
    down_layer = layers.DepthwiseConv2D(kernel_size, padding='same', use_bias=False, depthwise_initializer=CONV_KERNEL_INITIALIZER, name='dwconv_up2_2')(down_layer)
    down_layer = layers.BatchNormalization(axis=-1, name='bn2_2')(down_layer)
    down_layer = layers.Activation(get_swish(), name='activation2_2')(down_layer)
    down_layer = Conv2D(2 * n_ch_upperlayer2, (1, 1), padding="same", name="down_layer_1x1_2_2")(down_layer)
    down_layer = layers.DepthwiseConv2D(kernel_size, padding='same', use_bias=False, depthwise_initializer=CONV_KERNEL_INITIALIZER, name='dwconv_up2_3')(down_layer)
    down_layer = layers.BatchNormalization(axis=-1, name='bn2_3')(down_layer)
    down_layer = layers.Activation(get_swish(), name='activation2_3')(down_layer)
    # upper layer 3 (64x64)
    n_ch_ind_finding = fundus_network.get_layer("block4a_Hemorrhage_expand_activation").output_shape[-1]//division_factor
    n_ch_upperlayer3 = n_findings * n_ch_ind_finding
    down_layer = Conv2D(n_ch_upperlayer3, (1, 1), padding="same", name="down_layer_1x1_3_0")(down_layer)
    list_featuremap = []
    for finding in LIST_FINDINGS:
        featuremap = fundus_network.get_layer("block4a_{}_expand_activation".format(finding)).output
        featuremap = Conv2D(n_ch_ind_finding, (1, 1), padding="same", name="upper_layer_3_{}".format(finding))(featuremap)
        list_featuremap.append(featuremap)
    upper_layer = Concatenate(axis=3)(list_featuremap)
    down_layer = Concatenate(axis=3)([UpSampling2D(size=(2, 2))(down_layer), upper_layer])
    down_layer = layers.DepthwiseConv2D(kernel_size, padding='same', use_bias=False, depthwise_initializer=CONV_KERNEL_INITIALIZER, name='dwconv_up3_1')(down_layer)
    down_layer = layers.BatchNormalization(axis=-1, name='bn3_1')(down_layer)
    down_layer = layers.Activation(get_swish(), name='activation3_1')(down_layer)
    down_layer = Conv2D(2 * n_ch_upperlayer3, (1, 1), padding="same", name="down_layer_1x1_3_1")(down_layer)
    down_layer = layers.DepthwiseConv2D(kernel_size, padding='same', use_bias=False, depthwise_initializer=CONV_KERNEL_INITIALIZER, name='dwconv_up3_2')(down_layer)
    down_layer = layers.BatchNormalization(axis=-1, name='bn3_2')(down_layer)
    down_layer = layers.Activation(get_swish(), name='activation3_2')(down_layer)
    down_layer = Conv2D(2 * n_ch_upperlayer3, (1, 1), padding="same", name="down_layer_1x1_3_2")(down_layer)
    down_layer = layers.DepthwiseConv2D(kernel_size, padding='same', use_bias=False, depthwise_initializer=CONV_KERNEL_INITIALIZER, name='dwconv_up3_3')(down_layer)
    down_layer = layers.BatchNormalization(axis=-1, name='bn3_3')(down_layer)
    down_layer = layers.Activation(get_swish(), name='activation3_3')(down_layer)
    down_layer = Conv2D(2 * n_ch_upperlayer3, (1, 1), padding="same", name="down_layer_1x1_3_3")(down_layer)
    down_layer = layers.DepthwiseConv2D(kernel_size, padding='same', use_bias=False, depthwise_initializer=CONV_KERNEL_INITIALIZER, name='dwconv_up3_4')(down_layer)
    down_layer = layers.BatchNormalization(axis=-1, name='bn3_4')(down_layer)
    down_layer = layers.Activation(get_swish(), name='activation3_4')(down_layer)
    
    confidence_map = Conv2D(1, (1, 1), strides=(1, 1), padding="same", activation='sigmoid')(down_layer)

    network = Model(fundus_network.input, confidence_map, name="lesion_segmentor")
    return network


def set_output_lesion_segmentation(fundus_network):

    division_factor=16
    kernel_size=3
    
    # set outputs and generate model
    list_featuremap = []
    for finding in LIST_FINDINGS:
        featuremap = fundus_network.get_layer("top_activation_{}".format(finding)).output
        featuremap = Conv2D(fundus_network.get_layer("top_activation_{}".format(finding)).output_shape[-1]//division_factor, (1, 1), padding="same", activation=get_swish(), name="final_layer_{}".format(finding))(featuremap)
        list_featuremap.append(featuremap)
    down_layer = Concatenate(axis=3)(list_featuremap)
    # upper layer 1 (16x16)
    list_featuremap = []
    for finding in LIST_FINDINGS:
        featuremap = fundus_network.get_layer("block7a_{}_expand_activation".format(finding)).output
        featuremap = Conv2D(fundus_network.get_layer("block7a_{}_expand_activation".format(finding)).output_shape[-1]//division_factor, (1, 1), padding="same", activation=get_swish(), name="upper_layer_1_{}".format(finding))(featuremap)
        list_featuremap.append(featuremap)
    upper_layer = Concatenate(axis=3)(list_featuremap)
    down_layer = Concatenate(axis=3)([UpSampling2D(size=(2, 2))(down_layer), upper_layer])
    down_layer = layers.DepthwiseConv2D(kernel_size, padding='same', use_bias=False, depthwise_initializer=CONV_KERNEL_INITIALIZER, name='dwconv_up1')(down_layer)
    # upper layer 2 (32x32)
    down_layer = Conv2D(fundus_network.get_layer("block6a_Hemorrhage_expand_activation").output_shape[-1], (1, 1), padding="same", activation=get_swish(), name="down_layer_1x1_2")(down_layer)
    list_featuremap = []
    for finding in LIST_FINDINGS:
        featuremap = fundus_network.get_layer("block6a_{}_expand_activation".format(finding)).output
        featuremap = Conv2D(fundus_network.get_layer("block6a_{}_expand_activation".format(finding)).output_shape[-1]//division_factor, (1, 1), padding="same", activation=get_swish(), name="upper_layer_2_{}".format(finding))(featuremap)
        list_featuremap.append(featuremap)
    upper_layer = Concatenate(axis=3)(list_featuremap)
    down_layer = Concatenate(axis=3)([UpSampling2D(size=(2, 2))(down_layer), upper_layer])
    down_layer = layers.DepthwiseConv2D(kernel_size, padding='same', use_bias=False, depthwise_initializer=CONV_KERNEL_INITIALIZER, name='dwconv_up2')(down_layer)
    # upper layer 3 (64x64)
    down_layer = Conv2D(fundus_network.get_layer("block4a_Hemorrhage_expand_activation").output_shape[-1], (1, 1), padding="same", activation=get_swish(), name="down_layer_1x1_3")(down_layer)
    list_featuremap = []
    for finding in LIST_FINDINGS:
        featuremap = fundus_network.get_layer("block4a_{}_expand_activation".format(finding)).output
        featuremap = Conv2D(fundus_network.get_layer("block4a_{}_expand_activation".format(finding)).output_shape[-1]//division_factor, (1, 1), padding="same", activation=get_swish(), name="upper_layer_3_{}".format(finding))(featuremap)
        list_featuremap.append(featuremap)
    upper_layer = Concatenate(axis=3)(list_featuremap)
    down_layer = Concatenate(axis=3)([UpSampling2D(size=(2, 2))(down_layer), upper_layer])
    down_layer = layers.DepthwiseConv2D(kernel_size, padding='same', use_bias=False, depthwise_initializer=CONV_KERNEL_INITIALIZER, name='dwconv_up3')(down_layer)
    # upper layer 4 (128x128)
    down_layer = Conv2D(fundus_network.get_layer("block2f_activation").output_shape[-1], (1, 1), padding="same", activation=get_swish(), name="down_layer_1x1_4")(down_layer)
    upper_layer = fundus_network.get_layer("block2f_activation").output
    down_layer = Concatenate(axis=3)([UpSampling2D(size=(2, 2))(down_layer), upper_layer])
    down_layer = layers.DepthwiseConv2D(kernel_size, padding='same', use_bias=False, depthwise_initializer=CONV_KERNEL_INITIALIZER, name='dwconv_up4')(down_layer)
    # upper layer 5 (256x256)
    down_layer = Conv2D(fundus_network.get_layer("block2a_expand_activation").output_shape[-1], (1, 1), padding="same", activation=get_swish(), name="down_layer_1x1_5")(down_layer)
    upper_layer = fundus_network.get_layer("block2a_expand_activation").output
    down_layer = Concatenate(axis=3)([UpSampling2D(size=(2, 2))(down_layer), upper_layer])
    down_layer = layers.DepthwiseConv2D(kernel_size, padding='same', use_bias=False, depthwise_initializer=CONV_KERNEL_INITIALIZER, name='dwconv_up5')(down_layer)
    down_layer = Conv2D(32, (1, 1), padding="same", activation=get_swish(), name="down_layer_1x1_final")(down_layer)
    
    featuremap = UpSampling2D(size=(4, 4))(down_layer)

    confidence_map = Conv2D(6, (7, 7), strides=(1, 1), padding="same", activation='softmax')(featuremap)

    network = Model(fundus_network.input, confidence_map, name="lesion_segmentor")
    return network

def set_output_lesion_segmentation(fundus_network):

    division_factor=16
    kernel_size=3
    
    # set outputs and generate model
    list_featuremap = []
    for finding in LIST_FINDINGS:
        featuremap = fundus_network.get_layer("top_activation_{}".format(finding)).output
        featuremap = Conv2D(fundus_network.get_layer("top_activation_{}".format(finding)).output_shape[-1]//division_factor, (1, 1), padding="same", activation=get_swish(), name="final_layer_{}".format(finding))(featuremap)
        list_featuremap.append(featuremap)
    down_layer = Concatenate(axis=3)(list_featuremap)
    # upper layer 1 (16x16)
    list_featuremap = []
    for finding in LIST_FINDINGS:
        featuremap = fundus_network.get_layer("block7a_{}_expand_activation".format(finding)).output
        featuremap = Conv2D(fundus_network.get_layer("block7a_{}_expand_activation".format(finding)).output_shape[-1]//division_factor, (1, 1), padding="same", activation=get_swish(), name="upper_layer_1_{}".format(finding))(featuremap)
        list_featuremap.append(featuremap)
    upper_layer = Concatenate(axis=3)(list_featuremap)
    down_layer = Concatenate(axis=3)([UpSampling2D(size=(2, 2))(down_layer), upper_layer])
    down_layer = layers.DepthwiseConv2D(kernel_size, padding='same', use_bias=False, depthwise_initializer=CONV_KERNEL_INITIALIZER, name='dwconv_up1')(down_layer)
    # upper layer 2 (32x32)
    down_layer = Conv2D(fundus_network.get_layer("block6a_Hemorrhage_expand_activation").output_shape[-1], (1, 1), padding="same", activation=get_swish(), name="down_layer_1x1_2")(down_layer)
    list_featuremap = []
    for finding in LIST_FINDINGS:
        featuremap = fundus_network.get_layer("block6a_{}_expand_activation".format(finding)).output
        featuremap = Conv2D(fundus_network.get_layer("block6a_{}_expand_activation".format(finding)).output_shape[-1]//division_factor, (1, 1), padding="same", activation=get_swish(), name="upper_layer_2_{}".format(finding))(featuremap)
        list_featuremap.append(featuremap)
    upper_layer = Concatenate(axis=3)(list_featuremap)
    down_layer = Concatenate(axis=3)([UpSampling2D(size=(2, 2))(down_layer), upper_layer])
    down_layer = layers.DepthwiseConv2D(kernel_size, padding='same', use_bias=False, depthwise_initializer=CONV_KERNEL_INITIALIZER, name='dwconv_up2')(down_layer)
    # upper layer 3 (64x64)
    down_layer = Conv2D(fundus_network.get_layer("block4a_Hemorrhage_expand_activation").output_shape[-1], (1, 1), padding="same", activation=get_swish(), name="down_layer_1x1_3")(down_layer)
    list_featuremap = []
    for finding in LIST_FINDINGS:
        featuremap = fundus_network.get_layer("block4a_{}_expand_activation".format(finding)).output
        featuremap = Conv2D(fundus_network.get_layer("block4a_{}_expand_activation".format(finding)).output_shape[-1]//division_factor, (1, 1), padding="same", activation=get_swish(), name="upper_layer_3_{}".format(finding))(featuremap)
        list_featuremap.append(featuremap)
    upper_layer = Concatenate(axis=3)(list_featuremap)
    down_layer = Concatenate(axis=3)([UpSampling2D(size=(2, 2))(down_layer), upper_layer])
    down_layer = layers.DepthwiseConv2D(kernel_size, padding='same', use_bias=False, depthwise_initializer=CONV_KERNEL_INITIALIZER, name='dwconv_up3')(down_layer)
    # upper layer 4 (128x128)
    down_layer = Conv2D(fundus_network.get_layer("block2f_activation").output_shape[-1], (1, 1), padding="same", activation=get_swish(), name="down_layer_1x1_4")(down_layer)
    upper_layer = fundus_network.get_layer("block2f_activation").output
    down_layer = Concatenate(axis=3)([UpSampling2D(size=(2, 2))(down_layer), upper_layer])
    down_layer = layers.DepthwiseConv2D(kernel_size, padding='same', use_bias=False, depthwise_initializer=CONV_KERNEL_INITIALIZER, name='dwconv_up4')(down_layer)
    # upper layer 5 (256x256)
    down_layer = Conv2D(fundus_network.get_layer("block2a_expand_activation").output_shape[-1], (1, 1), padding="same", activation=get_swish(), name="down_layer_1x1_5")(down_layer)
    upper_layer = fundus_network.get_layer("block2a_expand_activation").output
    down_layer = Concatenate(axis=3)([UpSampling2D(size=(2, 2))(down_layer), upper_layer])
    down_layer = layers.DepthwiseConv2D(kernel_size, padding='same', use_bias=False, depthwise_initializer=CONV_KERNEL_INITIALIZER, name='dwconv_up5')(down_layer)
    down_layer = Conv2D(32, (1, 1), padding="same", activation=get_swish(), name="down_layer_1x1_final")(down_layer)
    
    featuremap = UpSampling2D(size=(4, 4))(down_layer)

    confidence_map = Conv2D(6, (7, 7), strides=(1, 1), padding="same", activation='softmax')(featuremap)

    network = Model(fundus_network.input, confidence_map, name="lesion_segmentor")
    return network


def set_optimizer_lesion_segmentation(network, lr_start_value):

    def dice_coef(y_true, y_pred, smooth=1):
        y_true_f = K.flatten(y_true)
        y_pred_f = K.flatten(y_pred)
        intersection = K.sum(y_true_f * y_pred_f)
        return (2. * intersection + smooth) / (K.sum(y_true_f) + K.sum(y_pred_f) + smooth)

    # def weighted_ce(y_true, y_pred):
    #     weights = K.variable([0.0017966772766189424, 0.09730740311700403, 0.11550660667880756, 0.2779337670960289, 0.13577214514129204, 0.37168340069024836])
    #     y_pred = K.clip(y_pred, K.epsilon(), 1 - K.epsilon())
    #     loss = y_true * K.log(y_pred) * weights
    #     loss = -K.sum(loss, -1)
    #     return loss

    network.compile(optimizer=Adam(lr=lr_start_value, beta_1=0.5), loss=objectives.categorical_crossentropy, metrics=[dice_coef])
    return network

def set_optimizer_lesion_segmentation_individual(network, lr_start_value):

    def dice_coef(y_true, y_pred, smooth=1):
        y_true_f = K.flatten(y_true)
        y_pred_f = K.flatten(y_pred)
        intersection = K.sum(y_true_f * y_pred_f)
        return (2. * intersection + smooth) / (K.sum(y_true_f) + K.sum(y_pred_f) + smooth)

    def bce(y_true, y_pred):
        y_true_flat = K.batch_flatten(y_true)
        y_pred_flat = K.batch_flatten(y_pred)
        return objectives.binary_crossentropy(y_true_flat, y_pred_flat)

    network.compile(optimizer=Adam(lr=lr_start_value, beta_1=0.5), loss=bce, metrics=[dice_coef])
    return network