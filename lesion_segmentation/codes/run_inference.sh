# python inference_hemorrhage.py --gpu_index=0 --img_dir=../../../data/AMD/Validation-400-images/ --submission_version=FinalRound
# python inference_drusen.py --gpu_index=0 --img_dir=../../../data/AMD/Validation-400-images/ --submission_version=FinalRound
# python inference_exudate.py --gpu_index=0 --img_dir=../../../data/AMD/Validation-400-images/ --submission_version=FinalRound
# python inference_others.py --gpu_index=0 --img_dir=../../../data/AMD/Validation-400-images/ --submission_version=FinalRound
# python inference_scar.py --gpu_index=0 --img_dir=../../../data/AMD/Validation-400-images/ --submission_version=FinalRound

python inference_hemorrhage.py --gpu_index=0 --img_dir=../../../data/AMD/Test-400-images/ --submission_version=FinalRound
python inference_drusen.py --gpu_index=0 --img_dir=../../../data/AMD/Test-400-images/ --submission_version=FinalRound
python inference_exudate.py --gpu_index=0 --img_dir=../../../data/AMD/Test-400-images/ --submission_version=FinalRound
python inference_others.py --gpu_index=0 --img_dir=../../../data/AMD/Test-400-images/ --submission_version=FinalRound
python inference_scar.py --gpu_index=0 --img_dir=../../../data/AMD/Test-400-images/ --submission_version=FinalRound