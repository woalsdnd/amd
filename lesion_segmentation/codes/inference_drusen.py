# python inference_drusen.py  --gpu_index=2 --img_dir=../../../data/AMD/Training400/ori_images --submission_version=20th_epoch_train
import os
import argparse
import time

import numpy as np
import pandas as pd
from PIL import Image
from skimage import measure

import utils

from model.efficient_net_modify import EfficientNet_multiheads, set_output_lesion_segmentation_individual

from keras.models import Model

# arrange arguments
parser = argparse.ArgumentParser()
parser.add_argument(
    '--gpu_index',
    type=str,
    required=True
    )
parser.add_argument(
    '--img_dir',
    type=str,
    required=True
    )
parser.add_argument(
    '--submission_version',
    type=str,
    required=True
    )
FLAGS, _ = parser.parse_known_args()

# misc params
dir_lesions = ["Drusen", "Exudate", "Hemorrhage", "Others", "Scar"]
dict_blob_threshold = {"Drusen":10, "Exudate":5, "Hemorrhage":10, "Others":50, "Scar":30}
target_lesion = "Drusen"
threshold = dict_blob_threshold[target_lesion]
list_model_path = ["/home/vuno/development/AMD/lesion_segmentation/model/drusen/weight_90epoch.h5",
                    # "/home/vuno/development/AMD/lesion_segmentation/model/drusen/weight_115epoch.h5",
                    "/home/vuno/development/AMD/lesion_segmentation/model/drusen/weight_120epoch.h5", 
                    # "/home/vuno/development/AMD/lesion_segmentation/model/drusen/weight_140epoch.h5",
                    "/home/vuno/development/AMD/lesion_segmentation/model/drusen/weight_150epoch.h5"]
input_size = (1024, 1024, 3)  # (h,w)
os.environ['CUDA_VISIBLE_DEVICES'] = FLAGS.gpu_index
out_dir_home="/home/vuno/development/AMD/lesion_segmentation/submission/{}".format(FLAGS.submission_version)
utils.makedirs(os.path.join(out_dir_home, target_lesion))

# load a network
list_network = []
for model_path in list_model_path:
    network = EfficientNet_multiheads(input_shape=input_size)
    network = set_output_lesion_segmentation_individual(network, 3)
    network.load_weights(model_path)
    list_network.append(network)
    print("load weights from {}".format(model_path))

# run inference
all_filepaths = utils.all_files_under(FLAGS.img_dir)
for filepath in all_filepaths:
    print("processing {}...".format(filepath))
    img = np.array(Image.open(filepath))
    ori_h, ori_w, _ = img.shape
    resized_img = utils.resize_img(img, input_size[0], input_size[1])
    normalized_resized_img = utils.normalize(resized_img)
    network_input = np.expand_dims(normalized_resized_img, axis=0)

    list_pred = []
    for network in list_network:
        pred = network.predict(network_input)[0,...,0]
        list_pred.append(pred)
    pred_avg = np.mean(np.array(list_pred), axis=0)
    pred_mask = np.round(pred_avg)
    pred_mask = utils.fill_holes(pred_mask)
    lesion_mask = (np.ones(pred_avg.shape)*255).astype(np.uint8)
    lesion_mask[pred_mask==1] = 0
    blobs_labels, n_blobs = measure.label(lesion_mask==0, return_num=True)
    if n_blobs > 0:
        if np.array([np.sum(blobs_labels==pos_index) < threshold for pos_index in range(1,n_blobs+1)]).all():
            for pos_index in range(1,n_blobs+1):
                lesion_mask[blobs_labels==pos_index]=255 # set to background
    mask_ori_size = utils.resize_img(lesion_mask, ori_h, ori_w, method="nearest")
    Image.fromarray(mask_ori_size).save(os.path.join(out_dir_home, target_lesion, os.path.basename(filepath).replace(".jpg", ".png")))
        
