import numpy as np
import os
from PIL import Image

import utils

# img_size = (64,64)
img_size = (1024,1024)

fundus_filepaths = utils.all_files_under("/home/vuno/development/data/AMD/Training400/resized_1024x1024")
homedir_resized_lesion_mask = "/home/vuno/development/data/AMD/Training400-Lesion/Lesion_Masks_{}x{}_ori".format(img_size[0], img_size[1])

list_lesion=["drusen",	"exudate",  "hemorrhage",  "others",  "scar"]
for lesion in list_lesion:
    maskdir = os.path.join(homedir_resized_lesion_mask, lesion)
    os.makedirs(maskdir, exist_ok=True)
    mask_filepaths = utils.all_files_under(maskdir)
    for fname_null_mask in set([os.path.basename(fp) for fp in fundus_filepaths])-set([os.path.basename(fp) for fp in mask_filepaths]):
        Image.fromarray((np.ones(img_size)*255).astype(np.uint8)).save(os.path.join(maskdir, fname_null_mask))

homedir_flipped_resized_lesion_mask = "/home/vuno/development/data/AMD/Training400-Lesion/Lesion_Masks_{}x{}".format(img_size[0],img_size[1])
list_lesion=["drusen",	"exudate",  "hemorrhage",  "others",  "scar"]
for lesion in list_lesion:
    flipped_maskdir = os.path.join(homedir_flipped_resized_lesion_mask, lesion)
    os.makedirs(flipped_maskdir, exist_ok=True)
    mask_filepaths = utils.all_files_under(os.path.join(homedir_resized_lesion_mask, lesion))
    for mask_filepath in mask_filepaths:
        Image.fromarray((255-np.array(Image.open(mask_filepath))).astype(np.uint8)).save(os.path.join(flipped_maskdir, os.path.basename(mask_filepath)))