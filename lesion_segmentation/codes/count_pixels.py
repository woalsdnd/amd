import numpy as np
import os
from PIL import Image

import utils

lesions = ["drusen", "exudate", "hemorrhage", "others", "scar"]

dict_n_pixels = {lesion: 0 for lesion in lesions}
dict_n_pixels.update({"background": 0})
n_pos_imgs = 0
for lesion in lesions:
    for fpath in utils.all_files_under(os.path.join("/home/vuno/development/data/AMD/Training400-Lesion/Lesion_Masks", lesion)):
        arr = np.array(Image.open(fpath))
        dict_n_pixels[lesion] += len(arr[arr == 0])  # lesion masked as 0 in the original data
        dict_n_pixels["background"] += len(arr[arr == 255])  # lesion masked as 255 in the original data
        n_pos_imgs += 1

dict_n_pixels["background"] += 2056*2124*n_pos_imgs/10
dict_normalized = {key: 1.*val/np.sum(list(dict_n_pixels.values())) for key, val in dict_n_pixels.items()}
dict_inverse = {key: 1./val for key, val in dict_n_pixels.items()}
dict_inverse_normalized = {key: 1.*val/np.sum(list(dict_inverse.values())) for key, val in dict_inverse.items()}

print("n_pixels", dict_n_pixels)
print("normalized n_pixels", dict_normalized)
print("inverse n_pixels", dict_inverse)
print("normalized inverse", dict_inverse_normalized)
