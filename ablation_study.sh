cd fovea_localization/codes/
python train_ablation.py  --config_file=../config/ablation.cfg

cd ../../disc_segmentation/codes/
python train_ablation.py --config_file=../config/ablation.cfg